/*
 * CCoffee.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "CCoffee.hpp"

CCoffee::CCoffee(int amount, int caffeine)
: CLiquid(amount)
, mCaffeine(caffeine)
{
	// TODO Auto-generated constructor stub

}

CCoffee::~CCoffee()
{
	// TODO Auto-generated destructor stub
}

void CCoffee::add(int amount)
{
	mAmount += amount;
}

void CCoffee::remove(int amount)
{
	mAmount -= amount;

		if(mAmount < 0)
		{
			mAmount = 0;
		}
}

void CCoffee::removeAll()
{
	mAmount = 0;
}

