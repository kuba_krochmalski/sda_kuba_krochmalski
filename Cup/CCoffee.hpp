/*
 * CCoffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CCOFFEE_HPP_
#define CCOFFEE_HPP_
#include "CLiquid.hpp"

class CCoffee : public CLiquid
{
protected:
	int mCaffeine;

public:
	CCoffee(int amount, int caffeine);
	virtual ~CCoffee();
	virtual void add(int amount);
	virtual void remove(int amount);
	virtual void removeAll();
};

#endif /* CCOFFEE_HPP_ */
