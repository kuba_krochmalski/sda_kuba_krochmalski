/*
 * CCup.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "CCup.hpp"

CCup::CCup()
: mContent(0)
, mLiquidCounter(0)
{
	// TODO Auto-generated constructor stub

}

CCup::~CCup()
{
	// TODO Auto-generated destructor stub
}

void CCup::resize()
{
	if (mLiquidCounter == 0)
	{
		mContent = new CLiquid*[++mLiquidCounter];
	}
	else
	{
		CLiquid** tmp = new CLiquid*[++mLiquidCounter];

		for (int i = 0; i < mLiquidCounter - 1; ++i) // kopiowanie ze starej tablicy
		{
			tmp[i] = mContent[i];
		}

		delete[] mContent;
		mContent = tmp;
	}
}

void CCup::add(CLiquid* liquid)
{
	resize();

	mContent[mLiquidCounter-1] = liquid;
}

int CCup::calculate()
{
	int liquidAmount = 0;
	if(mLiquidCounter == 0)
	{
		//nic
	}
	else
	{
		for (int i = 0; i < mLiquidCounter; ++i)
		{
			liquidAmount += mContent[i]->getAmount();
		}
	}

	return liquidAmount;
}

void CCup::takeSip(int sip)
{
	int liquidAmount = calculate();

	if(liquidAmount > 0 && sip > 0)
	{
		if(liquidAmount <= sip)
		{
			spill();
		}

		else
		{
			for (int i = 0; i < mLiquidCounter; ++i)
			{
				float liquidRatio = (float)mContent[i]->getAmount() / (float)liquidAmount;
				mContent[i]->remove(liquidRatio*sip);
			}
		}

	}
	else
	{
		//nic
	}
}

void CCup::spill()
{
	for (int i = 0; i < mLiquidCounter; ++i)
	{
		mContent[i]->removeAll();
	}

	clear();
}

void CCup::clear()
{
	for (int i = 0; i < mLiquidCounter; ++i)
	{
		delete mContent[i];
	}

	delete[] mContent;
	mContent = 0;
	mLiquidCounter = 0;
}
