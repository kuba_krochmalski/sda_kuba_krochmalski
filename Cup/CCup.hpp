/*
 * CCup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CCUP_HPP_
#define CCUP_HPP_
#include "CRum.hpp"
#include "CMilk.hpp"
#include "CDecaff.hpp"
#include "CEspresso.hpp"

class CCup
{
private:
	CLiquid** mContent;
	int mLiquidCounter;
	void resize();
	int calculate();

public:
	CCup();
	virtual ~CCup();

	void add(CLiquid* liquid);
	void takeSip(int sip);
	void spill();
	void clear();

};

#endif /* CCUP_HPP_ */
