/*
 * CDecaff.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "CDecaff.hpp"

CDecaff::CDecaff(int amount)
: CCoffee(amount, 0)
{
	// TODO Auto-generated constructor stub

}

CDecaff::~CDecaff()
{
	// TODO Auto-generated destructor stub
}

void CDecaff::add(int amount)
{
	mAmount += amount;
}

void CDecaff::remove(int amount)
{
	mAmount -= amount;

			if(mAmount < 0)
			{
				mAmount = 0;
			}
}

void CDecaff::removeAll()
{
	mAmount = 0;
}

