/*
 * CDecaff.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CDECAFF_HPP_
#define CDECAFF_HPP_
#include "CCoffee.hpp"

class CDecaff : public CCoffee
{
public:
	CDecaff(int amount);
	virtual ~CDecaff();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* CDECAFF_HPP_ */
