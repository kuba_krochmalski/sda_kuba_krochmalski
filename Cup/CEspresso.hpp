/*
 * CEspresso.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CESPRESSO_HPP_
#define CESPRESSO_HPP_
#include "CCoffee.hpp"

class CEspresso : public CCoffee
{
public:
	CEspresso(int amount);
	virtual ~CEspresso();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* CESPRESSO_HPP_ */
