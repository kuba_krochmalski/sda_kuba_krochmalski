/*
 * CLiquid.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CLIQUID_HPP_
#define CLIQUID_HPP_

class CLiquid
{
protected:
	int mAmount;

public:
	CLiquid(int amount);
	virtual ~CLiquid();
	virtual void add(int amount) = 0;
	virtual void remove(int amount) = 0;
	virtual void removeAll() = 0;

	int getAmount() const;
};

#endif /* CLIQUID_HPP_ */
