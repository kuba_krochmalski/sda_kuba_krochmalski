/*
 * CMilk.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "CMilk.hpp"

CMilk::CMilk(int amount, float fat)
: CLiquid(amount)
, mFat(fat)
{
	// TODO Auto-generated constructor stub

}

CMilk::~CMilk()
{
	// TODO Auto-generated destructor stub
}

void CMilk::add(int amount)
{
	mAmount += amount;
}

void CMilk::remove(int amount)
{
	mAmount -= amount;

		if(mAmount < 0)
		{
			mAmount = 0;
		}
}

void CMilk::removeAll()
{
	mAmount = 0;
}

