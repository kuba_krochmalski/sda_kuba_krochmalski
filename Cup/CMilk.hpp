/*
 * CMilk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CMILK_HPP_
#define CMILK_HPP_
#include "CLiquid.hpp"

class CMilk : public CLiquid
{
private:
	float mFat;

public:
	CMilk(int amount, float fat);
	virtual ~CMilk();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* CMILK_HPP_ */
