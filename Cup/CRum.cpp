/*
 * CRum.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "CRum.hpp"

CRum::CRum(int amount, Colour colour)
: CLiquid(amount)
, mColour(colour)
{
	// TODO Auto-generated constructor stub

}

CRum::~CRum()
{
	// TODO Auto-generated destructor stub
}

void CRum::add(int amount)
{
	mAmount += amount;
}

void CRum::remove(int amount)
{
	mAmount -= amount;

	if(mAmount < 0)
	{
		mAmount = 0;
	}
}

void CRum::removeAll()
{
	mAmount = 0;
}
