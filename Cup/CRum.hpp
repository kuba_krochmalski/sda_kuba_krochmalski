/*
 * CRum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CRUM_HPP_
#define CRUM_HPP_
#include "CLiquid.hpp"

class CRum : public CLiquid
{
private:
	enum Colour
	{
		dark,
		light
	};

	Colour mColour;

public:
	CRum(int amount, Colour colour);
	virtual ~CRum();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* CRUM_HPP_ */
