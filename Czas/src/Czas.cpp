#include <iostream>
#include "CCzas.hpp"

using namespace std;

int main()
{
	CCzas teleexpres(17, 0, 0);
	teleexpres.wypisz();
	cout << "---------------" << endl;

	CCzas fakty(19, 0, 0);
	fakty.wypisz();
	cout << "---------------" << endl;

	CCzas roznica = fakty.podajRoznice(teleexpres);
	roznica.wypisz();

	return 0;
}
