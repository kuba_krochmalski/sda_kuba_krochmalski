//============================================================================
// Name        : DanePersonalne.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Osoba.hpp"

using namespace std;


int main()
{
	Osoba czlowiek("Jan", "Kowalski", 5, 12, 1980);

	czlowiek.wypiszDane();

	cout << "--------------------" << endl;

	Osoba czlowiek2("Piotr", "Nowak", 10, 4, 1994);

	czlowiek2.wypiszDane();

	return 0;
}
