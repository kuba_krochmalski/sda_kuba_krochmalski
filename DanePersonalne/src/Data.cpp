/*
 * Data.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Data.hpp"
#include <iostream>

using namespace std;

Data::Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)
{
	if (dzien < 1)
	{
		mDzien = 1;
	}

	else if (dzien > 31)
	{
		mDzien = 31;
	}

	else
	{
		mDzien = dzien;
	}

	if (miesiac < 1)
	{
		mMiesiac = 1;
	}

	else if (miesiac > 12)
	{
		mMiesiac = 12;
	}

	else
	{
		mMiesiac = miesiac;
	}

	if (rok < 1970)
	{
		mRok = 1970;
	}

	else if (rok > 2017)
	{
		mRok = 2017;
	}

	else
	{
		mRok = rok;
	}
}

Data::~Data()
{
	// TODO Auto-generated destructor stub
}

void Data::wypisz()
{
	cout << "Dzien: " << mDzien << endl;
	cout << "Miesiac: " << mMiesiac << endl;
	cout << "Rok: " << mRok << endl;
}

