/*
 * Data.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DATA_HPP_
#define DATA_HPP_

class Data
{
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;

public:
	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	virtual ~Data();
	void wypisz();
};

#endif /* DATA_HPP_ */
