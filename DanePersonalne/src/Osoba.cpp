/*
 * Osoba.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Osoba.hpp"
#include <iostream>

using namespace std;

Osoba::Osoba(std::string imie, std::string nazwisko,
		unsigned int dzien, unsigned int miesiac, unsigned int rok)
: mImie(imie)
, mNazwisko(nazwisko)
, dataUrodzenia(dzien, miesiac, rok)
{
	// TODO Auto-generated constructor stub

}

Osoba::~Osoba()
{
	// TODO Auto-generated destructor stub
}

void Osoba::wypiszDane()
{
	cout << "Imie: " << mImie << endl;
	cout << "Nazwisko: " << mNazwisko << endl;
	dataUrodzenia.wypisz();
}

