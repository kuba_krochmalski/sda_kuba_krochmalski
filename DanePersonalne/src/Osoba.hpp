/*
 * Osoba.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_
#include "Data.hpp"
#include <string>

class Osoba
{
private:
	std::string mImie;
	std::string mNazwisko;
	Data dataUrodzenia;

public:
	Osoba(std::string imie, std::string nazwisko,
			unsigned int dzien, unsigned int miesiac, unsigned int rok);

	virtual ~Osoba();
	void wypiszDane();
};

#endif /* OSOBA_HPP_ */
