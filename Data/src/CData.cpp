/*
 * CData.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "CData.hpp"
#include <iostream>
#include <cstdlib>
#include <cmath>

CData::CData()
: mDzien(1)
, mMiesiac(1)
, mRok(2017)
{

}

CData::CData(unsigned int dzien, unsigned int miesiac, unsigned int rok)
{
		if (dzien < 1)
		{
			mDzien = 1;
		}

		else if (dzien > 31)
		{
			mDzien = 31;
		}

		else
		{
			mDzien = dzien;
		}

		if (miesiac < 1)
		{
			mMiesiac = 1;
		}

		else if (miesiac > 12)
		{
			mMiesiac = 12;
		}

		else
		{
			mMiesiac = miesiac;
		}

		if (rok < 1900)
		{
			mRok = 1900;
		}

		else if (rok > 2017)
		{
			mRok = 2017;
		}

		else
		{
			mRok = rok;
		}
}

CData::~CData()
{
	// TODO Auto-generated destructor stub
}

void CData::wypisz()
{
	std::cout << "Dzien: " << mDzien << std::endl;
	std::cout << "Miesiac: " << mMiesiac << std::endl;
	std::cout << "Rok: " << mRok << std::endl;
}

void CData::przesunRok(int x)
{
	setRok(mRok + x);
}
void CData::przesunMiesiac(int x)
{
	setMiesiac(mMiesiac + x);
}
void CData::przesunDzien(int x)
{
	setDzien(mDzien + x);
}

CData CData::podajRoznice(CData data)
{
	unsigned int dni = abs(mDzien - data.getDzien());
	unsigned int miesiace = abs(mMiesiac - data.getMiesiac());
	unsigned int lata = abs(mRok - data.getRok());

	CData roznica(dni, miesiace, lata);
	return roznica;
}



