#include <iostream>
#include "CData.hpp"

using namespace std;

int main()
{
	CData urodziny(31, 1, 1991);

	urodziny.wypisz();
	std::cout << "----------------" << std::endl;
	urodziny.przesunRok(2);
	urodziny.wypisz();
	std::cout << "----------------" << std::endl;
	urodziny.przesunMiesiac(3);
	urodziny.wypisz();
	std::cout << "----------------" << std::endl;
	urodziny.przesunDzien(-1);
	urodziny.wypisz();
	std::cout << "----------------" << std::endl;

	CData urodziny2(31, 1, 1991);
	CData roznica = urodziny2.podajRoznice(urodziny);
	std::cout << "Roznica: " << std::endl;
	roznica.wypisz();





	return 0;
}
