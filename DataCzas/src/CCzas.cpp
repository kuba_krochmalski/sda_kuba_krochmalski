/*
 * CCzas.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "CCzas.hpp"
#include <iostream>
#include <cstdlib>
#include <cmath>

CCzas::CCzas()
: mGodziny(0)
, mMinuty(0)
, mSekundy(0)
{

}

CCzas::CCzas(unsigned int godziny, unsigned int minuty, unsigned int sekundy)
{
	if (godziny < 0)
	{
		mGodziny = 0;
	}

	else if (godziny > 23)
	{
		mGodziny = 23;
	}

	else
	{
		mGodziny = godziny;
	}

	if (minuty < 0)
	{
		mMinuty = 0;
	}

	else if (minuty > 59)
	{
		mMinuty = 59;
	}

	else
	{
		mMinuty = minuty;
	}

	if (sekundy < 0)
	{
		mSekundy = 0;
	}

	else if (sekundy > 59)
	{
		mSekundy = 59;
	}

	else
	{
		mSekundy = sekundy;
	}
}

CCzas::~CCzas()
{
	// TODO Auto-generated destructor stub
}

void CCzas::wypisz()
{
	std::cout << "Godzina: " << mGodziny << std::endl;
	std::cout << "Minuty: " << mMinuty << std::endl;
	std::cout << "Sekundy: " << mSekundy << std::endl;
}

void CCzas::przesunGodzine(int x)
{
	setGodziny(mGodziny + x);
}
void CCzas::przesunMinute(int x)
{
	setMinuty(mMinuty + x);
}
void CCzas::przesunSekunde(int x)
{
	setSekundy(mSekundy + x);
}

CCzas CCzas::podajRoznice(CCzas czas)
{
	unsigned int godziny = abs(mGodziny - czas.getGodziny());
	unsigned int minuty = abs(mMinuty - czas.getMinuty());
	unsigned int sekundy = abs(mSekundy - czas.getSekundy());

	CCzas roznica(godziny, minuty, sekundy);
	return roznica;
}

