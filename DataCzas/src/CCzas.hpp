/*
 * CCzas.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef CCZAS_HPP_
#define CCZAS_HPP_

class CCzas
{
private:
	unsigned int mGodziny;
	unsigned int mMinuty;
	unsigned int mSekundy;

public:
	CCzas();
	CCzas(unsigned int godziny, unsigned int minuty, unsigned int sekundy);
	virtual ~CCzas();
	virtual void wypisz();
	void przesunGodzine(int x);
	void przesunMinute(int x);
	void przesunSekunde(int x);
	CCzas podajRoznice(CCzas jakisCzas);

	unsigned int getGodziny() const
	{
		return mGodziny;
	}

	void setGodziny(unsigned int godziny)
	{
		if (godziny < 0)
		{
			mGodziny = 0;
		}

		else if (godziny > 23)
		{
			mGodziny = 23;
		}

		else
		{
			mGodziny = godziny;
		}
	}

	unsigned int getMinuty() const
	{
		return mMinuty;
	}

	void setMinuty(unsigned int minuty)
	{
		if (minuty < 0)
		{
			mMinuty = 0;
		}

		else if (minuty > 59)
		{
			mMinuty = 59;
		}

		else
		{
			mMinuty = minuty;
		}
	}

	unsigned int getSekundy() const
	{
		return mSekundy;
	}

	void setSekundy(unsigned int sekundy)
	{
		if (sekundy < 0)
		{
			mSekundy = 0;
		}

		else if (sekundy > 59)
		{
			mSekundy = 59;
		}

		else
		{
			mSekundy = sekundy;
		}
	}
};

#endif /* CCZAS_HPP_ */
