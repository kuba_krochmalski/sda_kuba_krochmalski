/*
 * CDataCzas.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "CDataCzas.hpp"

CDataCzas::CDataCzas()
: CData()
, CCzas()
{
	// TODO Auto-generated constructor stub

}

CDataCzas::CDataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok,
		unsigned int godziny, unsigned int minuty, unsigned int sekundy)
: CData(dzien, miesiac, rok)
, CCzas(godziny, minuty, sekundy)
{
	// TODO Auto-generated constructor stub

}

CDataCzas::CDataCzas(CData data, CCzas czas)
: CData(data.getDzien(), data.getMiesiac(), data.getRok())
, CCzas(czas.getGodziny(), czas.getMinuty(), czas.getSekundy())
{

}

CDataCzas::~CDataCzas()
{
	// TODO Auto-generated destructor stub
}

void CDataCzas::wypisz()
{
	CData::wypisz();
	CCzas::wypisz();
}

