/*
 * CDataCzas.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef CDATACZAS_HPP_
#define CDATACZAS_HPP_
#include "CData.hpp"
#include "CCzas.hpp"

class CDataCzas : public CData, public CCzas
{
public:
	CDataCzas();
	CDataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok,
			unsigned int godziny, unsigned int minuty, unsigned int sekundy);
	CDataCzas(CData data, CCzas czas);
	virtual ~CDataCzas();
	void wypisz();
};

#endif /* CDATACZAS_HPP_ */
