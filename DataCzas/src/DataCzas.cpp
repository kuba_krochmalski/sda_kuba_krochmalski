#include <iostream>
#include "CDataCzas.hpp"

using namespace std;

int main()
{
	CDataCzas wydarzenie;
	wydarzenie.CData::wypisz();
	wydarzenie.CCzas::wypisz();
	cout << "----------------" << endl;

	CDataCzas wydarzenie1(31, 1, 1991, 16, 15, 0);
	wydarzenie1.CData::wypisz();
	wydarzenie1.CCzas::wypisz();
	cout << "----------------" << endl;

	CData urodziny(31, 1, 1991);
	CCzas jakisCzas(16, 15, 0);
	CDataCzas polaczenie(urodziny, jakisCzas);
	polaczenie.wypisz();




	return 0;
}
