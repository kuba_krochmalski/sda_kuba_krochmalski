/*
 * CAbsFabryka.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CABSFABRYKA_HPP_
#define CABSFABRYKA_HPP_

#include "CProcesorIntel.hpp"
#include "CProcesorAMD.hpp"
#include "CProcesor.hpp"
#include "CCoolerIntel.hpp"
#include "CCoolerAMD.hpp"
#include "CCooler.hpp"

class CAbsFabryka
{
protected:
	int mSerialCounter;

public:
	CAbsFabryka();
	virtual ~CAbsFabryka();
	virtual CProcesor* createProcesor() = 0;
	virtual CCooler* createCooler() = 0;

};

#endif /* CABSFABRYKA_HPP_ */
