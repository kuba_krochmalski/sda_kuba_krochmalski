/*
 * CComputer.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "CComputer.hpp"
#include <iostream>

CComputer::CComputer(std::string name, CAbsFabryka& factory)
: mName(name)
, mProcesor(factory.createProcesor())
, mCooler(factory.createCooler())
{
	// TODO Auto-generated constructor stub

}

CComputer::~CComputer()
{
	delete mProcesor;
	delete mCooler;
}

void CComputer::run()
{
	std::cout << "Nazwa: " << mName << std::endl;
	std::cout << "Procesor: "; mProcesor->procesuj();
	std::cout << "Nr Seryjny: " << mProcesor->getNumerSeryjny() << std::endl;
	std::cout << "Cooler: "; mCooler->wiatrakuj();

}

