/*
 * CComputer.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CCOMPUTER_HPP_
#define CCOMPUTER_HPP_
#include <string>
#include "CProcesorIntel.hpp"
#include "CProcesorAMD.hpp"
#include "CProcesor.hpp"
#include "CCoolerIntel.hpp"
#include "CCoolerAMD.hpp"
#include "CCooler.hpp"
#include "CAbsFabryka.hpp"

class CComputer
{
private:
	std::string mName;
	CProcesor* mProcesor;
	CCooler* mCooler;

public:
	CComputer(std::string name, CAbsFabryka& factory);
	virtual ~CComputer();
	void run();
};

#endif /* CCOMPUTER_HPP_ */
