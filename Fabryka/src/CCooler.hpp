/*
 * CCooler.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CCOOLER_HPP_
#define CCOOLER_HPP_

class CCooler
{
public:
	CCooler();
	virtual ~CCooler();
	virtual void wiatrakuj() = 0;
};

#endif /* CCOOLER_HPP_ */
