/*
 * CCoolerAMD.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CCOOLERAMD_HPP_
#define CCOOLERAMD_HPP_

#include "CCooler.hpp"

class CCoolerAMD : public CCooler
{
public:
	CCoolerAMD();
	virtual ~CCoolerAMD();
	void wiatrakuj();
};

#endif /* CCOOLERAMD_HPP_ */
