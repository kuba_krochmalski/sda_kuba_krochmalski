/*
 * CCoolerIntel.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CCOOLERINTEL_HPP_
#define CCOOLERINTEL_HPP_

#include "CCooler.hpp"

class CCoolerIntel : public CCooler
{
public:
	CCoolerIntel();
	virtual ~CCoolerIntel();
	void wiatrakuj();
};

#endif /* CCOOLERINTEL_HPP_ */
