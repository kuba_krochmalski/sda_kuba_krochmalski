/*
 * CFabrykaAMD.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "CFabrykaAMD.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>

CFabrykaAMD::CFabrykaAMD()
: CAbsFabryka()
{
	// TODO Auto-generated constructor stub

}

CFabrykaAMD::~CFabrykaAMD()
{
	// TODO Auto-generated destructor stub
}

CProcesor* CFabrykaAMD::createProcesor()
{
	mSerialCounter+=100;

	return new CProcesorAMD(mSerialCounter);

}

CCooler* CFabrykaAMD::createCooler()
{

	return new CCoolerAMD();

}

