/*
 * CFabrykaAMD.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CFABRYKAAMD_HPP_
#define CFABRYKAAMD_HPP_

#include "CAbsFabryka.hpp"

class CFabrykaAMD : public CAbsFabryka
{
public:
	CFabrykaAMD();
	virtual ~CFabrykaAMD();
	CProcesor* createProcesor();
	CCooler* createCooler();
};

#endif /* CFABRYKAAMD_HPP_ */
