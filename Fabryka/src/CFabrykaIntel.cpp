/*
 * CFabrykaIntel.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "CFabrykaIntel.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>

CFabrykaIntel::CFabrykaIntel()
: CAbsFabryka()
{
	// TODO Auto-generated constructor stub

}

CFabrykaIntel::~CFabrykaIntel()
{
	// TODO Auto-generated destructor stub
}

CProcesor* CFabrykaIntel::createProcesor()
{
	mSerialCounter++;

	return new CProcesorIntel(mSerialCounter);

}

CCooler* CFabrykaIntel::createCooler()
{

	return new CCoolerIntel();

}

