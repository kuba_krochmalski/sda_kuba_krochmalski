/*
 * CFabrykaIntel.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CFABRYKAINTEL_HPP_
#define CFABRYKAINTEL_HPP_

#include "CAbsFabryka.hpp"

class CFabrykaIntel : public CAbsFabryka
{
public:
	CFabrykaIntel();
	virtual ~CFabrykaIntel();
	CProcesor* createProcesor();
	CCooler* createCooler();
};

#endif /* CFABRYKAINTEL_HPP_ */
