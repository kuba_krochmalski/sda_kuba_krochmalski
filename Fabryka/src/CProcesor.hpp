/*
 * CProcesor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CPROCESOR_HPP_
#define CPROCESOR_HPP_

class CProcesor
{
protected:
	int mNumerSeryjny;

public:
	CProcesor(int numer);
	virtual ~CProcesor();
	virtual void procesuj() = 0;

	int getNumerSeryjny() const
	{
		return mNumerSeryjny;
	}
};

#endif /* CPROCESOR_HPP_ */
