/*
 * CProcesorAMD.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CPROCESORAMD_HPP_
#define CPROCESORAMD_HPP_

#include "CProcesor.hpp"

class CProcesorAMD : public CProcesor
{
public:
	CProcesorAMD(int numer);
	virtual ~CProcesorAMD();
	void procesuj();
};

#endif /* CPROCESORAMD_HPP_ */
