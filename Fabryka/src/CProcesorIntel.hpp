/*
 * CProcesorIntel.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CPROCESORINTEL_HPP_
#define CPROCESORINTEL_HPP_

#include "CProcesor.hpp"

class CProcesorIntel : public CProcesor
{
public:
	CProcesorIntel(int numer);
	virtual ~CProcesorIntel();
	void procesuj();
};

#endif /* CPROCESORINTEL_HPP_ */
