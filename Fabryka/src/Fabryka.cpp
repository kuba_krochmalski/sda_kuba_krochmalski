#include <iostream>
using namespace std;

#include "CProcesorIntel.hpp"
#include "CProcesorAMD.hpp"
#include "CProcesor.hpp"
#include "CCoolerIntel.hpp"
#include "CCoolerAMD.hpp"
#include "CCooler.hpp"
#include "CAbsFabryka.hpp"
#include "CFabrykaIntel.hpp"
#include "CFabrykaAMD.hpp"
#include "CComputer.hpp"

int main()
{
	CFabrykaIntel fabrykaIntel;
	CFabrykaAMD fabrykaAMD;

	CComputer komputer1("IntelPC", fabrykaIntel);
	komputer1.run();

	cout << "-------------------------------------" << endl;

	CComputer komputer2("AMD_PC", fabrykaAMD);
	komputer2.run();

	cout << "-------------------------------------" << endl;

	CComputer komputer3("Jan", fabrykaIntel);
	komputer3.run();

	cout << "-------------------------------------" << endl;

	CComputer komputer4("Halina", fabrykaAMD);
	komputer4.run();


	return 0;
}
