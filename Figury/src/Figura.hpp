/*
 * Figura.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef FIGURA_HPP_
#define FIGURA_HPP_

class Figura
{
public:
	Figura();
	virtual ~Figura();
	virtual float pole() = 0;
};

#endif /* FIGURA_HPP_ */
