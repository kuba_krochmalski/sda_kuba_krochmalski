#include <iostream>
#include "Kolo.hpp"
#include "Trojkat.hpp"
#include "Prostokat.hpp"

using namespace std;

void wyswietlPole(Figura &figura)
{
	std::cout << "Pole figury: " << figura.pole() << std::endl;
}

int main()
{

Kolo k(1.0);
Trojkat t(2.0, 3.0);
Prostokat p(2.0, 3.0);

Figura *wsk;

wsk = &k;
std::cout << "Pole kola: " << wsk->pole() << std::endl;

wsk = &t;
std::cout << "Pole trojkata: " << wsk->pole() << std::endl;

wsk = &p;
std::cout << "Pole prostokata: " << wsk->pole() << std::endl;
std::cout << "--------------------------" << std::endl;

wyswietlPole(k);
wyswietlPole(t);
wyswietlPole(p);
std::cout << "--------------------------" << std::endl;

	return 0;
}
