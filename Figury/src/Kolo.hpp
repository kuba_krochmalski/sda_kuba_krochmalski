/*
 * Kolo.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_
#include "Figura.hpp"

class Kolo: public Figura
{
private:
	const float pi = 3.1415926;
	float r;

public:
	Kolo(float promien);
	virtual ~Kolo();
	float pole();
};

#endif /* KOLO_HPP_ */
