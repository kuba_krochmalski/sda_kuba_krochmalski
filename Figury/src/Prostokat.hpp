/*
 * Prostokat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PROSTOKAT_HPP_
#define PROSTOKAT_HPP_
#include "Figura.hpp"

class Prostokat: public Figura
{
private:
	float a, b;
public:
	Prostokat(float x, float y);
	virtual ~Prostokat();
	float pole();
};

#endif /* PROSTOKAT_HPP_ */
