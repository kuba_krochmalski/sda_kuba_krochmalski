/*
 * Kalkulator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_
#include "FileLogger.hpp"

class Kalkulator
{
public:
	Kalkulator();

	int podziel(int a, int b);
};

#endif /* KALKULATOR_HPP_ */
