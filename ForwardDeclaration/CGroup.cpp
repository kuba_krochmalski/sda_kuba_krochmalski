/*
 * CGroup.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "CGroup.hpp"
#include "CUnit.hpp"

CGroup::CGroup()
: mUnits(0)
, mSize(0)
{
	// TODO Auto-generated constructor stub

}

CGroup::~CGroup()
{
	clear();
}

void CGroup::resize()
{
	if(mSize == 0)
	{
		mSize++;
		mUnits = new CUnit*[mSize];
	}
	else
	{
		mSize++;
		CUnit** newUnitsArray = new CUnit*[mSize];

		for (unsigned int i = 0; i < mSize; ++i)
		{
			newUnitsArray[i] = mUnits[i];
		}

		delete[] mUnits;

		mUnits = newUnitsArray;
	}
}

void CGroup::add(CUnit* unitPtr)
{
	resize();
	mUnits[mSize-1] = unitPtr;
}

void CGroup::clear()
{
	for (unsigned int i = 0; i < mSize; ++i)
	{
		delete mUnits[i];
	}

	delete[] mUnits;
	mSize = 0;
	mUnits = 0;
}

void CGroup::replicateGroup()
{
	unsigned int currentSize = mSize;
	for (unsigned int i = 0; i < currentSize ; ++i)
	{
		mUnits[i]->replicate();
	}
}

void CGroup::printUnits()
{
	for (unsigned int i = 0; i < mSize ; ++i)
	{
		mUnits[i]->printId();
	}
}




