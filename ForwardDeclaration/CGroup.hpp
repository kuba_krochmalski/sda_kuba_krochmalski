/*
 * CGroup.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef CGROUP_HPP_
#define CGROUP_HPP_

class CUnit;

class CGroup
{
private:
	CUnit** mUnits;
	unsigned int mSize;

	void resize();

public:
	CGroup();
	virtual ~CGroup();

	void add(CUnit* unitPtr);
	void clear();
	void replicateGroup();
	void printUnits();
};

#endif /* CGROUP_HPP_ */
