/*
 * CUnit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "CUnit.hpp"
#include "CGroup.hpp"
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

CUnit::CUnit(std::string id, CGroup* groupPtr)
: mId(id)
, mGroupPtr(0)
{
	addToGroup(groupPtr);

}

CUnit::~CUnit()
{
	// TODO Auto-generated destructor stub
}

void CUnit::printId()
{
	std::cout << "Unit's id: " << mId << std::endl;
}

void CUnit::replicate()
{
	int idVariation = rand()%10;
	char newId = '0' + idVariation;

	CUnit* newborn = new CUnit(mId + newId, mGroupPtr);
//	newborn->addToGroup(mGroupPtr);
//	mGroupPtr->add(newborn);
}

void CUnit::addToGroup(CGroup* groupPtr)
{
	mGroupPtr = groupPtr;
	mGroupPtr->add(this);
}

