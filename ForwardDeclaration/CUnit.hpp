/*
 * CUnit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef CUNIT_HPP_
#define CUNIT_HPP_
#include <string>

class CGroup;

class CUnit
{
private:
	std::string mId;
	CGroup* mGroupPtr;

public:
	CUnit(std::string id, CGroup* groupPtr);
	virtual ~CUnit();

	void printId();
	void replicate();
	void addToGroup(CGroup* groupPtr);
};

#endif /* CUNIT_HPP_ */
