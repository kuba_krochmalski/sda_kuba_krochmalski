/*
 * CHaslo.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "CHaslo.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>

CHaslo::CHaslo(unsigned int dlugosc, int liczba)
: mDlugosc(dlugosc)
{
	srand(time(NULL) + liczba);

	mHaslo += losujMala();
	mHaslo += losujDuza();
	mHaslo += losujCyfre();

	for (unsigned int i = 3; i < mDlugosc; ++i)
	{
		mHaslo += losujCokolwiek();
	}

	przemieszaj(mHaslo);
}

CHaslo::~CHaslo()
{
	// TODO Auto-generated destructor stub
}

std::string CHaslo::getHaslo()
{
	return mHaslo;
}

int CHaslo::losujMala()
{
	return rand()%26 + 97;
}
int CHaslo::losujDuza()
{
	return losujMala() - 32;
}
int CHaslo::losujCyfre()
{
	return rand()%10 + 48;
}
int CHaslo::losujZnak()
{
	int jakiZnak = rand()%6;

	switch(jakiZnak)
	{
	case 0:
		return 33;
		break;
	case 1:
		return 64;
		break;
	case 2:
		return 35;
		break;
	case 3:
		return 36;
		break;
	case 4:
		return 37;
		break;
	case 5:
		return 38;
		break;
	}

	return 0;
}
int CHaslo::losujCokolwiek()
{
	int losowanie = rand()%4 + 1;

	switch (losowanie)
	{
	case 1:
		return losujMala();
		break;
	case 2:
		return losujDuza();
		break;
	case 3:
		return losujCyfre();
		break;
	case 4:
		return losujZnak();
		break;
	}

	return 0;
}

void CHaslo::przemieszaj(std::string& str)
{
	for (int i = str.size(); i > 0; i--)
	{
		int pos = rand()%str.size();
		char tmp = str[i-1];
		str[i-1] = str[pos];
		str[pos] = tmp;
	}
}

