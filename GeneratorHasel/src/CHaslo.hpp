/*
 * CHaslo.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef CHASLO_HPP_
#define CHASLO_HPP_

#include <string>

class CHaslo
{
private:
	unsigned int mDlugosc;
	std::string mHaslo;
	int losujMala();
	int losujDuza();
	int losujCyfre();
	int losujZnak();
	int losujCokolwiek();
	void przemieszaj(std::string& str);

public:
	CHaslo(unsigned int dlugosc, int liczba);
	virtual ~CHaslo();

	std::string getHaslo();
};

#endif /* CHASLO_HPP_ */
