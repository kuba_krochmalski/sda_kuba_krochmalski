//============================================================================
// Name        : GeneratorHasel.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "CHaslo.hpp"

int main()
{
	CHaslo haslo1(10, 0);

	cout << "Haslo: " << haslo1.getHaslo() << endl;
	cout << "--------------------" << endl;

	CHaslo haslo2(10, 5);

	cout << "Haslo: " << haslo2.getHaslo() << endl;
	cout << "--------------------" << endl;

	CHaslo haslo3(10, 88);

	cout << "Haslo: " << haslo3.getHaslo() << endl;
	cout << "--------------------" << endl;

	CHaslo haslo4(10, 123);

	cout << "Haslo: " << haslo4.getHaslo() << endl;
	cout << "--------------------" << endl;


}
