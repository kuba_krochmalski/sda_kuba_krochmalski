/*
 * Jednostka.hpp
 *
 *  Created on: 26.03.2017
 *      Author: Kuba2
 */

#ifndef JEDNOSTKA_HPP_
#define JEDNOSTKA_HPP_

class Jednostka
{
protected:
	int mPozycjaX;
	int mPozycjaY;
	char mSymbol;
	int mHP;
	int mSzybkosc;
	int mRefleks;
	int mAtak;

public:
	Jednostka(int x, int y, char sym, int hp, int spd, int ref, int atk);
	virtual ~Jednostka();
	virtual int atak();
	virtual int ruch();
};

#endif /* JEDNOSTKA_HPP_ */
