/*
 * Kawaleria.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef KAWALERIA_HPP_
#define KAWALERIA_HPP_

#include "Jednostka.hpp"

class Kawaleria : public Jednostka
{
private:
	int mPrzebytyDystans;

public:
	Kawaleria();
	virtual ~Kawaleria();
	int atak();
	int ruch();
};

#endif /* KAWALERIA_HPP_ */
