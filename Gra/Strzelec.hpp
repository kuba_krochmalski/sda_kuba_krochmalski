/*
 * Strzelec.hpp
 *
 *  Created on: 26.03.2017
 *      Author: Kuba2
 */

#ifndef STRZELEC_HPP_
#define STRZELEC_HPP_

#include "Jednostka.hpp"

class Strzelec : public Jednostka
{
public:
	Strzelec();
	virtual ~Strzelec();
	int atak();
	int ruch();
};

#endif /* STRZELEC_HPP_ */
