/*
 * Wojownik.hpp
 *
 *  Created on: 26.03.2017
 *      Author: Kuba2
 */

#ifndef WOJOWNIK_HPP_
#define WOJOWNIK_HPP_

#include "Jednostka.hpp"

class Wojownik : public Jednostka
{
public:
	Wojownik();
	virtual ~Wojownik();
	int atak();
	int ruch();
};

#endif /* WOJOWNIK_HPP_ */
