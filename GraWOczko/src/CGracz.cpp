/*
 * CGracz.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "CGracz.hpp"

CGracz::CGracz(std::string nazwa, CKarta kartaStartowaGracza)
{
	mNazwa = nazwa;
	mReka = CReka(kartaStartowaGracza);
	mStan = aktywny;
}

CGracz::CGracz()
{
	mNazwa = "";
	mStan = aktywny;
}

CGracz::~CGracz()
{
	// TODO Auto-generated destructor stub
}

void CGracz::dobierz(CKarta nowaKarta)
{
	mReka.dodajKarte(nowaKarta);
}

const std::string& CGracz::getNazwa() const
{
	return mNazwa;
}

CGracz::StanGracza CGracz::getStan() const
{
	return mStan;
}

void CGracz::pasuj()
{
	mStan = pas;
}

