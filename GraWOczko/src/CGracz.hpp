/*
 * CGracz.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef CGRACZ_HPP_
#define CGRACZ_HPP_
#include <string>

#include "CReka.hpp"

class CGracz
{
public:
	enum StanGracza
	{
		aktywny,
		pas,
		przegrany
	};

private:

	std::string mNazwa;
	CReka mReka;
	StanGracza mStan;

public:
	CGracz(std::string nazwa, CKarta kartaStartowaGracza);
	CGracz();
	virtual ~CGracz();

	void dobierz(CKarta nowaKarta);
	void pasuj();

	const std::string& getNazwa() const;
	StanGracza getStan() const;
};

#endif /* CGRACZ_HPP_ */
