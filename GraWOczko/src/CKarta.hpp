/*
 * CKarta.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef CKARTA_HPP_
#define CKARTA_HPP_
#include <string>

class CKarta
{
public:
	enum Kolor
	{
		kier,
		karo,
		trefl,
		pik
	};

	enum Figura
	{
		dwojka, trojka, czworka, piatka,
		szostka, siodemka, osemka, dziewiatka, dziesiatka,
		walet,
		dama,
		krol,
		as
	};

private:

	Kolor mKolor;
	Figura mFigura;
	int mWartosc;

public:
	CKarta(Kolor kolor, Figura figura, int wartosc);
	CKarta();
	virtual ~CKarta();

	std::string getFigura();
	void setFigura(Figura figura);
	Kolor getKolor() const;
	void setKolor(Kolor kolor);
	int getWartosc() const;
	void setWartosc(int wartosc);
};

#endif /* CKARTA_HPP_ */
