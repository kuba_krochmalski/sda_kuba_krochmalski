/*
 * CReka.cpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "CReka.hpp"

CReka::CReka(CKarta kartaStartowa)
: mIloscKart(1)
{
	karty[0] = kartaStartowa;

	mSuma = kartaStartowa.getWartosc();
}

CReka::CReka()
{
	mIloscKart = 0;
	mSuma = 0;
}

CReka::~CReka()
{
	// TODO Auto-generated destructor stub
}

void CReka::dodajKarte(CKarta nowaKarta)
{
	karty[mIloscKart] = nowaKarta;

	mSuma += nowaKarta.getWartosc();

	++mIloscKart;
}

