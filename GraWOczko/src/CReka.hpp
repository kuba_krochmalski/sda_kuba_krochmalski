/*
 * CReka.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef CREKA_HPP_
#define CREKA_HPP_

#include "CKarta.hpp"

class CReka
{
private:
	CKarta karty[9];
	int mIloscKart;
	int mSuma;

public:
	CReka(CKarta kartaStartowa);
	CReka();
	virtual ~CReka();
	void dodajKarte(CKarta nowaKarta);
};

#endif /* CREKA_HPP_ */
