/*
 * CTable.cpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "CTable.hpp"

#include <iostream>
using namespace std;

CTable::CTable(int iloscGraczy)
{
	mTalia = CTalia();
	mTalia.tasuj();

	wspolnaKarta = mTalia.nastepnaKarta();

	std::string nazwa;

	for (int i = 0; i < iloscGraczy; i++)
	{
		cout << "Podaj nazwe gracza nr " << i+1 << ": " << flush;
		cin >> nazwa;
		gracze[i] = new CGracz(nazwa, mTalia.nastepnaKarta());
	}

	mAktywnyGracz = 1;
	mIloscGraczy = iloscGraczy;
}

CTable::~CTable()
{
	delete [] gracze;
}

void CTable::rysujStol()
{
	cout << endl;
	cout << "----------- GRA W OCZKO -----------" << endl;
	cout << endl;
	cout << "Karta na stole: " << wspolnaKarta.getFigura() << endl;
}

