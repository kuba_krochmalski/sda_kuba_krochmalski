/*
 * CTable.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef CTABLE_HPP_
#define CTABLE_HPP_

#include "CGracz.hpp"
#include "CTalia.hpp"

class CTable
{
private:
	CGracz* gracze[8];
	CKarta wspolnaKarta;
	int mAktywnyGracz;
	int mIloscGraczy;
	CTalia mTalia;

public:
	CTable(int iloscGraczy);
	virtual ~CTable();

	void rysujStol();
};

#endif /* CTABLE_HPP_ */
