/*
 * CTalia.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "CTalia.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>

CTalia::CTalia()
: mHead(-1)
{
	talia[0] = new CKarta(CKarta::kier, CKarta::dwojka, 2);
	talia[1] = new CKarta(CKarta::karo, CKarta::dwojka, 2);
	talia[2] = new CKarta(CKarta::trefl, CKarta::dwojka, 2);
	talia[3] = new CKarta(CKarta::pik, CKarta::dwojka, 2);

	talia[4] = new CKarta(CKarta::kier, CKarta::trojka, 3);
	talia[5] = new CKarta(CKarta::karo, CKarta::trojka, 3);
	talia[6] = new CKarta(CKarta::trefl, CKarta::trojka, 3);
	talia[7] = new CKarta(CKarta::pik, CKarta::trojka, 3);

	talia[8] = new CKarta(CKarta::kier, CKarta::czworka, 4);
	talia[9] = new CKarta(CKarta::karo, CKarta::czworka, 4);
	talia[10] = new CKarta(CKarta::trefl, CKarta::czworka, 4);
	talia[11] = new CKarta(CKarta::pik, CKarta::czworka, 4);

	talia[12] = new CKarta(CKarta::kier, CKarta::piatka, 5);
	talia[13] = new CKarta(CKarta::karo, CKarta::piatka, 5);
	talia[14] = new CKarta(CKarta::trefl, CKarta::piatka, 5);
	talia[15] = new CKarta(CKarta::pik, CKarta::piatka, 5);

	talia[16] = new CKarta(CKarta::kier, CKarta::szostka, 6);
	talia[17] = new CKarta(CKarta::karo, CKarta::szostka, 6);
	talia[18] = new CKarta(CKarta::trefl, CKarta::szostka, 6);
	talia[19] = new CKarta(CKarta::pik, CKarta::szostka, 6);

	talia[20] = new CKarta(CKarta::kier, CKarta::siodemka, 7);
	talia[21] = new CKarta(CKarta::karo, CKarta::siodemka, 7);
	talia[22] = new CKarta(CKarta::trefl, CKarta::siodemka, 7);
	talia[23] = new CKarta(CKarta::pik, CKarta::siodemka, 7);

	talia[24] = new CKarta(CKarta::kier, CKarta::osemka, 8);
	talia[25] = new CKarta(CKarta::karo, CKarta::osemka, 8);
	talia[26] = new CKarta(CKarta::trefl, CKarta::osemka, 8);
	talia[27] = new CKarta(CKarta::pik, CKarta::osemka, 8);

	talia[28] = new CKarta(CKarta::kier, CKarta::dziewiatka, 9);
	talia[29] = new CKarta(CKarta::karo, CKarta::dziewiatka, 9);
	talia[30] = new CKarta(CKarta::trefl, CKarta::dziewiatka, 9);
	talia[31] = new CKarta(CKarta::pik, CKarta::dziewiatka, 9);

	talia[32] = new CKarta(CKarta::kier, CKarta::dziesiatka, 10);
	talia[33] = new CKarta(CKarta::karo, CKarta::dziesiatka, 10);
	talia[34] = new CKarta(CKarta::trefl, CKarta::dziesiatka, 10);
	talia[35] = new CKarta(CKarta::pik, CKarta::dziesiatka, 10);

	talia[36] = new CKarta(CKarta::kier, CKarta::walet, 2);
	talia[37] = new CKarta(CKarta::karo, CKarta::walet, 2);
	talia[38] = new CKarta(CKarta::trefl, CKarta::walet, 2);
	talia[39] = new CKarta(CKarta::pik, CKarta::walet, 2);

	talia[40] = new CKarta(CKarta::kier, CKarta::dama, 3);
	talia[41] = new CKarta(CKarta::karo, CKarta::dama, 3);
	talia[42] = new CKarta(CKarta::trefl, CKarta::dama, 3);
	talia[43] = new CKarta(CKarta::pik, CKarta::dama, 3);

	talia[44] = new CKarta(CKarta::kier, CKarta::krol, 4);
	talia[45] = new CKarta(CKarta::karo, CKarta::krol, 4);
	talia[46] = new CKarta(CKarta::trefl, CKarta::krol, 4);
	talia[47] = new CKarta(CKarta::pik, CKarta::krol, 4);

	talia[48] = new CKarta(CKarta::kier, CKarta::as, 11);
	talia[49] = new CKarta(CKarta::karo, CKarta::as, 11);
	talia[50] = new CKarta(CKarta::trefl, CKarta::as, 11);
	talia[51] = new CKarta(CKarta::pik, CKarta::as, 11);

}

CTalia::~CTalia()
{
	delete [] talia;
}

void CTalia::tasuj()
{
	srand(time(NULL));

	for (int i = 51; i > -1; i--)
		{
			int pos = rand()%51 + 1;
			CKarta tmp = *talia[i];
			*talia[i] = *talia[pos];
			*talia[pos] = tmp;
		}
}

CKarta CTalia::nastepnaKarta()
{
	++mHead;
	return *talia[mHead];
}
