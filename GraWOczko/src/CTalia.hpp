/*
 * CTalia.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef CTALIA_HPP_
#define CTALIA_HPP_

#include "CKarta.hpp"

class CTalia
{
private:
	CKarta* talia[52];
	int mHead;

public:
	CTalia();
	virtual ~CTalia();

	void tasuj();
	CKarta nastepnaKarta();
};

#endif /* CTALIA_HPP_ */
