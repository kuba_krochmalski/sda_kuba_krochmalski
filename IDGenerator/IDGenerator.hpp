/*
 * IDGenerator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef IDGENERATOR_HPP_
#define IDGENERATOR_HPP_

class IDGenerator
{
private:
	static int id;

public:
	IDGenerator();
	virtual ~IDGenerator();
	static int getNextID();
};

#endif /* IDGENERATOR_HPP_ */
