/*
 * CKalkulatorKalorii.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "CKalkulatorKalorii.hpp"

CKalkulatorKalorii::CKalkulatorKalorii(bool czyMezczyzna, double waga, double wzrost, int wiek, double aktywnosc)
: mMezczyzna(czyMezczyzna)
, mWaga(waga)
, mWzrost(wzrost)
, mWiek(wiek)
, mAktywnosc(aktywnosc)
{
	// TODO Auto-generated constructor stub

}

double CKalkulatorKalorii::getAktywnosc() const
{
	return mAktywnosc;
}

void CKalkulatorKalorii::setAktywnosc(double aktywnosc)
{
	mAktywnosc = aktywnosc;
}

double CKalkulatorKalorii::getWaga() const
{
	return mWaga;
}

void CKalkulatorKalorii::setWaga(double waga)
{
	mWaga = waga;
}

int CKalkulatorKalorii::getWiek() const
{
	return mWiek;
}

void CKalkulatorKalorii::setWiek(int wiek)
{
	mWiek = wiek;
}

double CKalkulatorKalorii::getWzrost() const
{
	return mWzrost;
}

bool CKalkulatorKalorii::isMezczyzna() const
{
	return mMezczyzna;
}

void CKalkulatorKalorii::setMezczyzna(bool mezczyzna)
{
	mMezczyzna = mezczyzna;
}

void CKalkulatorKalorii::setWzrost(double wzrost)
{
	mWzrost = wzrost;
}

CKalkulatorKalorii::~CKalkulatorKalorii()
{
	// TODO Auto-generated destructor stub
}

double CKalkulatorKalorii::obliczBMI()
{
	return mWaga/(mWzrost*mWzrost);
}

int CKalkulatorKalorii::obliczZapotrzebowanie()
{
	if(mMezczyzna == false)
	{
		return 655.1+(9.567*mWaga)+(1.85*mWzrost*100)-(4.68*mWiek);
	}

	else
	{
		return 66.47+(13.7*mWaga)+(5.0*mWzrost*100)-(6.76*mWiek);
	}
}

