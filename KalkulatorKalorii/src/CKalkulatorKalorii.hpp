/*
 * CKalkulatorKalorii.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef CKALKULATORKALORII_HPP_
#define CKALKULATORKALORII_HPP_

class CKalkulatorKalorii
{
private:
	bool mMezczyzna;
	double mWaga;
	double mWzrost;
	int mWiek;
	double mAktywnosc;
	
public:
	CKalkulatorKalorii(bool czyMezczyzna, double waga, double wzrost, int wiek, double aktywnosc);
	virtual ~CKalkulatorKalorii();
	
	double getAktywnosc() const;
	void setAktywnosc(double aktywnosc);
	double getWaga() const;
	void setWaga(double waga);
	int getWiek() const;
	void setWiek(int wiek);
	double getWzrost() const;
	void setWzrost(double wzrost);
	bool isMezczyzna() const;
	void setMezczyzna(bool mezczyzna);

	double obliczBMI();
	int obliczZapotrzebowanie();
	
	
	
};

#endif /* CKALKULATORKALORII_HPP_ */
