/*
 * CKostka.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "CKostka.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>

CKostka::CKostka(int max)
: maksimum(max)
{
	srand(time(NULL));
	wylosowanaWartosc = rand()%maksimum + 1;
}

int CKostka::getWylosowanaWartosc() const
{
	return wylosowanaWartosc;
}


CKostka::~CKostka()
{
	// TODO Auto-generated destructor stub
}

int CKostka::getMaksimum() const
{
	return maksimum;
}

void CKostka::setMaksimum(int maksimum)
{
	this->maksimum = maksimum;
}

int CKostka::losuj()
{
	wylosowanaWartosc = rand()%maksimum + 1;
	return wylosowanaWartosc;
}

