/*
 * CKostka.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef CKOSTKA_HPP_
#define CKOSTKA_HPP_

class CKostka
{
private:
	int wylosowanaWartosc;
	int maksimum;

public:
	CKostka(int max);
	virtual ~CKostka();

	int getWylosowanaWartosc() const;

	int getMaksimum() const;
	void setMaksimum(int maksimum);

	int losuj();

};

#endif /* CKOSTKA_HPP_ */
