//============================================================================
// Name        : KalkulatorKalorii.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "CKalkulatorKalorii.hpp"
#include "gtest/gtest.h"

using namespace std;

//int main()
//{
//	CKalkulatorKalorii kalk(true, 67.5, 1.72, 26, 2.0);
//
//	cout << kalk.obliczBMI() << endl;
//	cout << kalk.obliczZapotrzebowanie() << endl;
//
//	return 0;
//}

TEST(KalkulatorKaloriiTest, Konstruktor)
{
	CKalkulatorKalorii kalk(true, 67.5, 1.72, 26, 2.0);

	EXPECT_EQ(true, kalk.isMezczyzna());
	EXPECT_EQ(67.5, kalk.getWaga());
	EXPECT_EQ(1.72, kalk.getWzrost());
	EXPECT_EQ(26, kalk.getWiek());
}

TEST(KalkulatorKaloriiTest, GetterSetterTest)
{
	CKalkulatorKalorii kalk(true, 67.5, 1.72, 26, 2.0);

	kalk.setMezczyzna(true);
	kalk.setWaga(89.6);
	kalk.setWiek(45);
	kalk.setWzrost(1.90);

	EXPECT_EQ(true, kalk.isMezczyzna());
	EXPECT_EQ(89.6, kalk.getWaga());
	EXPECT_EQ(1.90, kalk.getWzrost());
	EXPECT_EQ(45, kalk.getWiek());
}

int main(int argc, char **argv)
{

	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();

}
