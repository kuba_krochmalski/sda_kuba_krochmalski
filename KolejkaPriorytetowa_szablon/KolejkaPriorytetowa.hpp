/*
 * KolejkaPriorytetowa.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef KOLEJKAPRIORYTETOWA_HPP_
#define KOLEJKAPRIORYTETOWA_HPP_
#include <cstdlib>
#include <exception>
#include <iostream>

template <typename T>

class KolejkaPriorytetowa
{
private:
	struct Node
	{
		T value;
		int prior;
		Node* next;

		Node(T value, int prior)
		{
			this->value = value;
			next = 0;
			this->prior = prior;
		}
	};

	size_t mSize;
	Node* head;

public:
	KolejkaPriorytetowa()
	: mSize(0)
	, head(0)
	{
	}

	virtual ~KolejkaPriorytetowa()
	{

	}

	void push(const T& value, int prior)
	{
		Node* tmp = new Node(value, prior);

		if(mSize == 0)
		{
			head = tmp;
			++mSize;
		}
		else if(mSize == 1)
		{
			if(tmp->prior < head->prior)
			{
				tmp->next = head;
				head = tmp;
				++mSize;
			}
			else
			{
				tmp->next = head->next;
				head->next = tmp;
				++mSize;
			}
		}
		else
		{
			Node* search = head;
			while(search->next != 0 && search->next->prior < tmp->prior)
			{
				search = search->next;
			}

			tmp->next = search->next;
			search->next = tmp;
			mSize++;
		}
	}


	T pop()
	{
		if(mSize == 0)
		{
			throw std::out_of_range("Kolejka jest pusta!");
		}

		T tmp = head->value;
		Node* pop = head->next;
		delete head;
		head = pop;
		mSize--;
		return tmp;
	}

	size_t size()
	{
		return mSize;
	}

	bool empty()
	{
		if(mSize == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};

#endif /* KOLEJKAPRIORYTETOWA_HPP_ */
