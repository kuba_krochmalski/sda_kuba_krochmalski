#include <iostream>
#include "KolejkaPriorytetowa.hpp"
#include <exception>

int main()
{
	KolejkaPriorytetowa<int> kol;

	kol.push(1, 5);
	kol.push(2, 2);
	kol.push(3, 3);
	kol.push(4, 1);
	kol.push(5, 0);
	kol.push(6, 9);
	kol.push(7, 8);

	try
	{
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
		std::cout << kol.pop() << std::endl;
	}
	catch(std::out_of_range& ex)
	{
		std::cout << ex.what() << std::endl;
	}




	return 0;
}




