#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	vector<int> vectInt(100, 8);
	vector<string> vectString;

	vectString.push_back("kuku");
	vectString.push_back("popo");
	vectString.push_back("dudu");
	vectString.push_back("wewe");
	vectString.push_back("fifi");
	vectString.push_back("pupu");

	for(unsigned int i = 0; i < vectInt.size(); ++i)
	{
		cout << vectInt[i] << " ";
	}

	cout << endl;

	for (unsigned int i = 0; i < vectString.size(); ++i)
	{
		cout << vectString[i] << " ";
	}

	cout << endl << "---------------------" << endl;

	cout << vectInt.size() << endl;
	cout << vectInt.max_size() << endl;
	cout << vectInt.capacity() << endl;
	cout << "-----------------------" << endl;

	vectInt.reserve(1000);

	cout << vectInt.size() << endl;
	cout << vectInt.max_size() << endl;
	cout << vectInt.capacity() << endl;
	cout << "-----------------------" << endl;

	vectInt.resize(1300, 12);

	cout << vectInt.size() << endl;
	cout << vectInt.max_size() << endl;
	cout << vectInt.capacity() << endl;
	cout << "-----------------------" << endl;

	while(!vectInt.empty())
	{
		cout << vectInt.back() << endl;
		vectInt.pop_back();
	}

	return 0;
}




