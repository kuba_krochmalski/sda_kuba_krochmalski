#include <iostream>
#include <list>
#include <string>

using namespace std;

bool sortowanieIntowMalejace(const int& first, const int& second)
{
	return first>second;
}

bool sortowaniePoIlosciZnakow(const string& first, const string& second)
{
	return first.size()>second.size();
}

bool wiekszyOdDziesieciu(const int& x)
{
	return x > 10;
}

int main()
{
	list<int> listI;

	listI.push_back(9);
	listI.push_back(0);
	listI.push_back(4);
	listI.push_front(9);
	listI.push_front(0);
	listI.push_front(4);

	list<int>::iterator it = listI.begin();
	list<int>::reverse_iterator itrev = listI.rbegin();

	bool czyPalindrom = true;
	while(it != listI.end() || itrev != listI.rend())
	{
		if(*it != *itrev)
		{
			czyPalindrom = false;
			break;
		}

		++it;
		++itrev;
	}

	cout << ((czyPalindrom == true) ? "" : "nie ") << "jest palindromem" << endl;

	for(list<int>::iterator it = listI.begin(); it != listI.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	listI.sort(sortowanieIntowMalejace);

	for (list<int>::iterator it = listI.begin(); it != listI.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

//	list<string> listS;
//
//	listS.push_back("zzzzzzzz");
//	listS.push_back("ZZZZZZZZ");
//	listS.push_back("abcdefg");
//	listS.push_back("asytsfdiaeiuw");
//	listS.push_back("uyet");
//	listS.push_back("cis");
//
//	for (list<string>::iterator it = listS.begin(); it != listS.end(); ++it)
//	{
//			cout << *it << endl;
//	}
//
//	listS.sort(sortowaniePoIlosciZnakow);
//	cout << endl;
//
//	for (list<string>::iterator it = listS.begin(); it != listS.end(); ++it)
//	{
//			cout << *it << endl;
//	}

	list<int> listI2;
	listI2.push_back(11);
	listI2.push_back(12);
	listI2.push_back(2);

	for(list<int>::iterator it = listI2.begin(); it != listI2.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	listI2.splice(listI2.begin(), listI, ++listI.begin(), --listI.end());

	for(list<int>::iterator it = listI2.begin(); it != listI2.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	listI2.remove(0);

	for(list<int>::iterator it = listI2.begin(); it != listI2.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	listI2.remove_if(wiekszyOdDziesieciu);

	for(list<int>::iterator it = listI2.begin(); it != listI2.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	listI2.unique();

	for(list<int>::iterator it = listI2.begin(); it != listI2.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	listI.merge(listI2);

	for(list<int>::iterator it = listI.begin(); it != listI.end(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;


	return 0;
}


