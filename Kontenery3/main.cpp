#include <iostream>
#include <set>
#include <string>

using namespace std;

struct compareIntegralPart
{
	bool operator () (float first, float second)
	{
		return static_cast<int>(first) < static_cast<int>(second);
	}
};

int main()
{
//	set<string> imiona;
//
//	string tabString[] = {"Janusz", "Zdzislaw", "Adolf"};
//
//	imiona.insert(tabString, tabString+3);
//
//	imiona.insert("Ala");
//	imiona.insert("Jan");
//	imiona.insert("Henryk");
//	imiona.insert("Janina");
//	imiona.insert("Karol");
//	imiona.insert("Zofia");
//	imiona.insert("Krystyna");
//
//	for(set<string>::iterator it = imiona.begin(); it != imiona.end(); ++it)
//	{
//		cout << *it << endl;
//	}

	set<float, compareIntegralPart> liczby;

	liczby.insert(2.1);
	liczby.insert(2.2);
	liczby.insert(3.3);
	liczby.insert(37.7);
	liczby.insert(4.8);
	liczby.insert(3.5);

	for(set<float>::iterator it = liczby.begin(); it != liczby.end(); ++it)
	{
			cout << *it << endl;
	}
	cout << endl;

	set<float>::iterator it;
	it = liczby.find(4.8);
	liczby.erase(it);

	for(set<float>::iterator it = liczby.begin(); it != liczby.end(); ++it)
	{
			cout << *it << endl;
	}



	return 0;
}




