#include <iostream>
#include <utility>
#include <string>
#include <map>

using namespace std;

int main()
{
//	pair <string, int> uczen1;
//	pair <string, int> uczen2("Tomek", 2);
//
//	uczen1 = make_pair("Arek", 5);
//
//	cout << uczen1.first << " " << uczen2.second << endl;

	map<int, string> pracownicy;

	pracownicy.insert(pair<int, string>(1, "Mieciu"));
	pracownicy.insert(pair<int, string>(6, "Stachu"));
	pracownicy.insert(pair<int, string>(3, "Grazynka"));
	pracownicy.insert(pair<int, string>(9, "Heniek"));
	pracownicy.insert(pair<int, string>(13, "Zdzisiu"));

	for(map<int, string>::iterator it = pracownicy.begin(); it != pracownicy.end(); ++it)
	{
		cout << it->first << " " << it->second << endl;
	}

	cout << "Pracownik nr 3: " << pracownicy.find(3)->second << endl;



	return 0;
}




