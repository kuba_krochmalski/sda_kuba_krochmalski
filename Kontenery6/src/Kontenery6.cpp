//============================================================================
// Name        : Kontenery6.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <deque>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

void printIntegerPart(float x)
{
	cout << static_cast<int>(x) << " ";
}

void print(float x)
{
	cout << x << " ";
}

void castIntegerPart(float& x)
{
	x = static_cast<int>(x);
}

struct Sum
{
	Sum()
	:sum(0)
	{
	}

	void operator() (float x)
	{
		sum += x;
	}

	float sum;
};

int main()
{
	deque<int> maly;
	vector<int> duzy;

	for(int i = 0; i < 5; ++i)
	{
		maly.push_back(i + 1);
		duzy.push_back((i + 1)*10);
	}

//	back_insert_iterator< deque<int> > backIt(maly);

	copy(duzy.begin(), duzy.end(), back_inserter(maly));

	for(deque<int>::iterator it = maly.begin(); it != maly.end(); advance(it, 1))
	{
		cout << *it << " ";
	}
	cout << endl;

//	LISTA

	list<float> num;

	num.push_back(33.123);
	num.push_back(17.129);
	num.push_back(-35.921);
	num.push_back(40.437);
	num.push_back(71.886);
	num.push_back(28.641);

	for_each(num.begin(), num.end(), print);
	cout << endl;
	for_each(num.begin(), num.end(), castIntegerPart);
	for_each(num.begin(), num.end(), print);
	cout << endl;

	Sum suma = for_each(num.begin(), num.end(), Sum());

	cout << "Suma = " << suma.sum;

	return 0;
}
