/*
 * Osoba.cpp
 *
 *  Created on: 25.05.2017
 *      Author: RENT
 */

#include "Osoba.hpp"

Osoba::Osoba(std::string imie, int wiek)
: mImie(imie)
, mWiek(wiek)
{
	// TODO Auto-generated constructor stub

}

std::string Osoba::getImie() const
{
	return mImie;
}

void Osoba::setImie(std::string imie)
{
	mImie = imie;
}

int Osoba::getWiek() const
{
	return mWiek;
}

void Osoba::setWiek(int wiek)
{
	mWiek = wiek;
}

Osoba::~Osoba()
{
	// TODO Auto-generated destructor stub
}

