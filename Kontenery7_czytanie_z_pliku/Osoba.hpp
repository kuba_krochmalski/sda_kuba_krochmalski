/*
 * Osoba.hpp
 *
 *  Created on: 25.05.2017
 *      Author: RENT
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_
#include <string>

class Osoba
{
private:
	std::string mImie;
	int mWiek;

public:
	Osoba(std::string imie, int wiek);
	virtual ~Osoba();
	std::string getImie() const;
	void setImie(std::string imie);
	int getWiek() const;
	void setWiek(int wiek);
};

#endif /* OSOBA_HPP_ */
