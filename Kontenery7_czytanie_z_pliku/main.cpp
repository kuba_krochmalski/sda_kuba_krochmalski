#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "Osoba.hpp"

using namespace std;

int main()
{
	vector<Osoba> wektor;

	fstream file;
	file.open("Spis.txt");

	string name;
	int age;

	while(!file.eof())
	{
		file >> name >> age;
		wektor.push_back(Osoba(name, age));
	}

	file.close();

	for(vector<Osoba>::iterator it = wektor.begin(); it != wektor.end(); it++)
	{
		cout << it->getImie() << " " << it->getWiek() << endl;
	}

	return 0;
}




