#include <iostream>
#include <string>
#include <list>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

string sortString(string text)
{
	list<char> characters;
	string revText = "";

	for(unsigned int i = 0; i < text.size(); ++i)
	{
		characters.push_back(text[i]);
	}

	characters.sort();

	for(list<char>::iterator it = characters.begin(); it != characters.end(); ++it)
	{
		revText += *it;
	}

	return revText;
}

string randomShuffle(string text)
{
	srand(time(NULL));
	vector<char> characters;
	string shuffledText = "";

	for(unsigned int i = 0; i < text.size(); ++i)
	{
		characters.push_back(text[i]);
	}

	random_shuffle(characters.begin(), characters.end());

	for(unsigned int i = 0; i < characters.size(); ++i)
	{
		shuffledText += characters[i];
	}

	return shuffledText;
}

int main()
{
	string text;

	cout << "Enter your text: " << flush;
	cin >> text;

	cout << sortString(text) << endl;
	cout << randomShuffle(text) << endl;

	return 0;
}




