#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <utility>

void zlicz(std::string& napis)
{
	int iloscSpacji = std::count(napis.begin(), napis.end(), ' ');

	std::string::iterator begin = napis.begin();
	std::map<std::string, int> mapa;

	for(int i = 0; i < iloscSpacji + 1; ++i)
	{
		std::string::iterator spacePos = std::find(begin, napis.end(), ' ');

		unsigned int substrStart = distance(napis.begin(), begin);
		unsigned int substrEnd = distance(begin, spacePos);

		std::string tmp = napis.substr(substrStart, substrEnd);
		bool czyBylo = false;

		for(std::map<std::string, int>::iterator it = mapa.begin(); it != mapa.end(); ++it)
		{
			if(tmp.compare(it->first) == 0)
			{
				it->second++;
				czyBylo = true;
				break;
			}
		}

		if(czyBylo == false)
		{
			mapa.insert(std::pair<std::string, int>(tmp, 1));
		}

		begin = spacePos + 1;
	}

	std::cout << "Liczba slow: " << iloscSpacji + 1 << std::endl;

	for(std::map<std::string, int>::iterator it = mapa.begin(); it != mapa.end(); ++it)
	{
		std::cout << it->first << " " << it->second << std::endl;
	}
}


int main()
{
	std::string ciagZnakow;
	getline(std::cin, ciagZnakow);

	zlicz(ciagZnakow);

	return 0;
}




