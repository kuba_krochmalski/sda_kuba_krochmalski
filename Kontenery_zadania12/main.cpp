#include <iostream>
#include <list>
#include <string>
#include <algorithm>

using namespace std;

bool Sortowanie(int first, int second)
{
	if(first % 2 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool ZnajdzNieparzysta(int x)
{
	if (x % 2 != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main()
{
	list<int> listaLiczbNaturalnych;

	for(int i = 1; i < 51; ++i)
	{
		listaLiczbNaturalnych.push_back(i);
	}

	listaLiczbNaturalnych.sort(Sortowanie);

//	for(list<int>::iterator it = listaLiczbNaturalnych.begin(); it != listaLiczbNaturalnych.end(); ++it)
//	{
//		cout << *it << endl;
//	}                             // WYPISYWANIE CALEJ LISTY

	list<int> listaLiczbNieparzystych;
	list<int> listaLiczbParzystych;

	list<int>::iterator it = find_if(listaLiczbNaturalnych.begin(), listaLiczbNaturalnych.end(), ZnajdzNieparzysta);

	listaLiczbNieparzystych.splice(listaLiczbNieparzystych.begin(), listaLiczbNaturalnych, it, listaLiczbNaturalnych.end());
	listaLiczbNaturalnych.sort();
	listaLiczbParzystych.splice(listaLiczbParzystych.begin(), listaLiczbNaturalnych, listaLiczbNaturalnych.begin(), listaLiczbNaturalnych.end());

	cout << "PARZYSTE" << endl;
	for(list<int>::iterator it = listaLiczbParzystych.begin(); it != listaLiczbParzystych.end(); ++it)
	{
		cout << *it << " ";
	}

	cout << endl << "NIEPARZYSTE" << endl;
	for(list<int>::iterator it = listaLiczbNieparzystych.begin(); it != listaLiczbNieparzystych.end(); ++it)
	{
		cout << *it << " ";
	}

	return 0;
}




