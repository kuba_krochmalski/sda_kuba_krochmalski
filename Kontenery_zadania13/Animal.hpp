/*
 * Animal.hpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_

class Animal
{
private:
	static int kolejnyNumerSeryjny;
	int numerSeryjny;

public:
	Animal();
	virtual ~Animal();
	virtual void whoAreYou();
	int getNumerSeryjny()
	{
		return numerSeryjny;
	}
};

#endif /* ANIMAL_HPP_ */
