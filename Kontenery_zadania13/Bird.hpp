/*
 * Bird.hpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef BIRD_HPP_
#define BIRD_HPP_
#include "Animal.hpp"

class Bird : public Animal
{
public:
	Bird();
	virtual ~Bird();
	void whoAreYou();
};

#endif /* BIRD_HPP_ */
