/*
 * Dog.hpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef DOG_HPP_
#define DOG_HPP_
#include "Animal.hpp"

class Dog : public Animal
{
public:
	Dog();
	virtual ~Dog();
	void whoAreYou();
};

#endif /* DOG_HPP_ */
