#include <iostream>
#include "Bird.hpp"
#include "Dog.hpp"
#include <list>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

Animal* losujZwierze()
{
	int numer = rand()%2;

	if(numer == 0)
	{
		return new Dog();
	}
	else
	{
		return new Bird();
	}
}

void przedstawSie(Animal* zwierze)
{
	zwierze->whoAreYou();
}

void przedstawPsa(Dog pies)
{
	pies.whoAreYou();
}

void przedstawPtaka(Bird ptak)
{
	ptak.whoAreYou();
}

bool SortowaniePiesPtak(Animal* first, Animal* second)
{
	Dog* DogTmp;

	if(DogTmp = dynamic_cast<Dog*>(first))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void rozdziel(list<Animal*>& zwierzaki, list<Dog>& psy, list<Bird>& ptaki)
{
	Dog* DogTmp;
	Bird* BirdTmp;

	for(list<Animal*>::iterator it = zwierzaki.begin(); it != zwierzaki.end(); ++it)
	{
		if(DogTmp = dynamic_cast<Dog*>(*it))
		{
			psy.push_back(*DogTmp);
		}
		else if(BirdTmp = dynamic_cast<Bird*>(*it))
		{
			ptaki.push_back(*BirdTmp);
		}
		else
		{
			cout << "Blad!" << endl;
		}
	}
}

int main()
{
	list<Animal*> listaZwierzat;

	srand(time(NULL));
	for(int i = 0; i < 200; ++i)
	{
		listaZwierzat.push_back(losujZwierze());
	}

	//for_each(listaZwierzat.begin(), listaZwierzat.end(), przedstawSie);

	//listaZwierzat.sort(SortowaniePiesPtak);

	//for_each(listaZwierzat.begin(), listaZwierzat.end(), przedstawSie);

	list<Dog> listaPsow;
	list<Bird> listaPtakow;

	rozdziel(listaZwierzat, listaPsow, listaPtakow);

	for_each(listaPsow.begin(), listaPsow.end(), przedstawPsa);
	for_each(listaPtakow.begin(), listaPtakow.end(), przedstawPtaka);

	for(list<Animal*>::iterator it = listaZwierzat.begin(); it != listaZwierzat.end(); ++it)
	{
		delete *it;
	}

	return 0;
}




