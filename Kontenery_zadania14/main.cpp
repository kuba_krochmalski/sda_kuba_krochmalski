#include <iostream>
#include <list>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

void wypelnijListe(list<int>& lista, int iloscLiczb)
{
	for(int i = 0; i < iloscLiczb; ++i)
	{
		lista.push_back(rand()%101);
	}
}

void print(int x)
{
	cout << x << " ";
}

bool ParzysteNaPoczatek(int first, int second)
{
	return (first % 2 == 0);
}

bool SortujMalejaco(int first, int second)
{
	return first > second;
}

bool ZnajdzNieparzysta(int x)
{
	return (x % 2 != 0);
}

int main()
{
	srand(time(NULL));

	list<int> listaLiczbCalkowitych;

	wypelnijListe(listaLiczbCalkowitych, 50);

	cout << "Losowo wypelniona lista" << endl;
	for_each(listaLiczbCalkowitych.begin(), listaLiczbCalkowitych.end(), print);
	cout << endl << endl;

	listaLiczbCalkowitych.sort(ParzysteNaPoczatek);

	list<int>::iterator it = find_if(listaLiczbCalkowitych.begin(), listaLiczbCalkowitych.end(), ZnajdzNieparzysta);

	cout << "Lista z parzystymi na poczatku" << endl;
	for_each(listaLiczbCalkowitych.begin(), listaLiczbCalkowitych.end(), print);
	cout << endl << endl;

	list<int> listaLiczbParzystych;

	listaLiczbParzystych.splice(listaLiczbParzystych.begin(), listaLiczbCalkowitych, listaLiczbCalkowitych.begin(), it);

	listaLiczbParzystych.sort();
	cout << "Parzyste posortowane rosnaco" << endl;
	for_each(listaLiczbParzystych.begin(), listaLiczbParzystych.end(), print);
	cout << endl << endl;

	listaLiczbCalkowitych.sort(SortujMalejaco);
	cout << "Nieparzyste posortowane malejaco" << endl;
	for_each(listaLiczbCalkowitych.begin(), listaLiczbCalkowitych.end(), print);
	cout << endl << endl;

	listaLiczbCalkowitych.splice(listaLiczbCalkowitych.begin(), listaLiczbParzystych, listaLiczbParzystych.begin(), listaLiczbParzystych.end());
	cout << "Wynik koncowy" << endl;
	for_each(listaLiczbCalkowitych.begin(), listaLiczbCalkowitych.end(), print);
	cout << endl << endl;

	return 0;

}




