#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<char> doubledCharacters(string first, string second)
{
	vector<char> dblCharacters;

	for(unsigned int i = 0; i < first.size(); ++i)
	{
		for(unsigned int k = 0; k < second.size(); ++k)
		{
			if(first[i] == second[k])
			{
				dblCharacters.push_back(first[i]);
				break;
			}
		}
	}

	for (unsigned int i = 0; i < dblCharacters.size(); ++i)
	{
		for (unsigned int k = i + 1; k < dblCharacters.size(); ++k)
		{
			if (dblCharacters[i] == dblCharacters[k])
			{
				dblCharacters.erase(dblCharacters.begin() + k);
				i = 0;
				k = 0;
			}
		}
	}

	return dblCharacters;
}

int main()
{
	string firstString = "ala ma kota";
	string secondString = "pawel ma psa";

//	cout << "Enter first string: " << flush;
//	cin >> firstString;
//	cout << "Enter second string: " << flush;
//	cin >> secondString;

	vector<char> vector = doubledCharacters(firstString, secondString);

	for(unsigned int i = 0; i < vector.size(); ++i)
	{
		cout << vector[i] << " ";
	}

	return 0;
}




