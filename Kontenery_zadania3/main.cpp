#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

void removeSpaceVector(vector<char>& characters)
{
	for(unsigned int i = 0; i < characters.size(); ++i)
	{
		if(characters[i] == ' ')
		{
			characters.erase(characters.begin() + i);
		}
	}
}

void removeSpaceList(list<char>& characters)
{
	characters.remove(' ');
}

int countChars(vector<char>& vector, char ch)
{
	int charCount = count(vector.begin(), vector.end(), ch);

	return charCount;
}

int main()
{
	string text;
	cout << "Enter your text: " << endl;
	getline(cin, text);

	vector<char> characters;
	list<char> chars;

	for(unsigned int i = 0; i < text.size(); ++i)
	{
		characters.push_back(text[i]);
		chars.push_back(text[i]);
	}

	removeSpaceVector(characters);
	removeSpaceList(chars);

	for(unsigned int i = 0; i < characters.size(); ++i)
	{
		cout << characters[i];
	}

	cout << endl;

	for(list<char>::iterator it = chars.begin(); it != chars.end(); ++it)
	{
		cout << *it;
	}

	cout << endl;

	cout << countChars(characters, 'a') << endl;

	return 0;
}




