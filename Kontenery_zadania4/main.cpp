#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

bool czyPalindrom(string& text)
{
	vector<char> chars;
	vector<char> revChars;

	bool czyPalindrom = true;

	for(unsigned int i = 0; i < text.size(); ++i)
	{
		chars.push_back(text[i]);
		revChars.push_back(text[i]);
	}

	reverse(revChars.begin(), revChars.end());

	for(unsigned int i = 0; i < chars.size(); ++i)
	{
		if(chars[i] != revChars[i])
		{
			czyPalindrom = false;
			break;
		}
	}

	return czyPalindrom;

}

int main()
{
	string text;
	cout << "Enter a string: " << endl;
	getline(cin, text);

	if(czyPalindrom(text))
	{
		cout << "PALINDROM" << endl;
	}

	else
	{
		cout << "NIE PALINDROM" << endl;
	}

	return 0;
}




