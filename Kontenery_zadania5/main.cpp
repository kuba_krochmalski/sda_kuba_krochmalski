#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <numeric>

using namespace std;

void kwadrat(float& x)
{
	x = x*x;
}

void print(float x)
{
	cout << x << " ";
}

vector<float> stworzKwadraty(int start, int end)
{
	vector<float> kwadraty;

	for(int i = start; i <= end; ++i)
	{
		kwadraty.push_back(static_cast<float>(i));
	}

	for_each(kwadraty.begin(), kwadraty.end(), kwadrat);

	return kwadraty;
}

int main()
{
	int poczatek = 3;
	int koniec = 10;

	vector<float> vector = stworzKwadraty(poczatek, koniec);

	for_each(vector.begin(), vector.end(), print);

	cout << endl;

	cout << accumulate(vector.begin(), vector.end(), 0) << endl;

	return 0;
}




