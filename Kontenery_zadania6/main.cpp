#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void print(int x)
{
	cout << x << " ";
}

vector<int> stworzWektor(int n)
{
	vector<int> wektor;

	for(int i = 1; i < n + 1; ++i)
	{
		wektor.push_back(i);
	}

	return wektor;
}

bool czyWielo2(int x)
{
	if(x%2 == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool czyWielo3(int x)
{
	if(x%3 == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

int permutujCyfry(vector<int>& cyfry)
{
	int licznik = 0;

	do
	{
		unsigned int number = 0;

		for(unsigned int i = 0, n = 1; i < cyfry.size(); ++i, n = n*10)
		{
			number += cyfry[i] * n;
		}

		if(number%11 == 0)
		{
			cout << number << endl;
			++licznik;
		}

	} while (next_permutation(cyfry.begin(), cyfry.end()));

	return licznik;
}

int main()
{
	int n = 23;
	vector<int> nowyWektor = stworzWektor(n);
	vector<int> wielo2(n);
	vector<int> wielo3(n);

	for_each(nowyWektor.begin(), nowyWektor.end(), print);
	cout << endl;

	vector<int>::iterator it2 = remove_copy_if(nowyWektor.begin(), nowyWektor.end(), wielo2.begin(), czyWielo2);
	wielo2.resize(distance(wielo2.begin(), it2));
	for_each(wielo2.begin(), wielo2.end(), print);
	cout << endl;

	vector<int>::iterator it3 = remove_copy_if(nowyWektor.begin(), nowyWektor.end(), wielo3.begin(), czyWielo3);
	wielo3.resize(distance(wielo3.begin(), it3));
	for_each(wielo3.begin(), wielo3.end(), print);
	cout << endl;

	vector<int> nowy(n);
	vector<int>::iterator itn = set_intersection(wielo2.begin(), wielo2.end(), wielo3.begin(), wielo3.end(), nowy.begin());
	nowy.resize(distance(nowy.begin(), itn));
	for_each(nowy.begin(), nowy.end(), print);

	do
	{
		for_each(nowy.begin(), nowy.end(), print);
		cout << endl;
	}while(next_permutation(nowy.begin(), nowy.end()));

	cout << "----------------------------" << endl;

	vector<int> wektorCyfr;

	for(int i = 0; i < 10; ++i)
	{
		wektorCyfr.push_back(i);
	}

	cout << permutujCyfry(wektorCyfr) << endl;


	return 0;
}




