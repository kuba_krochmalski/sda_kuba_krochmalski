#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> zamienNaWektor(int& liczba)
{
	vector<int> vector;
	int tmp = liczba;

	do
	{
		vector.push_back(tmp%10);
		tmp = tmp/10;

	}while(tmp > 0);

	reverse(vector.begin(), vector.end());
	return vector;
}

bool czyPalindrom(vector<int> liczba)
{
	bool czyPalindrom = true;

	vector<int> rev;

	for(unsigned int i = 0; i < liczba.size(); ++i)
	{
		rev.push_back(liczba[i]);
	}

	reverse(rev.begin(), rev.end());

	for(unsigned int i = 0; i < liczba.size(); ++i)
	{
		if(liczba[i] != rev[i])
		{
			czyPalindrom = false;
		}
	}

	return czyPalindrom;
}

void print(int x)
{
	cout << x;
}

int main()
{
	int liczba;
	cin >> liczba;

	vector<int> wektorCyfr = zamienNaWektor(liczba);

	for_each(wektorCyfr.begin(), wektorCyfr.end(), print);

	while(czyPalindrom(zamienNaWektor(liczba)) == false)
	{
		++liczba;
	}

	cout << endl << liczba << endl;

	return 0;
}




