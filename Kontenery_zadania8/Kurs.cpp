/*
 * Kurs.cpp
 *
 *  Created on: 27.05.2017
 *      Author: RENT
 */

#include "Kurs.hpp"

Kurs::Kurs(std::string nazwa, int dzien, int poczatek, int koniec, int sala)
: mNazwa(nazwa)
, mDzien(dzien)
, mPoczatek(poczatek)
, mKoniec(koniec)
, mNumerSali(sala)
{
	// TODO Auto-generated constructor stub

}

int Kurs::getDzien() const
{
	return mDzien;
}

void Kurs::setDzien(int dzien)
{
	mDzien = dzien;
}

int Kurs::getKoniec() const
{
	return mKoniec;
}

void Kurs::setKoniec(int koniec)
{
	mKoniec = koniec;
}

const std::string& Kurs::getNazwa() const
{
	return mNazwa;
}

void Kurs::setNazwa(const std::string& nazwa)
{
	mNazwa = nazwa;
}

int Kurs::getPoczatek() const
{
	return mPoczatek;
}

int Kurs::getNumerSali() const
{
	return mNumerSali;
}

void Kurs::setNumerSali(int numerSali)
{
	mNumerSali = numerSali;
}

void Kurs::setPoczatek(int poczatek)
{
	mPoczatek = poczatek;
}

Kurs::~Kurs()
{
	// TODO Auto-generated destructor stub
}

