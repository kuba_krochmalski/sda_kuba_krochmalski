/*
 * Kurs.hpp
 *
 *  Created on: 27.05.2017
 *      Author: RENT
 */

#ifndef KURS_HPP_
#define KURS_HPP_
#include <string>

class Kurs
{
private:
	std::string mNazwa;
	int mDzien;
	int mPoczatek;
	int mKoniec;
	int mNumerSali;

public:
	Kurs(std::string nazwa, int dzien, int poczatek, int koniec, int sala);
	virtual ~Kurs();

	int getDzien() const;
	void setDzien(int dzien);
	int getKoniec() const;
	void setKoniec(int koniec);
	const std::string& getNazwa() const;
	void setNazwa(const std::string& nazwa);
	int getPoczatek() const;
	void setPoczatek(int poczatek);
	int getNumerSali() const;
	void setNumerSali(int numerSali);
};

#endif /* KURS_HPP_ */
