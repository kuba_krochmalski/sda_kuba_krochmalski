/*
 * Kursanci.cpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#include "Kursanci.hpp"

Kursanci::Kursanci(std::string imieNazwisko, int kursy, std::vector<Kurs> spisKursow)
: mImieNazwisko(imieNazwisko)
, mLiczbaKursow(kursy)
, mKursy(spisKursow)
{

}

const std::string& Kursanci::getImieNazwisko() const
{
	return mImieNazwisko;
}

void Kursanci::setImieNazwisko(const std::string& imieNazwisko)
{
	mImieNazwisko = imieNazwisko;
}

const std::vector<Kurs>& Kursanci::getKursy() const
{
	return mKursy;
}

void Kursanci::setKursy(const std::vector<Kurs>& kursy)
{
	mKursy = kursy;
}

int Kursanci::getLiczbaKursow() const
{
	return mLiczbaKursow;
}

void Kursanci::setLiczbaKursow(int liczbaKursow)
{
	mLiczbaKursow = liczbaKursow;
}

Kursanci::~Kursanci()
{
	// TODO Auto-generated destructor stub
}

