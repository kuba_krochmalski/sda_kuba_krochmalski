/*
 * Kursanci.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef KURSANCI_HPP_
#define KURSANCI_HPP_
#include <string>
#include <vector>
#include "Kurs.hpp"

class Kursanci
{
private:
	std::string mImieNazwisko;
	int mLiczbaKursow;
	std::vector<Kurs> mKursy;

public:
	Kursanci(std::string imieNazwisko, int kursy, std::vector<Kurs> spisKursow);
	virtual ~Kursanci();

	const std::string& getImieNazwisko() const;
	void setImieNazwisko(const std::string& imieNazwisko);
	const std::vector<Kurs>& getKursy() const;
	void setKursy(const std::vector<Kurs>& kursy);
	int getLiczbaKursow() const;
	void setLiczbaKursow(int liczbaKursow);
};

#endif /* KURSANCI_HPP_ */
