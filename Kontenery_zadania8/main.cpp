#include <iostream>
#include <vector>
#include <list>
#include <fstream>
#include <string>
#include <algorithm>
#include "Kurs.hpp"
#include "Kursanci.hpp"

using namespace std;

bool sortowanieDniem(Kurs first, Kurs second)
{
	if(first.getDzien() < second.getDzien())
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool sortowanieGodzina(Kurs first, Kurs second)
{
	if(first.getPoczatek() < second.getPoczatek())
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main()
{
	vector<Kurs> listaKursow;

	fstream file;
	file.open("spis.txt");

	string name;
	int day;
	int start;
	int end;
	int sala;

	while(!file.eof())
	{
		file >> name >> day >> start >> end >> sala;
		listaKursow.push_back(Kurs(name, day, start, end, sala));
	}

	file.close();

	sort(listaKursow.begin(), listaKursow.end(), sortowanieGodzina);
	sort(listaKursow.begin(), listaKursow.end(), sortowanieDniem);

	for(vector<Kurs>::iterator it = listaKursow.begin(); it != listaKursow.end(); ++it)
	{
		cout << it->getNazwa() << " " << it->getDzien() << " " << it->getPoczatek() << " " << it->getKoniec() << " " << it->getNumerSali() << endl;
	}

	cout << "-----------------------------" << endl;
	cout << "Konflikt: " << endl;

	for(vector<Kurs>::iterator it = listaKursow.begin(); it != listaKursow.end(); ++it)
	{
		for(vector<Kurs>::iterator it2 = it + 1; it2 != listaKursow.end(); ++it2)
		{
			if(it->getDzien() == it2->getDzien())
			{
				if(it->getKoniec() > it2->getPoczatek())
				{
					cout << it->getNazwa() << " " << it->getDzien() << " " << it->getPoczatek() << " " << it->getKoniec() << endl;
					cout << it2->getNazwa() << " " << it2->getDzien() << " " << it2->getPoczatek() << " " << it2->getKoniec() << endl;
				}
			}
		}
	}

	cout << "-----------------------------" << endl;

	vector<Kursanci> listaKursantow;

	fstream file2;
	file2.open("kursanci.txt");

	string imieNazwisko;
	int liczbaKursow;

	while(!file2.eof())
	{
		vector<Kurs> tmp;
		file2 >> imieNazwisko >> liczbaKursow;

		for(int i = 0; i < liczbaKursow; ++i)
		{
			string kurs;
			file2 >> kurs;

			for(unsigned int i = 0; i < listaKursow.size(); ++i)
			{
				if(kurs == listaKursow[i].getNazwa())
				{
					tmp.push_back(listaKursow[i]);
				}
			}
		}

		listaKursantow.push_back(Kursanci(imieNazwisko, liczbaKursow, tmp));
	}

	file.close();

	for(vector<Kursanci>::iterator it = listaKursantow.begin(); it != listaKursantow.end(); ++it) //wypisywanie
	{
		cout << it->getImieNazwisko() << " " << it->getLiczbaKursow() << " ";

		for(unsigned int i = 0; i < it->getKursy().size(); ++i)
		{
			if(i == 0)
			{
				cout << it->getKursy()[i].getNazwa() << " ";
			}

			for(int k = i - 1; k >= 0; --k)
			{
				if(it->getKursy()[i].getNazwa() == it->getKursy()[k].getNazwa())
				{
					break;
				}

				if(k == 0)
				{
					cout << it->getKursy()[i].getNazwa() << " ";
				}
			}
		}
		cout << endl;
		cout << "Konflikty: " << endl;

		for(unsigned int i = 0; i < it->getKursy().size(); ++i)
		{
			for(unsigned int k = i + 1; k < it->getKursy().size(); ++k)
			{
				if(it->getKursy()[i].getDzien() == it->getKursy()[k].getDzien())
				{
					if(it->getKursy()[i].getKoniec() > it->getKursy()[k].getPoczatek())
					{
						cout << "Konfliktuja " << it->getKursy()[i].getNazwa() <<  " i " << it->getKursy()[k].getNazwa() << endl;
					}
				}
			}
		}

	}

	return 0;
}




