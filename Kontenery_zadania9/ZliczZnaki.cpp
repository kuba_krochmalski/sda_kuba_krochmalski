/*
 * ZliczZnaki.cpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#include "ZliczZnaki.hpp"
#include <utility>
#include <iostream>
#include <string>
#include <map>

using namespace std;

std::map<std::string, std::map<char, int> > ZliczZnaki::rezultaty;

ZliczZnaki::ZliczZnaki()
{
	// TODO Auto-generated constructor stub

}

ZliczZnaki::~ZliczZnaki()
{
	// TODO Auto-generated destructor stub
}

void ZliczZnaki::zlicz(std::string string, std::map<char, int>& mapa)
{
	bool czyPowtorka = false;
	for(std::map<std::string, std::map<char, int> >::iterator it = rezultaty.begin(); it != rezultaty.end(); ++it)
	{
		if(string.compare(it->first) == 0)
		{
			cout << "powtorka" << endl;

			for(std::map<char, int>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
			{
				mapa.insert(pair<char, int>(it2->first, it2->second));
			}

			czyPowtorka = true;
			break;

		}
	}

	if(czyPowtorka == false)
	{
		for(unsigned int i = 0; i < string.size(); ++i)
		{
			char ch = string[i];
			std::map<char, int>::iterator it = mapa.find(ch);

			if(it->first == ch)
			{
				it->second++;
			}

			else
			{
				mapa.insert(std::pair<char, int>(ch, 1));
			}
		}

		cout << "dopisuje nowe slowo" << endl;

		rezultaty.insert(pair<std::string, map<char, int>& >(string, mapa));
	}
}

