/*
 * ZliczZnaki.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef ZLICZZNAKI_HPP_
#define ZLICZZNAKI_HPP_
#include <string>
#include <map>

class ZliczZnaki
{
private:
	static std::map<std::string, std::map<char, int> > rezultaty;

public:
	ZliczZnaki();
	virtual ~ZliczZnaki();
	static void zlicz(std::string string, std::map<char, int>& mapa);
};

#endif /* ZLICZZNAKI_HPP_ */
