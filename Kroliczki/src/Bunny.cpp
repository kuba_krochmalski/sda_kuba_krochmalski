/*
 * Bunny.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "Bunny.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>

Bunny::Bunny(std::string name)
: mAge(0)
, mName(name)
{
	mSex = randomizeSex();
	mColor = randomizeColor();
	isMutant = randomizeMutant();
}

Bunny::Bunny(std::string name, Color color)
: mColor(color)
, mAge(0)
, mName(name)
{
	mSex = randomizeSex();
	isMutant = randomizeMutant();
}

bool Bunny::isIsMutant() const
{
	return isMutant;
}

void Bunny::setIsMutant(bool isMutant)
{
	this->isMutant = isMutant;
}

Bunny::Color Bunny::getColor() const
{
	return Bunny::mColor;
}

const std::string& Bunny::getName() const
{
	return mName;
}

unsigned int Bunny::getAge() const
{
	return mAge;
}

Bunny::Sex Bunny::getSex() const
{
	return Bunny::mSex;
}

Bunny::~Bunny()
{
	// TODO Auto-generated destructor stub
}

Bunny::Sex Bunny::randomizeSex()
{
	int random = rand()%2;

	if(random == 0)
	{
		return Bunny::Male;
	}
	else
	{
		return Bunny::Female;
	}
}
Bunny::Color Bunny::randomizeColor()
{
	int random = rand()%4;

	switch(random)
	{
	case 0:
		return Bunny::Black;
		break;
	case 1:
		return Bunny::White;
		break;
	case 2:
		return Bunny::Spotted;
		break;
	case 3:
		return Bunny::Gray;
		break;
	default:
		return Bunny::Black;
	}
}
bool Bunny::randomizeMutant()
{
	int random = rand()%100;

	if (random == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

