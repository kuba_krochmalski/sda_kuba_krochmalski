/*
 * Bunny.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef BUNNY_HPP_
#define BUNNY_HPP_
#include <string>

class Bunny
{
public:
	enum Sex
	{
		Male,
		Female
	};

	enum Color
	{
		Black,
		White,
		Spotted,
		Gray
	};

private:
	Sex mSex;
	Color mColor;
	unsigned int mAge;
	std::string mName;
	bool isMutant;

	Sex randomizeSex();
	Color randomizeColor();
	bool randomizeMutant();

public:
	Bunny(std::string name);
	Bunny(std::string name, Color color);
	virtual ~Bunny();

	bool isIsMutant() const;
	void setIsMutant(bool isMutant);
	Color getColor() const;
	const std::string& getName() const;
	Sex getSex() const;

	bool happyBirthday();
	unsigned int getAge() const;
};

#endif /* BUNNY_HPP_ */
