#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
	string rzym = "XIX";
	int arab = 0;

	//cout << "Podaj liczbe jak prawdziwy Rzymianin: " << flush;
	//getline(cin, rzym);

	if (rzym.empty())
	{
		cout << "Koniec" << endl;
		return 0;
	}

	for (unsigned int i=0; i<rzym.length(); ++i)
	{
		if (rzym[i]=='I')
		{
			if ((rzym[i+1]=='V')||(rzym[i+1]=='X')||(rzym[i+1]=='L')||(rzym[i+1]=='C')||(rzym[i+1]=='D'))
			{
				arab -= 1;
			}
			else
			{
				arab +=1;
			}
		}

		else if (rzym[i] == 'V')
		{
			if ((rzym[i+1]=='X')||(rzym[i+1]=='L')||(rzym[i+1]=='C')||(rzym[i+1]=='D'))
			{
				arab -= 5;
			}
			else
			{
				arab +=5;
			}
		}

		else if (rzym[i] == 'X')
		{
			if ((rzym[i + 1] == 'L') || (rzym[i + 1] == 'C')|| (rzym[i + 1] == 'D'))
			{
				arab -= 10;
			}
			else
			{
				arab += 10;
			}
		}

		else if (rzym[i] == 'L')
		{
			if ((rzym[i + 1] == 'C') || (rzym[i + 1] == 'D'))
			{
				arab -= 50;
			}
			else
			{
				arab += 50;
			}
		}

		else if (rzym[i] == 'C')
		{
			if (rzym[i + 1] == 'D')
			{
				arab -= 100;
			}
			else
			{
				arab += 100;
			}
		}

		else if (rzym[i] == 'D')
		{
			arab += 500;
		}

	}

	cout << "Po rzymsku: " << rzym << endl;
	cout << "Po arabsku: " << arab << endl;

	return 0;
}
