//============================================================================
// Name        : MagiczneKwadraty.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
	int wiersze = 6;
	int kolumny = 6;
	int suma = 111;

	if(wiersze != kolumny)
	{
		cout << "To nie jest kwadrat!" << endl;
		return -1;
	}

	int kwadrat[wiersze][kolumny] = {{6, 32, 3, 34, 35, 1},
									 {7, 11, 27, 28, 8, 30},
									 {19, 14, 16, 15, 23, 24},
									 {18, 20, 22, 21, 17, 13},
									 {25, 29, 10, 9, 26, 12},
									 {36, 5, 33, 4, 2, 31}};

	for (int i = 0; i < wiersze; ++i)
	{
		for (int k = 0; k < kolumny; ++k)
		{
			cout << kwadrat[i][k] << " " << flush;
		}

		cout << endl;
	}

	bool czyMagiczny = true;

	for (int i = 0; i < wiersze; ++i) // sprawdzenie wierszy
	{
		int pomocniczaSuma = 0;

		for (int k = 0; k < kolumny; ++k)
		{
			pomocniczaSuma += kwadrat[i][k];

			if((k == kolumny - 1) && (pomocniczaSuma != suma))
			{
				czyMagiczny = false;
			}
		}
	}

	for (int k = 0; k < kolumny; ++k) // sprawdzenie kolumn
	{
		int pomocniczaSuma = 0;

		for (int i = 0; i < wiersze; ++i)
		{
			pomocniczaSuma += kwadrat[i][k];

			if((i == wiersze - 1) && (pomocniczaSuma != suma))
			{
				czyMagiczny = false;
			}
		}
	}

	for (int i = 0, pomocniczaSuma = 0; i < wiersze; ++i) // sprawdzenie jednej przekatnej
	{
		pomocniczaSuma += kwadrat[i][i];

		if ((i == wiersze - 1) && (pomocniczaSuma != suma))
		{
			czyMagiczny = false;
		}
	}

	for (int i = 0, pomocniczaSuma = 0; i < wiersze; ++i) // sprawdzenie drugiej przekatnej
	{
		pomocniczaSuma += kwadrat[wiersze - 1 - i][i];

		if ((i == wiersze - 1) && (pomocniczaSuma != suma))
		{
			czyMagiczny = false;
		}
	}

	if(czyMagiczny == true)
	{
		cout << "Kwadrat jest magiczny!" << endl;
	}

	else
	{
		cout << "Kwadrat nie jest magiczny." << endl;
	}

	return 0;
}
