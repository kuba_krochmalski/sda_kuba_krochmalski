/*
 * CKalendarz.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "CKalendarz.hpp"

CKalendarz::CKalendarz()
{
	// TODO Auto-generated constructor stub

}

CKalendarz::~CKalendarz()
{
	// TODO Auto-generated destructor stub
}

bool CKalendarz::liczbaDni(int miesiac, int iloscDni, bool przestepny)
{
	if(miesiac == 1 && iloscDni < 32 && iloscDni > 0) return true;
	else if(miesiac == 2 && iloscDni < 29 && iloscDni > 0 && przestepny == 0) return true;
	else if(miesiac == 2 && iloscDni < 30 && iloscDni > 0 && przestepny == 1) return true;
	else if(miesiac == 3 && iloscDni < 32 && iloscDni > 0) return true;
	else if(miesiac == 4 && iloscDni < 31 && iloscDni > 0) return true;
	else if(miesiac == 5 && iloscDni < 32 && iloscDni > 0) return true;
	else if(miesiac == 6 && iloscDni < 31 && iloscDni > 0) return true;
	else if(miesiac == 7 && iloscDni < 32 && iloscDni > 0) return true;
	else if(miesiac == 8 && iloscDni < 32 && iloscDni > 0) return true;
	else if(miesiac == 9 && iloscDni < 31 && iloscDni > 0) return true;
	else if(miesiac == 10 && iloscDni < 32 && iloscDni > 0) return true;
	else if(miesiac == 11 && iloscDni < 31 && iloscDni > 0) return true;
	else if(miesiac == 12 && iloscDni < 32 && iloscDni > 0) return true;

	else
	{
		return false;
	}
}

bool CKalendarz::czyDataPoprawna(int dzien, int miesiac, int rok)
{
	if(rok < 0 || miesiac < 1 || miesiac > 12)
	{
		return false;
	}

	else if(czyPrzestepny(rok) == true)
	{
		return liczbaDni(miesiac, dzien, 1);
	}

	else
	{
		return liczbaDni(miesiac, dzien, 0);
	}
}

bool CKalendarz::czyPrzestepny(int rok)
{
	if((rok%4 == 0 && rok%100 != 0) || rok%400 == 0)
	{
		return true;
	}

	else
	{
		return false;
	}
}

