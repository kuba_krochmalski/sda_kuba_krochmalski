/*
 * CKalendarz.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef CKALENDARZ_HPP_
#define CKALENDARZ_HPP_

class CKalendarz
{
public:
	CKalendarz();
	virtual ~CKalendarz();

	bool liczbaDni(int miesiac, int iloscDni, bool przestepny);
	bool czyDataPoprawna(int dzien, int miesiac, int rok);
	bool czyPrzestepny(int rok);
};

#endif /* CKALENDARZ_HPP_ */
