/*
 * CMatematyka.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "CMatematyka.hpp"

CMatematyka::CMatematyka()
{
	// TODO Auto-generated constructor stub

}

CMatematyka::~CMatematyka()
{
	// TODO Auto-generated destructor stub
}

long long CMatematyka::silnia(int x)
{
	if(x == 0)
	{
		return 1;
	}
	else
	{
		return silnia(x - 1) * x;
	}
}

long long CMatematyka::fibonacci(int n)
{
	if(n == 1)
	{
		return 0;
	}

	else if(n == 2)
	{
		return 1;
	}

	else
	{
		return fibonacci(n - 1) + fibonacci(n - 2);
	}
}

int CMatematyka::NWD(int x, int y)
{
	if(x == y)
	{
		return x;
	}

	else if(x > y)
	{
		x = x - y;
		return NWD(x, y);
	}

	else
	{
		y = y - x;
		return NWD(x, y);
	}
}

