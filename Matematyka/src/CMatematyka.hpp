/*
 * CMatematyka.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef CMATEMATYKA_HPP_
#define CMATEMATYKA_HPP_

class CMatematyka
{
public:
	CMatematyka();
	virtual ~CMatematyka();

	long long silnia(int x);
	long long fibonacci(int n);
	int NWD(int x, int y);
};

#endif /* CMATEMATYKA_HPP_ */
