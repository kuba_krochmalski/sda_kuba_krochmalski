//============================================================================
// Name        : Matematyka.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "CMatematyka.hpp"
#include "CKalendarz.hpp"
#include "gtest/gtest.h"

TEST(Kalendarz, czyDataPoprawna)
{
	CKalendarz kal;

	EXPECT_EQ(true, kal.czyDataPoprawna(29, 2, 2004));
	EXPECT_EQ(false, kal.czyDataPoprawna(29, 2, 2005));
	EXPECT_EQ(false, kal.czyDataPoprawna(34, -1, -100));
	EXPECT_EQ(true, kal.czyDataPoprawna(31, 1, 1991));
}

int main(int argc, char **argv)
{

	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();

}

//int main()
//{
//	CMatematyka mat;
//
//	cout << mat.silnia(5) << endl;
//
//	cout << mat.fibonacci(20) << endl;
//
//	cout << mat.NWD(1989, 867) << endl;
//	cout << "-------------------" << endl;
//
//	CKalendarz kal;
//
//	if(kal.czyDataPoprawna(29, 2, 2005) == true)
//	{
//		cout << "Data poprawna" << endl;
//	}
//
//	else
//	{
//		cout << "Data niepoprawna" << endl;
//	}
//
//
//
//	return 0;
//}
