/*
 * CFigura.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef CFIGURA_HPP_
#define CFIGURA_HPP_
#include "Kolor.hpp"

class CFigura
{
protected:
	Kolor::Kolor mKolor;

public:
	CFigura(Kolor::Kolor kolor);
	virtual ~CFigura();

	virtual void wypisz()=0;
};

#endif /* CFIGURA_HPP_ */
