/*
 * CKolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "CKolo.hpp"
#include <iostream>
#include "Geometria.hpp"

Geometria::CKolo::CKolo(Kolor::Kolor kolor, float promien)
: CFigura(kolor)
, mPromien(promien)
{
	// TODO Auto-generated constructor stub

}

Geometria::CKolo::~CKolo()
{
	// TODO Auto-generated destructor stub
}

void Geometria::CKolo::wypisz()
{
	std::cout << "Promien kola: " << mPromien << std::endl;
	std::cout << "Kolor kola: " << Kolor::convertToString(mKolor) << std::endl;
}

