/*
 * CKolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef CKOLO_HPP_
#define CKOLO_HPP_
#include "CFigura.hpp"

class CKolo : public CFigura
{
private:
	float mPromien;

public:
	CKolo(Kolor::Kolor kolor, float promien);
	virtual ~CKolo();

	void wypisz();
};

#endif /* CKOLO_HPP_ */
