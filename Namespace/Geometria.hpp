/*
 * Geometria.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef GEOMETRIA_HPP_
#define GEOMETRIA_HPP_

namespace Geometria
{
	class CFigura
	{
	protected:
		Kolor::Kolor mKolor;

	public:
		CFigura(Kolor::Kolor kolor);
		virtual ~CFigura();

		virtual void wypisz()=0;
	};

	class CKolo : public CFigura
	{
	private:
		float mPromien;

	public:
		CKolo(Kolor::Kolor kolor, float promien);
		virtual ~CKolo();

		void wypisz();
	};

	class Kwadrat : public CFigura
	{
	private:
		float mBok;

	public:
		Kwadrat(Kolor::Kolor kolor, float bok);
		virtual ~Kwadrat();

		void wypisz();
	};
}





#endif /* GEOMETRIA_HPP_ */
