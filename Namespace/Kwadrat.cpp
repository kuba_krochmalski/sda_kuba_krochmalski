/*
 * Kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kwadrat.hpp"
#include <iostream>
#include "Geometria.hpp"

Geometria::Kwadrat::Kwadrat(Kolor::Kolor kolor, float bok)
: CFigura(kolor)
, mBok(bok)
{
	// TODO Auto-generated constructor stub

}

Geometria::Kwadrat::~Kwadrat()
{
	// TODO Auto-generated destructor stub
}

void Geometria::Kwadrat::wypisz()
{
	std::cout << "Bok kwadratu: " << mBok << std::endl;
	std::cout << "Kolor kwadratu: " << Kolor::convertToString(mKolor) << std::endl;
}

