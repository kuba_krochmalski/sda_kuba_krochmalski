/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_
#include "CFigura.hpp"

class Kwadrat : public CFigura
{
private:
	float mBok;

public:
	Kwadrat(Kolor::Kolor kolor, float bok);
	virtual ~Kwadrat();

	void wypisz();
};

#endif /* KWADRAT_HPP_ */
