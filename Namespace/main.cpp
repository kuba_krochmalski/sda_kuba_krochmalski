/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "Kolor.hpp"
#include "CKolo.hpp"
#include "Kwadrat.hpp"
#include "Geometria.hpp"

int main()
{
	Geometria::CKolo k(Kolor::czerwony, 2.5);
	Geometria::Kwadrat kw(Kolor::niebieski, 3.4);

	Geometria::CFigura* figura = &k;
	figura->wypisz();

	std::cout << "--------------------" << std::endl;

	figura = &kw;
	figura->wypisz();




	return 0;
}



