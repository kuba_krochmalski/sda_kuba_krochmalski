//============================================================================
// Name        : OdwroconaSilnia.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
	double liczba;

	cout << "Podaj liczbe: " << flush;
	cin >> liczba;

	double dzielnik = 2.0;
	double iloraz = liczba;

	while(iloraz > 1.0)
	{
		iloraz = iloraz / dzielnik;
		++dzielnik;
	}

	if (iloraz == 1)
	{
		cout << "To jest: " << dzielnik - 1 << "!" << endl;
	}

	else
	{
		cout << "NONE" << endl;
	}

	return 0;
}
