/*
 * CData.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef CDATA_HPP_
#define CDATA_HPP_

class CData
{
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;

public:
	CData();
	CData(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	virtual ~CData();
	void wypisz();
	void przesunRok(int x);
	void przesunMiesiac(int x);
	void przesunDzien(int x);
	CData podajRoznice(CData data);
	bool czyMniejsza(CData innaData);

	unsigned int getDzien() const
	{
		return mDzien;
	}

	void setDzien(unsigned int dzien)
	{
		if (dzien < 1)
		{
			mDzien = 1;
		}

		else if (dzien > 31)
		{
			mDzien = 31;
		}

		else
		{
			mDzien = dzien;
		}
	}

	unsigned int getMiesiac() const
	{
		return mMiesiac;
	}

	void setMiesiac(unsigned int miesiac)
	{
		if (miesiac < 1)
		{
			mMiesiac = 1;
		}

		else if (miesiac > 12)
		{
			mMiesiac = 12;
		}

		else
		{
			mMiesiac = miesiac;
		}
	}

	unsigned int getRok() const
	{
		return mRok;
	}

	void setRok(unsigned int rok)
	{
		if (rok < 1900)
		{
			mRok = 1900;
		}

		else if (rok > 2017)
		{
			mRok = 2017;
		}

		else
		{
			mRok = rok;
		}
	}
};

#endif /* CDATA_HPP_ */
