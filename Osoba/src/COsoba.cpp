/*
 * COsoba.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "COsoba.hpp"
#include <iostream>
using namespace std;

COsoba::COsoba()
{
	mImie = "Jan";
	mNazwisko = "Nowak";
	mDataUrodzenia.setDzien(1);
	mDataUrodzenia.setMiesiac(1);
	mDataUrodzenia.setRok(1987);
	plec = mezczyzna;
	PESEL[0] = 8;
	PESEL[1] = 7;
	PESEL[2] = 0;
	PESEL[3] = 1;
	PESEL[4] = 0;
	PESEL[5] = 1;
	PESEL[6] = 0;
	PESEL[7] = 1;
	PESEL[8] = 2;
	PESEL[9] = 3;
	PESEL[10] = 4;

	sprawdzPoprawnosc(PESEL);
}

COsoba::COsoba(std::string imie, std::string nazwisko,
		std::string pesel, Plec plec)
{
	mImie = imie;
	mNazwisko = nazwisko;
	this -> plec = plec;
	PESEL[0] = pesel[0] - '0';
	PESEL[1] = pesel[1] - '0';
	PESEL[2] = pesel[2] - '0';
	PESEL[3] = pesel[3] - '0';
	PESEL[4] = pesel[4] - '0';
	PESEL[5] = pesel[5] - '0';
	PESEL[6] = pesel[6] - '0';
	PESEL[7] = pesel[7] - '0';
	PESEL[8] = pesel[8] - '0';
	PESEL[9] = pesel[9] - '0';
	PESEL[10] = pesel[10] - '0';

	unsigned int dzien = PESEL[4] * 10 + PESEL[5];
	unsigned int miesiac = PESEL[2] * 10 + PESEL[3];
	unsigned int rok = PESEL[0] * 10 + PESEL[1] + 1900;

	mDataUrodzenia.setDzien(dzien);
	mDataUrodzenia.setMiesiac(miesiac);
	mDataUrodzenia.setRok(rok);

	sprawdzPoprawnosc(PESEL);
}

COsoba::~COsoba()
{
	// TODO Auto-generated destructor stub
}

void COsoba::wypisz()
{
	cout << "Imie: " << mImie << endl;
	cout << "Nazwisko: " << mNazwisko << endl;
	mDataUrodzenia.wypisz();
	cout << "PESEL: " << flush;

	for (int i = 0; i < 11; ++i)
	{
		cout << PESEL[i] << flush;
	}
	cout << endl;

	if (plec == mezczyzna)
	{
		cout << "Plec: mezczyzna" << endl;
	}
	else
	{
		cout << "Plec: kobieta" << endl;
	}
}

void COsoba::sprawdzPoprawnosc(int tab[])
{
	int wagi[] = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
	int suma = 0;

	for (int i = 0; i < 10; ++i)
	{
		suma += wagi[i]*tab[i];
	}

	int kontrol = 10 - (suma % 10);

	if (kontrol != tab[10])
	{
		cout << "Bledny numer PESEL!" << endl;
		for (int i = 0; i < 11; ++i)
		{
			PESEL[i] = 0;
		}
	}

	else
	{
		cout << "Poprawny PESEL!" << endl;
	}
}

bool COsoba::czyStarszy(COsoba innaOsoba)
{
	if (mDataUrodzenia.czyMniejsza(innaOsoba.getDataUrodzenia()))
	{
		cout << mImie << " jest starszy." << endl;
		return true;
	}
	else
	{
		cout << mImie << " jest mlodszy." << endl;
		return false;
	}
}

