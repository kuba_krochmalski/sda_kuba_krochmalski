/*
 * COsoba.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef COSOBA_HPP_
#define COSOBA_HPP_
#include <string>
#include "CData.hpp"

class COsoba
{
public:
	enum Plec
	{
		mezczyzna,
		kobieta
	};

private:
	std::string mImie;
	std::string mNazwisko;
	CData mDataUrodzenia;
	Plec plec;
	int PESEL[11];


public:
	COsoba();
	COsoba(std::string imie, std::string nazwisko, std::string pesel, Plec plec);
	virtual ~COsoba();
	void wypisz();
	void sprawdzPoprawnosc(int tab[]);
	bool czyStarszy(COsoba innaOsoba);

	const CData& getDataUrodzenia() const
	{
		return mDataUrodzenia;
	}

	const std::string& getImie() const
	{
		return mImie;
	}

	void setImie(const std::string& imie)
	{
		mImie = imie;
	}

	const std::string& getNazwisko() const
	{
		return mNazwisko;
	}

	void setNazwisko(const std::string& nazwisko)
	{
		mNazwisko = nazwisko;
	}

	const int* getPesel() const
	{
		return PESEL;
	}

	void setPesel(int x0, int x1, int x2, int x3, int x4,
			int x5, int x6, int x7, int x8, int x9, int x10)
	{
		PESEL[0] = x0;
		PESEL[1] = x1;
		PESEL[2] = x2;
		PESEL[3] = x3;
		PESEL[4] = x4;
		PESEL[5] = x5;
		PESEL[6] = x6;
		PESEL[7] = x7;
		PESEL[8] = x8;
		PESEL[9] = x9;
		PESEL[10] = x10;

		sprawdzPoprawnosc(PESEL);

		unsigned int dzien = x4*10 + x5;
		unsigned int miesiac = x2*10 + x3;
		unsigned int rok = x0*10 + x1 + 1900;

		mDataUrodzenia.setDzien(dzien);
		mDataUrodzenia.setMiesiac(miesiac);
		mDataUrodzenia.setRok(rok);

	}

	Plec getPlec() const
	{
		return plec;
	}

	void setPlec(Plec plec)
	{
		this->plec = plec;
	}
};

#endif /* COSOBA_HPP_ */
