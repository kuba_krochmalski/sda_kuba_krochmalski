/*
 * CPesel.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "CPesel.hpp"
#include <iostream>
#include <string>
#include <sstream>

CPesel::CPesel()
{
	for (int i = 0; i < 11; ++i)
	{
		mNumer[i] = 1;
	}
}

CPesel::CPesel(int tab[])
{
	for (int i = 0; i < 11; ++i)
	{
		mNumer[i] = tab[i];
	}
}

CPesel::~CPesel()
{
	// TODO Auto-generated destructor stub
}

void CPesel::wypisz()
{
	for (int i = 0; i < 11; ++i)
	{
		std::cout << mNumer[i] << std::flush;
	}
	std::cout << std::endl;
}

void CPesel::sprawdzPoprawnoscNumeru(int tab[])
{
	int wagi[] = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
	int suma = 0;

	for (int i = 0; i < 10; ++i)
	{
		suma += wagi[i]*tab[i];
	}

	int kontrol = 10 - (suma % 10);

	if (kontrol != tab[10])
	{
		std::cout << "Bledny numer PESEL!" << std::endl;

		for (int i = 0; i < 11; ++i)
		{
			mNumer[i] = 0;
		}
	}

	else
	{
		std::cout << "Poprawny PESEL!" << std::endl;
	}
}

CData CPesel::pobierzDate()
{
	unsigned int dzien = mNumer[4]*10 + mNumer[5];
	unsigned int miesiac = mNumer[2]*10 + mNumer[3];
	unsigned int rok = 1900 + mNumer[0]*10 + mNumer[1];

	CData pobranaData(dzien, miesiac, rok);

	return pobranaData;
}

std::string CPesel::naString()
{
	ostringstream ss;

	std::ss << mNumer[0] << mNumer[1] << mNumer[2] << mNumer[3] << mNumer[4] << mNumer[5] <<
			mNumer[6] << mNumer[7] << mNumer[8] << mNumer[9] << mNumer[10] << std::endl;

	std::string peselStr = ss.str();

	return peselStr;
}

