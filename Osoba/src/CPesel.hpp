/*
 * CPesel.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CPESEL_HPP_
#define CPESEL_HPP_
#include <string>
#include "CData.hpp"

class CPesel
{
private:
	int mNumer[11];

public:
	CPesel();
	CPesel(int tab[]);
	virtual ~CPesel();
	void wypisz();
	void sprawdzPoprawnoscNumeru(int tab[]);
	CData pobierzDate();
	std::string naString();

	const int* getNumer() const
	{
		return mNumer;
	}

	void setNumer(std::string numer)
	{
		for (int i = 0; i < 11; ++i)
		{
			mNumer[i] = numer[i] - '0';
		}

		sprawdzPoprawnoscNumeru(mNumer);

	}
};

#endif /* CPESEL_HPP_ */
