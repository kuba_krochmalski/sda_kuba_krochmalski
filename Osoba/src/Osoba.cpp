//============================================================================
// Name        : Osoba.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "COsoba.hpp"
#include "CPesel.hpp"

using namespace std;

int main()
{
	COsoba jan;
	jan.wypisz();
	cout << "---------------------" << endl;

//	jan.setPesel(9,1,0,1,3,1,0,9,7,9,8);
//	jan.wypisz();
//	cout << "---------------------" << endl;

	COsoba ja("Kuba", "Krochmalski", "91013109798", COsoba::mezczyzna);
	ja.wypisz();
	ja.czyStarszy(jan);
	cout << "---------------------" << endl;

	CPesel pesel1;
	pesel1.wypisz();

	int tablica[] = {9,1,0,1,3,1,0,9,7,9,8};

	CPesel pesel2(tablica);
	pesel2.wypisz();
	cout << "---------------------" << endl;
	pesel2.pobierzDate().wypisz();

	string napis = pesel2.naString();

	cout << napis << endl;


	return 0;
}
