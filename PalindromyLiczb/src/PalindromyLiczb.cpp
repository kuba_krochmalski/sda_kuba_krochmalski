#include <iostream>
#include <string>
#include <sstream>

using namespace std;

string naStringa(int x);
int sprawdz(string napis);

int main()
{

	int sprawdzana;
	int szukana;
	int najwieksza=10000;

	for (int i=100; i<1000; ++i)
	{
		for (int k=100; k<1000; ++k)
		{
			sprawdzana = i*k;
			naStringa(sprawdzana);
			if (sprawdz(naStringa(sprawdzana)) == 0)
			{
				szukana = i*k;

				if (szukana > najwieksza)
				{
					najwieksza = szukana;
				}
			}
		}
	}

	cout << najwieksza << endl;
	return 0;
}

string naStringa(int x)
{
	ostringstream ss;
	ss << x;
	string napis = ss.str();
	return napis;
}

int sprawdz(string napis)
{
	if (napis.length() == 5)
	{
		for (unsigned int i=0; i<napis.length(); ++i)
		{
			if (napis[0] == napis[napis.length()-1])
			{
				if (napis[1] == napis[napis.length()-2])
				{
					return 0;
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
		return 1;
	}

	if (napis.length() == 6)
		{
			for (unsigned int i=0; i<napis.length(); ++i)
			{
				if (napis[0] == napis[napis.length()-1])
				{
					if (napis[1] == napis[napis.length()-2])
					{
						if (napis[2] == napis[napis.length()-3])
						{
							return 0;
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			return 1;
		}
	return 1;
}
