/*
 * CDzikieZwierze.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CDZIKIEZWIERZE_HPP_
#define CDZIKIEZWIERZE_HPP_
#include <string>

class CDzikieZwierze
{
protected:
	std::string mImie;

public:
	CDzikieZwierze();
	CDzikieZwierze(std::string imie);
	virtual ~CDzikieZwierze();
	virtual void dajGlos() = 0;
};

#endif /* CDZIKIEZWIERZE_HPP_ */
