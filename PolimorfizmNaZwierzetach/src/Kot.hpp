/*
 * Kot.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef KOT_HPP_
#define KOT_HPP_
#include "CDzikieZwierze.hpp"

class Kot : public virtual CDzikieZwierze
{
public:
	Kot();
	Kot(std::string imie);
	virtual ~Kot();
	virtual void dajGlos();
};

#endif /* KOT_HPP_ */
