/*
 * KotPiec.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef KOTPIEC_HPP_
#define KOTPIEC_HPP_
#include "Pies.hpp"
#include "Kot.hpp"

class KotPiec : public Pies, public Kot
{
public:
	KotPiec();
	KotPiec(std::string imie);
	virtual ~KotPiec();
	void dajGlos();
};

#endif /* KOTPIEC_HPP_ */
