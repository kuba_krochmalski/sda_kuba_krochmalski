/*
 * Pies.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef PIES_HPP_
#define PIES_HPP_
#include "CDzikieZwierze.hpp"

class Pies : public virtual CDzikieZwierze
{
public:
	Pies();
	Pies(std::string imie);
	virtual ~Pies();
	virtual void dajGlos();
};

#endif /* PIES_HPP_ */
