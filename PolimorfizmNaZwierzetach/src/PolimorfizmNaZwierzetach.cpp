#include <iostream>
using namespace std;

#include "CDzikieZwierze.hpp"
#include "Pies.hpp"
#include "Kot.hpp"
#include "KotPiec.hpp"


int main()
{
	CDzikieZwierze* tablica[5];
	tablica[0] = new Pies("Reksio");
	tablica[1] = new Pies("Szarik");
	tablica[2] = new Pies("�ajka");
	tablica[3] = new Kot("Garfield");
	tablica[4] = new Kot("Filemon");

	for (int i = 0; i < 5; ++i)
	{
		tablica[i]->dajGlos();
	}

	for (int i = 0; i < 5; ++i)
	{
		delete tablica[i];
	}

	KotPiec kp;
	kp.dajGlos();

	return 0;
}
