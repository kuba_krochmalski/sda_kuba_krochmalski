/*
 * CDoorSensor.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CDOORSENSOR_HPP_
#define CDOORSENSOR_HPP_
#include "CSensor.hpp"

class CDoorSensor : public CSensor
{
public:
	CDoorSensor();
	virtual ~CDoorSensor();
	bool check();
};

#endif /* CDOORSENSOR_HPP_ */
