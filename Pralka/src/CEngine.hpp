/*
 * CEngine.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CENGINE_HPP_
#define CENGINE_HPP_
#include "CMachine.hpp"

class CEngine : public CMachine
{
private:
	int mRotation;

public:
	CEngine();
	virtual ~CEngine();
};

#endif /* CENGINE_HPP_ */
