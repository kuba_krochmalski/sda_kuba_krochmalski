/*
 * CMachine.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CMACHINE_HPP_
#define CMACHINE_HPP_

class CMachine
{
public:
	CMachine();
	virtual ~CMachine();

	void turnOn();
	void turnOff();

};

#endif /* CMACHINE_HPP_ */
