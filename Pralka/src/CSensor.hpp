/*
 * CSensor.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CSENSOR_HPP_
#define CSENSOR_HPP_

class CSensor
{
public:
	CSensor();
	virtual ~CSensor();
	virtual bool check() = 0;
};

#endif /* CSENSOR_HPP_ */
