/*
 * CTempSensor.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CTEMPSENSOR_HPP_
#define CTEMPSENSOR_HPP_
#include "CSensor.hpp"

class CTempSensor : public CSensor
{
public:
	CTempSensor();
	virtual ~CTempSensor();
	bool check();
};

#endif /* CTEMPSENSOR_HPP_ */
