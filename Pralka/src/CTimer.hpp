/*
 * CTimer.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CTIMER_HPP_
#define CTIMER_HPP_

class CTimer
{
private:
	int mValue;
	int mDuration;

public:
	CTimer();
	virtual ~CTimer();

	void setDuration(int duration)
	{
		mDuration = duration;
	}

	void start();
	int count();

	int getDuration() const
	{
		return mDuration;
	}

	int getValue() const
	{
		return mValue;
	}

};

#endif /* CTIMER_HPP_ */
