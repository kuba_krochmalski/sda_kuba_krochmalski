/*
 * CWashOption.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CWASHOPTION_HPP_
#define CWASHOPTION_HPP_

class CWashOption
{
private:
	int mWashSelection;

public:
	CWashOption();
	virtual ~CWashOption();

	int getWashSelection() const
	{
		return mWashSelection;
	}
};

#endif /* CWASHOPTION_HPP_ */
