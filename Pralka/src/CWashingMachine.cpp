/*
 * CWashingMachine.cpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#include "CWashingMachine.hpp"
#include "CWashOption.hpp"
#include "CEngine.hpp"
#include "CTimer.hpp"
#include <iostream>

CWashingMachine::CWashingMachine()
: mWashTime(1)
, mRinseTime(1)
, mSpinTime(1)
{
	// TODO Auto-generated constructor stub

}

CWashingMachine::~CWashingMachine()
{
	// TODO Auto-generated destructor stub
}

void CWashingMachine::mainWM()
{
	CWashOption washOption;

	int option = washOption.getWashSelection();

	switch(option)
	{
	case 1:
		standardWash();
		break;
	case 2:
		twiceRinse();
		break;
	case 3:
		spin();
		break;
	default:
		break;
	}
}

void CWashingMachine::standardWash()
{
	std::cout << "Standard Wash" << std::endl;
}

void CWashingMachine::twiceRinse()
{
	std::cout << "Twice Rinse" << std::endl;
}

void CWashingMachine::spin()
{
	std::cout << "Spin" << std::endl;

	CEngine engine;
	engine.turnOn();

	CTimer timer;
	timer.setDuration(60*60);

	timer.start();
	int time = timer.getValue();
	int duration = timer.getDuration();

	while(time != duration)
	{
		time = timer.count();
	}

	engine.turnOff();
}


