/*
 * CWashingMachine.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CWASHINGMACHINE_HPP_
#define CWASHINGMACHINE_HPP_

class CWashingMachine
{
private:
	int mWashTime;
	int mRinseTime;
	int mSpinTime;

public:
	CWashingMachine();
	virtual ~CWashingMachine();

	void mainWM();
	void wash();
	void rinse();
	void spin();
	void fill();
	void empty();
	void standardWash();
	void twiceRinse();

};

#endif /* CWASHINGMACHINE_HPP_ */
