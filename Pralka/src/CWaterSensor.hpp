/*
 * CWaterSensor.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CWATERSENSOR_HPP_
#define CWATERSENSOR_HPP_
#include "CSensor.hpp"

class CWaterSensor : public CSensor
{
private:
	int mCurrentLevel;
	int mDesiredLevel;

public:
	CWaterSensor();
	virtual ~CWaterSensor();
	bool check();
};

#endif /* CWATERSENSOR_HPP_ */
