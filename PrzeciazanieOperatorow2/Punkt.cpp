/*
 * Punkt.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Punkt.hpp"
#include <iostream>

Punkt::Punkt()
: mX(0.0)
, mY(0.0)
{
	// TODO Auto-generated constructor stub

}

Punkt::Punkt(float x, float y)
: mX(x)
, mY(y)
{

}

float Punkt::getX() const
{
	return mX;
}

void Punkt::setX(float x)
{
	mX = x;
}

float Punkt::getY() const
{
	return mY;
}

void Punkt::setY(float y)
{
	mY = y;
}

Punkt::~Punkt()
{
	// TODO Auto-generated destructor stub
}


Punkt Punkt::operator+ (const float& liczba) const
{
	Punkt wynik;
	wynik.setX(mX + liczba);
	wynik.setY(mY + liczba);

	return wynik;
}

Punkt Punkt::operator+ (const Punkt& punkt) const
{
	Punkt wynik;
	wynik.setX(mX + punkt.getX());
	wynik.setY(mY + punkt.getY());

	return wynik;
}

void Punkt::wypisz()
{
	std::cout << "[" << this->getX() << ", " << this->getY() << "]" << std::endl;
}

Punkt operator+ ( const float& liczba, const Punkt& punkt)
{
	return punkt + liczba;
}

Punkt operator*(const float& liczba, const Punkt& punkt)
{
	return punkt * liczba;
}

Punkt Punkt::operator*(const float& liczba) const
{
	Punkt wynik;
	wynik.setX(mX * liczba);
	wynik.setY(mY * liczba);

	return wynik;
}

Punkt Punkt::operator*(const Punkt& punkt) const
{
	Punkt wynik;
	wynik.setX(mX * punkt.getX());
	wynik.setY(mY * punkt.getY());

	return wynik;
}

Punkt& Punkt::operator++()
{
	this->setX(mX+1);
	this->setY(mY+1);

	return *this;
}

Punkt Punkt::operator++(int)
{
	Punkt tmp(*this);

	this->operator++();

	return tmp;
}


