/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
private:
	float mX;
	float mY;

public:
	Punkt();
	Punkt(float x, float y);
	virtual ~Punkt();
	void wypisz();

	float getX() const;
	void setX(float x);
	float getY() const;
	void setY(float y);

	Punkt operator+ (const float& liczba) const;
	Punkt operator+ (const Punkt& punkt) const;

	Punkt operator* (const float& liczba) const;
	Punkt operator* (const Punkt& punkt) const;

	Punkt& operator++();
	Punkt operator++(int);

};

Punkt operator+ (const float& liczba, const Punkt& punkt);
Punkt operator* (const float& liczba, const Punkt& punkt);



#endif /* PUNKT_HPP_ */
