/*
 * Zbior.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Zbior.hpp"
#include <iostream>

Zbior::Zbior()
: mPunkty(0)
, mSize(0)
{
	// TODO Auto-generated constructor stub

}

Zbior::~Zbior()
{
	if(mSize != 0)
	{
		for (unsigned int i = 0; i < mSize; ++i)
		{
			delete mPunkty[i];
		}

		delete [] mPunkty;
		mSize = 0;
	}
}

void Zbior::wypisz()
{
	std::cout << "ZBIOR: " << std::endl;

	for (unsigned int i = 0; i < mSize; ++i)
	{
		mPunkty[i]->wypisz();
	}

	std::cout << std::endl;
}


void Zbior::zwieksz()
{
	if (mSize == 0)
	{
		mPunkty = new Punkt*[++mSize];
	}
	else
	{
		Punkt** tmp = new Punkt*[++mSize];

		for (unsigned int i = 0; i < mSize - 1; ++i) // kopiowanie ze starej tablicy
		{
			tmp[i] = mPunkty[i];
		}

		delete[] mPunkty;
		mPunkty = tmp;
	}
}

void Zbior::operator+ (const Punkt& punkt)
{
	zwieksz();

	mPunkty[mSize-1] = new Punkt(punkt.getX(), punkt.getY());
}

Punkt Zbior::operator[](std::size_t idx)
{
	if(idx < mSize)
	{
		return Punkt(*mPunkty[idx]);
	}
	else
	{
		return Punkt(0, 0);
	}
}

std::size_t Zbior::getSize() const
{
	return mSize;
}

void Zbior::operator+ (Zbior& zbior)
{
	for (unsigned int i = 0; i < zbior.getSize(); ++i)
	{
		*this + zbior[i];
	}
}

Zbior::operator bool()
{
	return (this->getSize() != 0);
}

