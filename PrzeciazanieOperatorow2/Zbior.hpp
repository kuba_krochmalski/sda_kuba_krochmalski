/*
 * Zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIOR_HPP_
#define ZBIOR_HPP_

#include "Punkt.hpp"
#include <cstddef>

class Zbior
{
private:
	Punkt** mPunkty;
	std::size_t mSize;
	void zwieksz();

public:
	Zbior();
	virtual ~Zbior();
	void wypisz();

	void operator+ (const Punkt& punkt);
	Punkt operator[](std::size_t idx);
	void operator+ (Zbior& zbior);
	operator bool();

	std::size_t getSize() const;
};

#endif /* ZBIOR_HPP_ */
