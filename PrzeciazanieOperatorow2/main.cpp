/*
 * main.cpp

 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Punkt.hpp"
#include "Zbior.hpp"
#include <iostream>

int main()
{
	Punkt pkt1(1.5, 2.2);
	//pkt1.wypisz();

	Punkt pkt2 = ++pkt1 + 0.5;
	//pkt2.wypisz();

	Punkt pkt3 = 0.5 + pkt1++;
	//pkt3.wypisz();

	Punkt pkt4 = pkt1 + pkt2;
	//pkt4.wypisz();
	//std::cout << "---------------" << std::endl;

	Punkt pkt5 = pkt1 * 2.0;
	//pkt5.wypisz();

	Punkt pkt6 = 0.0 * pkt1;
	//pkt6.wypisz();

	Punkt pkt7 = pkt1 * pkt2;
	//pkt7.wypisz();
	//std::cout << "---------------" << std::endl;

	Zbior zbior;

	zbior + pkt1;
	zbior + pkt2;
	zbior + pkt3;
	zbior + pkt4;

	zbior.wypisz();

	Zbior zbiornik;

	zbiornik + Punkt(5, 5);
	zbiornik + Punkt(10, 10);

	//zbiornik.wypisz();

	zbior + zbiornik;
	zbior.wypisz();

	if(zbior)
	{
		std::cout << "Prawidlowy" << std::endl;
	}
	else
	{
		std::cout << "Nieprawidlowy" << std::endl;
	}

	return 0;
}



