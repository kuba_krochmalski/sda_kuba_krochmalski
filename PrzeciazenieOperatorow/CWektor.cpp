/*
 * CWektor.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "CWektor.hpp"
#include <iostream>

CWektor::CWektor(Punkt p1, Punkt p2)
: mP1(p1)
, mP2(p2)
{
	// TODO Auto-generated constructor stub

}

CWektor::~CWektor()
{
	// TODO Auto-generated destructor stub
}

void CWektor::wypisz()
{
	std::cout << "P1.x = " << mP1.getX() << " P1.y = " << mP1.getY() << " P2.x = " << mP2.getX() << " P2.y = " << mP2.getY() << std::endl;
}

void CWektor::operator!()
{
	Punkt temp = mP1;
	mP1 = mP2;
	mP2 = temp;
}

const Punkt& CWektor::getP1() const
{
	return mP1;
}

void CWektor::setP1(const Punkt& p1)
{
	mP1 = p1;
}

const Punkt& CWektor::getP2() const
{
	return mP2;
}

void CWektor::setP2(const Punkt& p2)
{
	mP2 = p2;
}

bool CWektor::operator==(const CWektor& wektor) const
{
	if(wektor.getP1() == this->getP1() && wektor.getP2() == this->getP2())
	{
		return true;
	}

	else
	{
		return false;
	}

}
