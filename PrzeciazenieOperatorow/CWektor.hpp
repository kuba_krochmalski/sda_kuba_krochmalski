/*
 * CWektor.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef CWEKTOR_HPP_
#define CWEKTOR_HPP_

#include "Punkt.hpp"

class CWektor
{
private:
	Punkt mP1;
	Punkt mP2;

public:
	CWektor(Punkt p1, Punkt p2);
	virtual ~CWektor();
	void wypisz();

	void operator!();

	bool operator==(const CWektor& wektor) const;

	const Punkt& getP1() const;
	void setP1(const Punkt& p1);
	const Punkt& getP2() const;
	void setP2(const Punkt& p2);
};

#endif /* CWEKTOR_HPP_ */
