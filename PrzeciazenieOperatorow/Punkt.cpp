/*
 * Punkt.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Punkt.hpp"

Punkt::Punkt()
: mX(0)
, mY(0)
{
	// TODO Auto-generated constructor stub

}

Punkt::Punkt(int x, int y)
: mX(x)
, mY(y)
{

}

int Punkt::getX() const
{
	return mX;
}

void Punkt::setX(int x)
{
	mX = x;
}

int Punkt::getY() const
{
	return mY;
}

void Punkt::setY(int y)
{
	mY = y;
}

Punkt::~Punkt()
{
	// TODO Auto-generated destructor stub
}

Punkt Punkt::operator* (const int multi) const
{
	Punkt wynik;
	wynik.setX(mX*multi);
	wynik.setY(mY*multi);

	return wynik;
}

Punkt operator* (const int multi, const Punkt& pkt)
{
	return pkt*multi;
}

bool Punkt::operator==(const Punkt& pkt) const
{
	if(pkt.getX() == this->getX() && pkt.getY() == this->getY())
	{
		return true;
	}

	else
	{
		return false;
	}
}

