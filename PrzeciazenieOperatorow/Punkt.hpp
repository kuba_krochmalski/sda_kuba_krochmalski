/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
private:
	int mX;
	int mY;

public:
	Punkt();
	Punkt(int x, int y);
	virtual ~Punkt();

	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);

	Punkt operator* (const int multi) const;

	bool operator==(const Punkt& pkt) const;
};

Punkt operator* (const int multi, const Punkt& pkt);

#endif /* PUNKT_HPP_ */
