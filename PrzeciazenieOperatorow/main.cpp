/*
 * main.cpp

 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include "CWektor.hpp"
#include <iostream>

int main()
{

	CWektor w1(Punkt(3, 4), Punkt(6, 6));
	CWektor w2(Punkt(-4, 10), Punkt(8, -7));

	w1.wypisz();
	w2.wypisz();

	Punkt pkt(3, 6);
	CWektor w3(pkt, pkt*3); // deklaracja w klasie
	CWektor w4(pkt, 2*pkt); // deklaracja poza klas�

	w3.wypisz();
	w4.wypisz();

	!w4;

	w4.wypisz();
	CWektor w5(Punkt(3, 4), Punkt(6, 6));

	if(w1 == w2)
	{
		std::cout << "Sa rowne" << std::endl;
	}
	else
	{
		std::cout << "Nie sa rowne" << std::endl;
	}

	if (w1 == w5)
	{
		std::cout << "Sa rowne" << std::endl;
	}
	else
	{
		std::cout << "Nie sa rowne" << std::endl;
	}





	return 0;
}


