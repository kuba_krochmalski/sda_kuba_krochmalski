/*
 * CKolorowyPunkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "CKolorowyPunkt.hpp"
#include <iostream>
#include <string>

CKolorowyPunkt::CKolorowyPunkt()
: CPunkt(1, 1)
, kolor(bialy)
{
	// TODO Auto-generated constructor stub

}

CKolorowyPunkt::CKolorowyPunkt(int x, int y, Kolor kol)
: CPunkt(x, y)
, kolor(kol)
{

}

CKolorowyPunkt::~CKolorowyPunkt()
{
	// TODO Auto-generated destructor stub
}

void CKolorowyPunkt::wypiszPunkt()
{
	std::string jakiKolor;

	switch(kolor)
	{
	case czerwony:
		jakiKolor = "czerwony";
		break;
	case niebieski:
		jakiKolor = "niebieski";
		break;
	case zielony:
		jakiKolor = "zielony";
		break;
	case bialy:
		jakiKolor = "bialy";
		break;
	}

	std::cout << "[" << mX << ", " << mY << "]  " << jakiKolor << std::endl;
}

