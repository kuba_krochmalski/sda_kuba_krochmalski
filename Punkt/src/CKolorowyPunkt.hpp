/*
 * CKolorowyPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef CKOLOROWYPUNKT_HPP_
#define CKOLOROWYPUNKT_HPP_
#include "CPunkt.hpp"

class CKolorowyPunkt : public CPunkt
{
public:
	enum Kolor
	{
		czerwony,
		zielony,
		niebieski,
		bialy
	};

private:
	Kolor kolor;

public:
	CKolorowyPunkt();
	CKolorowyPunkt(int x, int y, Kolor kol);
	virtual ~CKolorowyPunkt();
	void wypiszPunkt();
};

#endif /* CKOLOROWYPUNKT_HPP_ */
