/*
 * CPunkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "CPunkt.hpp"
#include <iostream>
#include <cmath>

CPunkt::CPunkt()
: mX(1)
, mY(1)
{
	// TODO Auto-generated constructor stub

}

CPunkt::CPunkt(int x, int y)
: mX(x)
, mY(y)
{

}

CPunkt::~CPunkt()
{
	// TODO Auto-generated destructor stub
}

int CPunkt::getX() const
{
	return mX;
}

void CPunkt::setX(int x)
{
	mX = x;
}

int CPunkt::getY() const
{
	return mY;
}

void CPunkt::setY(int y)
{
	mY = y;
}

void CPunkt::wypiszPunkt()
{
	std::cout << "[" << mX << ", " << mY << "]" << std::endl;
}

void CPunkt::przesun(CPunkt inny)
{
	mX += inny.getX();
	mY += inny.getY();
}

void CPunkt::przesun(int x, int  y)
{
	mX += x;
	mY += y;
}

void CPunkt::przesunX(int x)
{
	przesun(x, 0);
}

void CPunkt::przesunY(int y)
{
	przesun(0, y);
}

void CPunkt::obliczOdleglosc(CPunkt inny)
{
	double odleglosc = sqrt((mX - inny.getX())*(mX - inny.getX())+(mY - inny.getY())*(mY - inny.getY()));

	std::cout << "Odleglosc miedzy punktami: " << odleglosc << std::endl;
}

void CPunkt::obliczOdleglosc(int x, int y)
{
	double odleglosc = sqrt((mX - x)*(mX - x)+(mY - y)*(mY - y));

	std::cout << "Odleglosc: " << odleglosc << std::endl;
}

