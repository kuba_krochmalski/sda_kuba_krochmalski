/*
 * CPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef CPUNKT_HPP_
#define CPUNKT_HPP_

class CPunkt
{
protected:
	int mX;
	int mY;

public:
	CPunkt();
	CPunkt(int x, int y);
	virtual ~CPunkt();

	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);

	void wypiszPunkt();
	void przesun(CPunkt inny);
	void przesun(int x, int  y);
	void przesunX(int x);
	void przesunY(int y);
	void obliczOdleglosc(CPunkt inny);
	void obliczOdleglosc(int x, int y);


};

#endif /* CPUNKT_HPP_ */
