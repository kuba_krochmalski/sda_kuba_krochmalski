//============================================================================
// Name        : Punkt.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "CPunkt.hpp"
#include "CKolorowyPunkt.hpp"

using namespace std;

int main()
{
	CPunkt p1(3, 4);
	p1.wypiszPunkt();
	std::cout << "-----------" << std::endl;

	CPunkt p2(1, 5);
	p2.wypiszPunkt();
	p2.przesun(p1);
	p2.wypiszPunkt();
	p2.przesun(-2, -2);
	p2.wypiszPunkt();
	p2.przesunX(1);
	p2.wypiszPunkt();
	p2.przesunY(1);
	p2.wypiszPunkt();
	std::cout << "-----------" << std::endl;

	p1.obliczOdleglosc(p2);
	p1.obliczOdleglosc(4, 5);
	std::cout << "-----------" << std::endl;

	CKolorowyPunkt kolp;
	kolp.wypiszPunkt();

	CKolorowyPunkt kol(2, 5, CKolorowyPunkt::zielony);
	kol.wypiszPunkt();








	return 0;
}
