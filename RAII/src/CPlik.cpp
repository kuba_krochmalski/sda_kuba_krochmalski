/*
 * CPlik.cpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#include "CPlik.hpp"

CPlik::CPlik(std::string filename, int flagi, int level)
: mPlik(filename.c_str(), std::ofstream::out)
, mFlags(flagi)
, mLevel(level)
{


}

CPlik::~CPlik()
{
	mPlik.close();

}

void CPlik::Log(std::string message, int flaga, int msgLevel)
{
	time_t t;
	t = time(NULL);
	struct tm* current = localtime(&t);

	if((msgLevel <= mLevel) && ((flaga & mFlags) != 0))
	{
		mPlik << "[" << current->tm_mday << "." << current->tm_mon + 1 << "."
				<< current->tm_year + 1900 << "r. " << current->tm_hour << ":"
				<< current->tm_min << ":" << current->tm_sec << "] " << message
				<< std::endl;
	}

}
