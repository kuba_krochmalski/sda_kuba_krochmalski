/*
 * CPlik.hpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef CPLIK_HPP_
#define CPLIK_HPP_
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>

class CPlik
{
private:
	std::fstream mPlik;
	int mFlags; // 1-program(001), 2-dzialania(010), 3-oba(011)
	int mLevel; // 1-wazne, 2-sredniowazne, 3-niewazne

public:
	CPlik(std::string filename, int flagi, int level);
	virtual ~CPlik();

	void Log(std::string message, int flaga, int msgLevel);
};

#endif /* CPLIK_HPP_ */
