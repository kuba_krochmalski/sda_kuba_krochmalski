/*
 * Animal.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_
#include "Enemy.hpp"

class Animal : public Enemy
{
public:
	Animal();
	Animal(int x, int y, char symbol);
	virtual ~Animal();
	virtual void makeNoise() = 0;
};

#endif /* ANIMAL_HPP_ */
