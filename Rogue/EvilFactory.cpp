/*
 * EvilFactory.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "EvilFactory.hpp"
#include "Undead.hpp"
#include "Skeleton.hpp"
#include "Zombie.hpp"
#include "Animal.hpp"
#include "Rat.hpp"
#include "Snail.hpp"

EvilFactory::EvilFactory()
{
	// TODO Auto-generated constructor stub

}

EvilFactory::~EvilFactory()
{
	// TODO Auto-generated destructor stub
}

Undead* EvilFactory::createUndead(UndeadType::UndeadID undead, int x, int y)
{
	switch(undead)
	{
	case UndeadType::skeleton:
		return new Skeleton(x, y);
		break;
	case UndeadType::zombie:
		return new Zombie(x, y);
		break;
	default:
		return 0;
	}
}

Animal* EvilFactory::createAnimal(AnimalType::AnimalID animal, int x, int y)
{
	switch (animal)
	{
	case AnimalType::rat:
		return new Rat(x, y);
		break;
	case AnimalType::snail:
		return new Snail(x, y);
		break;
	default:
		return 0;
	}
}

