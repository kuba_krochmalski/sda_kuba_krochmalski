/*
 * EvilFactory.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef EVILFACTORY_HPP_
#define EVILFACTORY_HPP_
#include "Undead.hpp"
#include "Animal.hpp"

namespace UndeadType
{
	enum UndeadID
	{
		skeleton, zombie
	};
}

namespace AnimalType
{
	enum AnimalID
	{
		rat, snail
	};
}

class EvilFactory
{
public:
	EvilFactory();
	virtual ~EvilFactory();
	static Undead* createUndead(UndeadType::UndeadID undead, int x, int y);
	static Animal* createAnimal(AnimalType::AnimalID animal, int x, int y);

};

#endif /* EVILFACTORY_HPP_ */
