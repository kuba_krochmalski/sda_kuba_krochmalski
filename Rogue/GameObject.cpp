/*
 * GameObject.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "GameObject.hpp"

GameObject::GameObject()
: mX(0)
, mY(0)
, mSymbol('?')
{
	// TODO Auto-generated constructor stub

}

GameObject::GameObject(int x, int y, char symbol)
: mX(x)
, mY(y)
, mSymbol(symbol)
{

}

char GameObject::getSymbol() const
{
	return mSymbol;
}

int GameObject::getX() const
{
	return mX;
}

void GameObject::setX(int x)
{
	mX = x;
}

int GameObject::getY() const
{
	return mY;
}

void GameObject::setY(int y)
{
	mY = y;
}

GameObject::~GameObject()
{
	// TODO Auto-generated destructor stub
}

