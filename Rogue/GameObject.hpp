/*
 * GameObject.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef GAMEOBJECT_HPP_
#define GAMEOBJECT_HPP_

class GameObject
{
private:
	int mX;
	int mY;
	char mSymbol;
	
public:
	GameObject();
	GameObject(int x, int y, char symbol);
	virtual ~GameObject();

	char getSymbol() const;

	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);

	virtual void update() = 0;
};

#endif /* GAMEOBJECT_HPP_ */
