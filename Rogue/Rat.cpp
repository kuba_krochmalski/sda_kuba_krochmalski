/*
 * Rat.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Rat.hpp"
#include <iostream>

Rat::Rat()
: Animal()
{
	// TODO Auto-generated constructor stub

}

Rat::Rat(int x, int y)
: Animal(x, y, 'Q')
{

}

Rat::~Rat()
{
	// TODO Auto-generated destructor stub
}

void Rat::makeNoise()
{
	std::cout << "Grrrrrr!" << std::endl;
}

