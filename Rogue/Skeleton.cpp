/*
 * Skeleton.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Skeleton.hpp"
#include <iostream>

Skeleton::Skeleton()
: Undead()
{
	// TODO Auto-generated constructor stub
}

Skeleton::Skeleton(int x, int y)
: Undead(x, y, 'S')
{

}

Skeleton::~Skeleton()
{
	// TODO Auto-generated destructor stub
}

void Skeleton::speak()
{
	std::cout << "I'm a skeleton!" << std::endl;
}

