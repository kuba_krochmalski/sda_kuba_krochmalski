/*
 * Skeleton.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SKELETON_HPP_
#define SKELETON_HPP_
#include "Undead.hpp"

class Skeleton : public Undead
{
public:
	Skeleton();
	Skeleton(int x, int y);
	virtual ~Skeleton();
	void speak();
};

#endif /* SKELETON_HPP_ */
