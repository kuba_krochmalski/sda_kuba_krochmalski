/*
 * Snail.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Snail.hpp"
#include <iostream>

Snail::Snail()
: Animal()
{
	// TODO Auto-generated constructor stub

}

Snail::Snail(int x, int y)
: Animal(x, y, '@')
{

}

Snail::~Snail()
{
	// TODO Auto-generated destructor stub
}

void Snail::makeNoise()
{
	std::cout << "<snail's noise>" << std::endl;
}

