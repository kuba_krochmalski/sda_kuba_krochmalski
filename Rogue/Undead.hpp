/*
 * Undead.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef UNDEAD_HPP_
#define UNDEAD_HPP_
#include "Enemy.hpp"

class Undead : public Enemy
{
public:
	Undead();
	Undead(int x, int y, char symbol);
	virtual ~Undead();
	virtual void speak() = 0;
};

#endif /* UNDEAD_HPP_ */
