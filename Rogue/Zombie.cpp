/*
 * Zombie.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Zombie.hpp"
#include <iostream>

Zombie::Zombie()
: Undead()
{
	// TODO Auto-generated constructor stub
}

Zombie::Zombie(int x, int y)
: Undead(x, y, 'Z')
{

}

Zombie::~Zombie()
{
	// TODO Auto-generated destructor stub
}

void Zombie::speak()
{
	std::cout << "Braaaaaaaaaain..." << std::endl;
}

