#include "EvilFactory.hpp"

int main()
{

	Undead* skeleton1 = EvilFactory::createUndead(UndeadType::skeleton, 2, 3);
	skeleton1->speak();

	Undead* zombie1 = EvilFactory::createUndead(UndeadType::zombie, 7, -8);
	zombie1->speak();

	Animal* snail1 = EvilFactory::createAnimal(AnimalType::snail, 7, -14);
	snail1->makeNoise();


	return 0;
}


