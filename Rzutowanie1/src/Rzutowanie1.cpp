//============================================================================
// Name        : Rzutowanie1.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

using namespace std;

int policz_literke(const char* napis, char literka)
{
    int n = 0;
    int i = 0;
    //char* c = const_cast<char*>(napis);
    while (napis[i])
    {
        if (napis[i++] == literka)
        {
        	n++;
        }
    }
    return n;
}

int main()
{
	cout << "alabama" << policz_literke("alabama", 'a') << endl;

	return 0;
}
