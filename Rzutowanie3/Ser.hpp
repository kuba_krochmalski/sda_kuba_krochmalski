/*
 * Ser.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_HPP_
#define SER_HPP_

class Ser
{
protected:
	double mCena;

public:
	Ser();
	Ser(double cena);
	virtual ~Ser();
	virtual void podajCene() = 0;

};

#endif /* SER_HPP_ */
