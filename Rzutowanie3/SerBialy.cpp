/*
 * SerBialy.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "SerBialy.hpp"
#include <iostream>
#include <string>

SerBialy::SerBialy()
: Ser()
, mTluszcz(chudy)
{
	// TODO Auto-generated constructor stub

}

SerBialy::SerBialy(double cena, Tluszcz tluszcz)
: Ser(cena)
, mTluszcz(tluszcz)
{
	// TODO Auto-generated constructor stub

}

SerBialy::~SerBialy()
{
	// TODO Auto-generated destructor stub
}

void SerBialy::podajCene()
{
	std::cout << "SerBialy::podajCene() " << mCena << std::endl;
}
void SerBialy::podajRodzaj()
{
	std::cout << toString() << std::endl;
}

std::string SerBialy::toString()
{
	if (mTluszcz == chudy)
	{
		return "Jestem chudym serem.";
	}
	else if (mTluszcz == poltlusty)
	{
		return "Jestem serem poltlustym.";
	}
	else if (mTluszcz == tlusty)
	{
		return "Jestem tlustym serem.";
	}
	else
	{
		return "Nie wiem jakim serem jestem.";
	}
}

