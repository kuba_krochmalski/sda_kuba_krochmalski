/*
 * SerBialy.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERBIALY_HPP_
#define SERBIALY_HPP_
#include "Ser.hpp"
#include <string>

class SerBialy : public Ser
{
public:
	enum Tluszcz
	{
		chudy,
		poltlusty,
		tlusty
	};

private:
	Tluszcz mTluszcz;

public:
	SerBialy();
	SerBialy(double cena, Tluszcz tluszcz);
	virtual ~SerBialy();
	void podajCene();
	void podajRodzaj();
	std::string toString();
};

#endif /* SERBIALY_HPP_ */
