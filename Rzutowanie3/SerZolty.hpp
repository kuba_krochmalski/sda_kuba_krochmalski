/*
 * SerZolty.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERZOLTY_HPP_
#define SERZOLTY_HPP_
#include "Ser.hpp"

class SerZolty : public Ser
{
private:
	int mWiek;

public:
	SerZolty();
	SerZolty(double cena, int wiek);
	virtual ~SerZolty();
	void podajWiek();
	void podajCene();
};

#endif /* SERZOLTY_HPP_ */
