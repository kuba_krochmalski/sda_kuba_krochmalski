#include "Ser.hpp"
#include "SerZolty.hpp"
#include "SerBialy.hpp"
#include <iostream>

void sertuj(Ser** tab, int rozmiar)
{
	SerBialy** bialaTab = new SerBialy* [rozmiar];
	SerZolty** zoltaTab = new SerZolty* [rozmiar];
	int ileBialych = 0;
	int ileZoltych = 0;

	SerBialy* bialyTmp;
	SerZolty* zoltyTmp;

	for(int i = 0; i < rozmiar; ++i)
	{
		if(bialyTmp = dynamic_cast<SerBialy*>(tab[i]))
		{
			bialaTab[ileBialych] = bialyTmp;
			ileBialych++;
			continue;
		}

		else if(zoltyTmp = dynamic_cast<SerZolty*>(tab[i]))
		{
			zoltaTab[ileZoltych] = zoltyTmp;
			ileZoltych++;
		}
		else
		{
			std::cout << "Blad!" << std::endl;
		}
	}

	std::cout << "BIALE!" << std::endl;
	for(int i = 0; i < ileBialych; ++i)
	{
		bialaTab[i]->podajCene();
		bialaTab[i]->podajRodzaj();
	}
	std::cout << std::endl;

	std::cout << "ZOLTE!" << std::endl;
	for(int i = 0; i < ileZoltych; ++i)
	{
		zoltaTab[i]->podajCene();
		zoltaTab[i]->podajWiek();
	}
	std::cout << std::endl;

	delete[] bialaTab;
	delete[] zoltaTab;
}

int main()
{
	Ser* sery[8];

	sery[0] = new SerZolty(11.30, 0);
	sery[1] = new SerBialy(9.99, SerBialy::chudy);
	sery[2] = new SerZolty(12.50, 1);
	sery[3] = new SerBialy(8.99, SerBialy::poltlusty);
	sery[4] = new SerZolty(17.25, 1);
	sery[5] = new SerBialy(10.0, SerBialy::tlusty);
	sery[6] = new SerZolty(25.99, 2);
	sery[7] = new SerBialy(10.50, SerBialy::chudy);

	sertuj(sery, 8);

	for(int i = 0; i < 8; ++i)
	{
		delete sery[i];
	}

	return 0;
}



