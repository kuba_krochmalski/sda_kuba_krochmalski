/*
 * Fiat.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef FIAT_HPP_
#define FIAT_HPP_
#include "Samochod.hpp"

class Fiat : public Samochod
{
public:
	Fiat(std::string kol, double x);
	virtual ~Fiat();
	void podajKolor();
	void podajMarke();
	void podajPojemnosc();
};

#endif /* FIAT_HPP_ */
