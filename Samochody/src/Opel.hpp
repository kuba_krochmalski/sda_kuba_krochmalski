/*
 * Opel.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef OPEL_HPP_
#define OPEL_HPP_
#include "Samochod.hpp"

class Opel : public Samochod
{
public:
	Opel(std::string kol, double x);
	virtual ~Opel();
	void podajKolor();
	void podajMarke();
	void podajPojemnosc();

};

#endif /* OPEL_HPP_ */
