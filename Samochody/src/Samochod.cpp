/*
 * Samochod.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Samochod.hpp"
#include <iostream>

Samochod::Samochod(std::string kol, std::string mar, double x)
: kolor(kol)
, marka(mar)
, pojemnosc(x)
{
	// TODO Auto-generated constructor stub

}

Samochod::~Samochod()
{
	std::cout << "Niszcze Samochod" << std::endl;
}

void Samochod::podajKolor()
{
	std::cout << "Kolor: " << kolor << std::endl;
}
void Samochod::podajMarke()
{
	std::cout << "Marka: " << marka << std::endl;
}
void Samochod::podajPojemnosc()
{
	std::cout << "Pojemnosc: " << pojemnosc << std::endl;
}

