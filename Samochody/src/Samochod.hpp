/*
 * Samochod.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef SAMOCHOD_HPP_
#define SAMOCHOD_HPP_
#include <string>
#include <iostream>

class Samochod
{
protected:
	std::string kolor;
	std::string marka;
	double pojemnosc;

public:
	Samochod(std::string kol, std::string mar, double x);
	virtual ~Samochod();
	virtual void podajKolor() = 0;
	virtual void podajMarke() = 0;
	virtual void podajPojemnosc() = 0;

};

#endif /* SAMOCHOD_HPP_ */
