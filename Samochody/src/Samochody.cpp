//============================================================================
// Name        : Samochody.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Fiat.hpp"
#include "Opel.hpp"

using namespace std;

int main()
{
	Samochod *tablica[3];

	tablica[0] = new Fiat("czerwony", 1.0);
	tablica[1] = new Fiat("zielony", 1.2);
	tablica[2] = new Opel("bialy", 1.4);

	for (int i = 0; i < 3; ++i)
	{
		tablica[i]->podajKolor();
		tablica[i]->podajMarke();
		tablica[i]->podajPojemnosc();
		std::cout << "----------------------" << std::endl;
	}

	delete tablica[0];
	std::cout << "----------------------" << std::endl;
	delete tablica[1];
	std::cout << "----------------------" << std::endl;
	delete tablica[2];

	return 0;
}
