//============================================================================
// Name        : Static.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int licznik()
{
	static int a;
	a++;
	return a;
}

int main()
{
	cout << licznik() << endl;
	cout << licznik() << endl;
	cout << licznik() << endl;
	cout << licznik() << endl;
	cout << licznik() << endl;

	return 0;
}
