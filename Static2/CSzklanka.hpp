/*
 * CSzklanka.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef CSZKLANKA_HPP_
#define CSZKLANKA_HPP_

class CSzklanka
{
private:
	static int cena;
	int mPojemnosc;

public:
	CSzklanka(int pojemnosc);
	virtual ~CSzklanka();

	static int getCena();
	static void setCena(int nowaCena);

	int getPojemnosc() const;
	void setPojemnosc(int pojemnosc);
};

#endif /* CSZKLANKA_HPP_ */
