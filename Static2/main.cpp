/*
 * main.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include <iostream>
#include "CSzklanka.hpp"
#include "test.hpp"

void f()
{
	std::cout << "Cos main" << std::endl;
}

void p()
{
	f();
}

int main()
{
	f();
	p();
	cosPubliczne();

	std::cout << CSzklanka::getCena() << std::endl;
	CSzklanka::setCena(8);

	CSzklanka szklanka1(200);
	CSzklanka szklanka2(250);

	std::cout << szklanka1.getCena() << std::endl;
	szklanka1.setCena(10);
	std::cout << szklanka1.getCena() << std::endl;
	std::cout << szklanka2.getCena() << std::endl;

	return 0;
}


