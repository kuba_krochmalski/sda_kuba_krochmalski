/*
 * CStringCalculator.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "CStringCalculator.hpp"
#include <cstdlib>

CStringCalculator::CStringCalculator()
{
	// TODO Auto-generated constructor stub

}

CStringCalculator::~CStringCalculator()
{
	// TODO Auto-generated destructor stub
}

int CStringCalculator::add(std::string numbers)
{
	if(numbers.size() == 0)
	{
		return 0;
	}

	else if(numbers.find(",") == std::string::npos)
	{
		return std::atoi(numbers.c_str());
	}

	else
	{
		int comaPosition = numbers.find(",");
		int numberOne = std::atoi(numbers.substr(0, comaPosition).c_str());
		int numberTwo = std::atoi(numbers.substr(comaPosition+1, std::string::npos).c_str());

		return numberOne + numberTwo;
	}

}

