/*
 * CStringCalculator.hpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#ifndef CSTRINGCALCULATOR_HPP_
#define CSTRINGCALCULATOR_HPP_

#include <string>

class CStringCalculator
{
public:
	CStringCalculator();
	virtual ~CStringCalculator();

	int add(std::string numbers);
};

#endif /* CSTRINGCALCULATOR_HPP_ */
