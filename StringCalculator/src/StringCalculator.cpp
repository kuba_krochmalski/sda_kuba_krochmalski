#include "gtest/gtest.h"
#include "CStringCalculator.hpp"

TEST(StringCalculatorTest, EmptyString)
{
	CStringCalculator c;
	EXPECT_EQ(0, c.add(""));
}

TEST(StringCalculatorTest, OneNumber)
{
	CStringCalculator c;
	EXPECT_EQ(1, c.add("1"));
	EXPECT_EQ(123, c.add("123"));
}

TEST(StringCalculatorTest, TwoNumbers)
{
	CStringCalculator c;
	EXPECT_EQ(3, c.add("1,2"));
	EXPECT_EQ(128, c.add("121,7"));
	EXPECT_EQ(336, c.add("101,235"));
}

//TEST(StringCalculatorTest, UnknownNumbers)
//{
//	CStringCalculator c;
//	EXPECT_EQ(6, c.add("1,2,3"));
//	EXPECT_EQ(131, c.add("121,7,1,2"));
//	EXPECT_EQ(1484, c.add("101,235,56,419,79,143,170,215,66"));
//}

int main(int argc, char **argv)
{

	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();

}
