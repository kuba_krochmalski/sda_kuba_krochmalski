#include <iostream>

struct Czlowiek
{
	int dzienUr;
	int miesiacUr;
	int rokUr;

	Czlowiek(int dzien, int miesiac, int rok)
	: dzienUr(dzien)
	, miesiacUr(miesiac)
	, rokUr(rok)
	{
	}

	int podajWiek(int rok)
	{
		return rok - rokUr;
	}
};

struct Pracownik : Czlowiek
{
	float mPensja;
	int mStaz;

	Pracownik(int dzien, int miesiac, int rok, float pensja, int staz)
	: Czlowiek(dzien, miesiac, rok)
	, mPensja(pensja)
	, mStaz(staz)
	{
	}

	void wyliczPensje()
	{
		mPensja = mPensja*(1 + mStaz * 0.1);
	}

	float podajPensje()
	{
		return mPensja;
	}
};

class Student : Czlowiek
{
private:
	int nrInd;

public:
	Student(int dzien, int miesiac, int rok, int numer)
	: Czlowiek(dzien, miesiac, rok)
	, nrInd(numer)
	{

	}
	void bawSie()
	{
		std::cout << "Student nr: " << nrInd << " bawi sie." << std::endl;
	}
};

int main()
{
	Czlowiek kuba(31,1,1991);
	std::cout << "Wiek: " << kuba.podajWiek(2017) << std::endl;

	Pracownik jan(23,5,1990, 3000, 5);
	std::cout << "Pensja: " << jan.podajPensje() << std::endl;
	jan.wyliczPensje();
	std::cout << "Pensja: " << jan.podajPensje() << std::endl;

	Student michal(12, 4, 1994, 165429);
	michal.bawSie();



	return 0;
}


