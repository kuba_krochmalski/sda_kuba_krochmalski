#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

struct UserInputData
{
	int iloscLiczb;
	float min;
	float max;
	bool czyRosnaco;
};

UserInputData pobierzDane()
{
	UserInputData uid;

	std::cout << "Ile liczb? : " << std::flush;
	std::cin >> uid.iloscLiczb;
	std::cout << "Min? : " << std::flush;
	std::cin >> uid.min;
	std::cout << "Max? : " << std::flush;
	std::cin >> uid.max;
	std::cout << "Rosnaco czy malejaco? : " << std::flush;
	std::cin >> uid.czyRosnaco;

	return uid;
}

void wylosuj(float* tab, int ileLiczb, float min, float max)
{
	srand(time(NULL));

	for (int i = 0; i < ileLiczb; ++i)
	{
		tab[i] = rand()%int(max - min) + min;
		std::cout << tab[i] << std::endl;
	}
}

int main()
{
	UserInputData daneWejsciowe = pobierzDane();

	std::cout << "Ile: " << daneWejsciowe.iloscLiczb << std::endl;
	std::cout << "Min: " << daneWejsciowe.min << std::endl;
	std::cout << "Max: " << daneWejsciowe.max << std::endl;
	std::cout << "Czy rosnaco: " << daneWejsciowe.czyRosnaco << std::endl;

	float* tab = new float[daneWejsciowe.iloscLiczb];

	wylosuj(tab, daneWejsciowe.iloscLiczb, daneWejsciowe.min, daneWejsciowe.max);






	delete [] tab;

	return 0;
}

