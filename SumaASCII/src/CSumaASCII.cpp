/*
 * CSumaASCII.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "CSumaASCII.hpp"

CSumaASCII::CSumaASCII(std::string tekst)
: mTekst(tekst)
{
	mSuma = 0;

	for (unsigned int i = 0; i < mTekst.size(); ++i)
	{
		mSuma += int(mTekst[i]);
	}

}

CSumaASCII::~CSumaASCII()
{
	// TODO Auto-generated destructor stub
}

int CSumaASCII::sumuj()
{
	return mSuma;
}

