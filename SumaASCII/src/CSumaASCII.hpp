/*
 * CSumaASCII.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef CSUMAASCII_HPP_
#define CSUMAASCII_HPP_
#include <string>

class CSumaASCII
{
private:
	std::string mTekst;
	int mSuma;

public:
	CSumaASCII(std::string tekst);
	virtual ~CSumaASCII();

	int sumuj();
};

#endif /* CSUMAASCII_HPP_ */
