#include <iostream>
#include <string>

using namespace std;

template <int N, typename T>
void wypisz(T zmienna)
{
	for(int i = 0; i < N; ++i)
	{
		cout << zmienna;
	}

	cout << endl;
}

template<>
void wypisz<10, string>(string zmienna)
{
	cout << "Bajlando!" << endl;
}

int main()
{
	string test = "ZZ ";

	wypisz<10, string>(test);
	wypisz<12, string>(test);
	wypisz<20, int>(7);
	wypisz<7, float>(2.3);



	return 0;
}




