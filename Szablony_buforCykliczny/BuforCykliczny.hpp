/*
 * BuforCykliczny.hpp
 *
 *  Created on: 17.05.2017
 *      Author: RENT
 */

#ifndef BUFORCYKLICZNY_HPP_
#define BUFORCYKLICZNY_HPP_
#include <cstdlib>
#include <exception>
#include <iostream>

template <typename Typ>
class BuforCykliczny
{
private:
	struct Node
	{
		Typ value;
		Node* next;

		Node(Typ value)
		{
			this->value = value;
			next = 0;
		}

		void overwrite(Typ value)
		{
			this->value = value;
		}
	};

	Node* readPtr;
	Node* addPtr;

	size_t mSize;
	size_t mMaxSize;

public:
	BuforCykliczny(size_t Capacity)
	: readPtr(0)
	, addPtr(0)
	, mSize(0)
	, mMaxSize(Capacity)
	{
	}

	virtual ~BuforCykliczny()
	{

	}

	void add(const Typ& element)
	{
		if(mSize == 0)
		{
			Node* tmpPtr = new Node(element);
			addPtr = tmpPtr;
			readPtr = tmpPtr;
			tmpPtr->next = tmpPtr;
			mSize++;
		}
		else if(mSize < mMaxSize)
		{
			Node* tmpPtr = new Node(element);
			tmpPtr->next = addPtr->next;
			addPtr->next = tmpPtr;
			addPtr = tmpPtr;
			mSize++;
		}
		else if(mSize == mMaxSize)
		{
			addPtr = addPtr->next;
			addPtr->overwrite(element);
		}
		else
		{

		}
	}

	Typ& next()
	{
		if(readPtr == 0)
		{
			throw std::out_of_range("Poza zasiegiem!");
		}
		else
		{
			readPtr = readPtr->next;
			return readPtr->value;
		}
	}

	void clear();

	size_t size();
	size_t maxSize();





};

#endif /* BUFORCYKLICZNY_HPP_ */
