/*
 * main.cpp

 *
 *  Created on: 17.05.2017
 *      Author: RENT
 */

#include "BuforCykliczny.hpp"
#include <iostream>

using namespace std;

int main()
{
	BuforCykliczny<int> b(3);

	b.add(1);
	b.add(2);
	b.add(3);
	b.add(4);

	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;
	cout << b.next() << endl;

	return 0;
}


