/*
 * Pojemnik.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef POJEMNIK_HPP_
#define POJEMNIK_HPP_
#include <iostream>

template <typename Typ>
class Pojemnik
{
private:
	Typ mObiekt;
public:
	Pojemnik(Typ obiekt)
	{
		mObiekt = obiekt;
	}
	virtual ~Pojemnik()
	{

	}
	void setObiekt(Typ obiekt)
	{
		mObiekt = obiekt;
	}
	Typ getObiekt()
	{
		return mObiekt;
	}
	void wypisz()
	{
		std::cout << mObiekt << std::endl;
	}

};

#endif /* POJEMNIK_HPP_ */
