/*
 * main.cpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>
#include "Pojemnik.hpp"

int main()
{
	Pojemnik<int> pojemnikNaInty(7);
	pojemnikNaInty.wypisz();
	pojemnikNaInty.setObiekt(178);
	pojemnikNaInty.wypisz();

	Pojemnik<std::string> pojemnikNaStringi("kuku");
	pojemnikNaStringi.wypisz();
	pojemnikNaStringi.setObiekt("tralalala");
	pojemnikNaStringi.wypisz();

	Pojemnik<float>* wskaznikNaPojemnik = new Pojemnik<float>(5.45);
	wskaznikNaPojemnik->wypisz();

	return 0;
}



