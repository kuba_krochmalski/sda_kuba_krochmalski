/*
 * TablicaDynamiczna.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef TABLICADYNAMICZNA_HPP_
#define TABLICADYNAMICZNA_HPP_
#include <iostream>
#include <exception>

template <typename Typ>

class TablicaDynamiczna
{
private:
	Typ* tablica;
	int mSize;

public:
	TablicaDynamiczna()
	{
		tablica = 0;
		mSize = 0;
	}
	virtual ~TablicaDynamiczna()
	{
		clear();
	}

	void add(const Typ& nowy)
	{
		resize();
		tablica[mSize-1] = nowy;
	}
	int find(const Typ& szukany)
	{
		for(int i = 0; i < mSize; ++i)
		{
			if(tablica[i] == szukany)
			{
				return i;
			}
		}

		return -1; //nie znaleziono
	}
	void resize()
	{
		if(mSize == 0)
		{
			tablica = new Typ[++mSize];
		}
		else
		{
			Typ* tmp = new Typ[++mSize];

			for(int i = 0; i < mSize - 1; ++i)
			{
				tmp[i] = tablica[i];
			}

			delete [] tablica;
			tablica = tmp;
		}
	}
	int size()
	{
		return mSize;
	}
	bool empty()
	{
		if(mSize == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void remove(int pozycja)
	{
		if(pozycja < mSize && pozycja >= 0)
		{
			Typ* tmp = new Typ[mSize - 1];

			for (int i = 0; i < pozycja; ++i)
			{
				tmp[i] = tablica[i];
			}

			for (int i = pozycja; i < mSize - 1; ++i)
			{
				tmp[i] = tablica[i + 1];
			}

			--mSize;
			delete[] tablica;
			tablica = tmp;
		}
		else
		{
			throw std::out_of_range("Indeks poza zasiegiem!");
		}
	}
	void remove(const Typ& elem)
	{
		int pos = find(elem);

		if(pos > -1)
		{
			remove(pos);
		}
		else
		{
			throw std::invalid_argument("Nie znaleziono argumentu!");
		}
	}
	Typ get(int pozycja)
	{
		if(pozycja < mSize && pozycja >= 0)
		{
			return tablica[pozycja];
		}
		else
		{
			throw std::out_of_range("Indeks poza zasiegiem!");
		}
	}
	void clear()
	{
		delete[] tablica;
		tablica = 0;
		mSize = 0;
	}
	void show()
	{
		for(int i = 0; i < mSize; ++i)
		{
			std::cout << tablica[i] << std::endl;
		}
	}


};

#endif /* TABLICADYNAMICZNA_HPP_ */
