#include <iostream>
#include "TablicaDynamiczna.hpp"

int main()
{
	TablicaDynamiczna<char> tab;
	tab.add('a');
	tab.show();
	tab.add('b');
	tab.add('c');
	tab.add('d');
	tab.show();

	std::cout << "--------------" << std::endl;
	std::cout << tab.find('a') << std::endl;
	std::cout << tab.find('b') << std::endl;
	std::cout << tab.find('c') << std::endl;
	std::cout << "--------------" << std::endl;

	try
	{
		std::cout << tab.get(3) << std::endl;
	}

	catch(std::out_of_range& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	try
	{
		tab.remove(3);
	}

	catch (std::out_of_range& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	try
	{
		tab.remove('z');
	}

	catch (std::invalid_argument& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	return 0;
}



