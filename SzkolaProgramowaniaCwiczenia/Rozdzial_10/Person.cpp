/*
 * Person.cpp
 *
 *  Created on: 19 sty 2018
 *      Author: ezkroja
 */

#include "Person.h"
#include <cstring>
#include <iostream>

Person::Person(const std::string& nazwisko, const char* imie)
: mNazwisko{nazwisko}
{
	std::strcpy(mImie, imie);
}

void Person::show() const
{
	std::cout << mImie << " " << mNazwisko << std::endl;
}

void Person::formalShow() const
{
	std::cout << mNazwisko << " " << mImie << std::endl;
}

