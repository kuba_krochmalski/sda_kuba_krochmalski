/*
 * Person.h
 *
 *  Created on: 19 sty 2018
 *      Author: ezkroja
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <string>

class Person
{
private:
	static const int LIMIT = 256;
	std::string mNazwisko;
	char mImie[LIMIT];

public:
	Person()
	{
		mNazwisko = "";
		mImie[0] = '\0';
	}
	Person(const std::string& nazwisko, const char* imie = "noname");
	~Person() = default;
	void show() const;
	void formalShow() const;
};

#endif /* PERSON_H_ */
