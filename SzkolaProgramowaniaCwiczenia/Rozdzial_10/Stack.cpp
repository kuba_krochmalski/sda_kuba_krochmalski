/*
 * Stack.cpp
 *
 *  Created on: 19 sty 2018
 *      Author: ezkroja
 */

#include "Stack.h"
#include <iostream>

Stack::Stack()
: top(0)
{}

bool Stack::isEmpty() const
{
	return (top == 0);
}
bool Stack::isFull() const
{
	return (top == MAX);
}
bool Stack::push(const Customer& c)
{
	if(isFull())
	{
		std::cout << "Stack is full." << std::endl;
		return false;
	}
	else
	{
		array[top++] = c;
		return true;
	}
}
bool Stack::pop(Customer& c)
{
	if(isEmpty())
	{
		std::cout << "Stack is empty." << std::endl;
		c.name = "No value";
		c.payment = 0.0;
		return false;
	}
	else
	{
		c = array[--top];
		return true;
	}
}

