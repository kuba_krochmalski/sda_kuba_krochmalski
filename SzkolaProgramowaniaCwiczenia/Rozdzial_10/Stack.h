/*
 * Stack.h
 *
 *  Created on: 19 sty 2018
 *      Author: ezkroja
 */

#ifndef STACK_H_
#define STACK_H_
#include <string>

struct Customer
{
	std::string name;
	double payment;
};

class Stack
{
private:
	static const int MAX = 10;
	Customer array[MAX];
	int top;

public:
	Stack();
	~Stack() = default;
	bool isEmpty() const;
	bool isFull() const;
	bool push(const Customer& c);
	bool pop(Customer& c);
};

#endif /* STACK_H_ */
