/*
 * main.cpp
 *
 *  Created on: 19 sty 2018
 *      Author: ezkroja
 */

#include "Person.h"
#include "Stack.h"
#include <iostream>

int main()
{
	/*
	Person one;
	Person two("Staszek");
	Person three("Jacek", "Placek");

	one.show();
	one.formalShow();

	two.show();
	two.formalShow();

	three.show();
	three.formalShow();
	*/

	Stack customers;

	std::cout << customers.isEmpty() << std::endl;
	std::cout << customers.isFull() << std::endl;

	customers.push(Customer{"Jan", 12.50});
	customers.push(Customer{"Marcin", 4.70});
	customers.push(Customer{"Karolina", 8.90});
	customers.push(Customer{"Rafal", 6.40});

	std::cout << customers.isEmpty() << std::endl;
	std::cout << customers.isFull() << std::endl;

	double totalPayment = 0.0;

	Customer popped;
	customers.pop(popped);
	totalPayment += popped.payment;
	std::cout << "Dodaje " << popped.payment << std::endl;

	customers.pop(popped);
	totalPayment += popped.payment;
	std::cout << "Dodaje " << popped.payment << std::endl;

	customers.pop(popped);
	totalPayment += popped.payment;
	std::cout << "Dodaje " << popped.payment << std::endl;

	customers.pop(popped);
	totalPayment += popped.payment;
	std::cout << "Dodaje " << popped.payment << std::endl;

	customers.pop(popped);
	totalPayment += popped.payment;
	std::cout << "Dodaje " << popped.payment << std::endl;

	std::cout << customers.isEmpty() << std::endl;
	std::cout << customers.isFull() << std::endl;

	return 0;
}
