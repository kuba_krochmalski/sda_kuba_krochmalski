#include "ComplexNumber.h"

ComplexNumber::ComplexNumber(): mReal{0.0}, mImaginary{0.0} {}

ComplexNumber::ComplexNumber(double real, double imaginary)
: mReal(real), mImaginary(imaginary)
{}

void ComplexNumber::set(double real, double imaginary)
{
	mReal = real;
	mImaginary = imaginary;
}

double ComplexNumber::getReal() const
{
	return mReal;
}

double ComplexNumber::getImaginary() const
{
	return mImaginary;
}

ComplexNumber ComplexNumber::operator~() const
{
	return ComplexNumber{mReal, -mImaginary};
}

ComplexNumber ComplexNumber::operator+(const ComplexNumber& other) const
{
	return ComplexNumber{mReal + other.mReal, mImaginary + other.mImaginary};
}

ComplexNumber ComplexNumber::operator-(const ComplexNumber& other) const
{
	return ComplexNumber{mReal - other.mReal, mImaginary - other.mImaginary};
}

ComplexNumber ComplexNumber::operator*(const ComplexNumber& other) const
{
	return ComplexNumber{mReal * other.mReal - mImaginary * other.mImaginary,
						 mReal * other.mImaginary + mImaginary * other.mReal};
}

ComplexNumber ComplexNumber::operator*(double val) const
{
	return ComplexNumber{val * mReal, val * mImaginary};
}

ComplexNumber operator*(double val, const ComplexNumber& num)
{
	return num * val;
}

std::ostream& operator<<(std::ostream& out, const ComplexNumber& num)
{
	out << "(" << num.mReal << ", " << num.mImaginary << "i)";
	return out;
}
