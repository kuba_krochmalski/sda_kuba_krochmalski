#ifndef COMPLEXNUMBER_H_
#define COMPLEXNUMBER_H_

#include <iostream>

class ComplexNumber
{
public:
	ComplexNumber();
	ComplexNumber(double real, double imaginary);
	~ComplexNumber() = default;
	void set(double real, double imaginary);

	ComplexNumber operator~() const;
	ComplexNumber operator+(const ComplexNumber& other) const;
	ComplexNumber operator-(const ComplexNumber& other) const;
	ComplexNumber operator*(const ComplexNumber& other) const;
	ComplexNumber operator*(double val) const;
	friend ComplexNumber operator*(double val, const ComplexNumber& num);
	friend std::ostream& operator<<(std::ostream& out, const ComplexNumber& num);

	double getReal() const;
	double getImaginary() const;

private:
	double mReal;
	double mImaginary;
};

#endif /* COMPLEXNUMBER_H_ */
