#include "Vector.h"
#include <cmath>
#include <iostream>

namespace VECTOR
{
	const double RAD_TO_DEG = 45.0 / atan(1.0);

	Vector::Vector()
	: mX{0.0}, mY{0.0}, mLen{0.0}, mAng{0.0}, mMode{Mode::RECT} {}

	void Vector::setX()
	{
		mX = mLen * cos(mAng);
	}
	void Vector::setY()
	{
		mY = mLen * sin(mAng);
	}
	void Vector::setLength()
	{
		mLen = std::sqrt(mX * mX + mY * mY);
	}
	void Vector::setAngle()
	{
		if(mX == 0.0 && mY == 0.0) { mAng = 0.0; }
		else { mAng = atan2(mY, mX); }
	}

	Vector::Vector(double x, double y, Mode mode)
	{
		mMode = mode;

		if(mode == Mode::RECT)
		{
			mX = x;
			mY = y;
			setLength();
			setAngle();
		}
		else if(mode == Mode::POLAR)
		{
			mLen = x;
			mAng = y / RAD_TO_DEG;
			setX();
			setY();
		}
		else
		{
			std::cout << "Niepoprawna wartosc trzeciego argumentu kostruktora klasy Vector!" << std::endl;
			std::cout << "Tworze wektor zerowy." << std::endl;
			mX = mY = mLen = mAng = 0.0;
			mMode = Mode::RECT;
		}
	}

	void Vector::reset(double x, double y, Mode mode)
	{
		if(mode == Mode::RECT)
		{
			mX = x;
			mY = y;
			setLength();
			setAngle();
		}
		else if(mode == Mode::POLAR)
		{
			mLen = x;
			mAng = y / RAD_TO_DEG;
			setX();
			setY();
		}
		else
		{
			std::cout << "Niepoprawna wartosc trzeciego argumentu kostruktora klasy Vector!" << std::endl;
			std::cout << "Tworze wektor zerowy." << std::endl;
			mX = mY = mLen = mAng = 0.0;
			mMode = Mode::RECT;
		}
	}

	double Vector::getX() const { return mX; }
	double Vector::getY() const { return mY; }
	double Vector::getLength() const { return mLen; }
	double Vector::getAngle() const { return mAng; }

	void Vector::switchToPolar()
	{
		mMode = Mode::POLAR;
	}

	void Vector::switchToRect()
	{
		mMode = Mode::RECT;
	}

	Vector Vector::operator+(const Vector& other) const
	{
		return Vector{mX + other.mX, mY + other.mY};
	}

	Vector Vector::operator-(const Vector& other) const
	{
		return Vector{mX - other.mX, mY - other.mY};
	}

	Vector Vector::operator-() const
	{
		return Vector{-mX, -mY};
	}

	Vector Vector::operator*(double n) const
	{
		return Vector{n * mX, n * mY};
	}

	Vector operator*(double n, const Vector& vec)
	{
		return vec * n;
	}

	std::ostream& operator<<(std::ostream& out, const Vector& vec)
	{
		if(vec.mMode == Vector::Mode::RECT)
		{
			out << "(x, y) = (" << vec.mX << ", " << vec.mY << ")";
		}
		else if(vec.mMode == Vector::Mode::POLAR)
		{
			out << "Length: " << vec.mLen << ", angle: " << vec.mAng * RAD_TO_DEG;
		}
		else
		{
			out << "Niepoprawny stan wektora!";
		}
		return out;
	}
}	//namespace VECTOR


