#ifndef VECTOR_H_
#define VECTOR_H_

#include <iostream>

namespace VECTOR
{
	class Vector
	{
	public:
		enum class Mode { RECT, POLAR };

		Vector();
		Vector(double x, double y, Mode mode = Mode::RECT);
		void reset(double x, double y, Mode mode = Mode::RECT);
		~Vector() = default;

		double getX() const;
		double getY() const;
		double getLength() const;
		double getAngle() const;

		void switchToPolar();
		void switchToRect();

		Vector operator+(const Vector& other) const;
		Vector operator-(const Vector& other) const;
		Vector operator-() const;
		Vector operator*(double n) const;

		friend Vector operator*(double n, const Vector& vec);
		friend std::ostream& operator<<(std::ostream& out, const Vector& vec);

	private:
		double mX;
		double mY;
		double mLen;
		double mAng;
		Mode mMode;

		void setX();
		void setY();
		void setLength();
		void setAngle();
	};
}	//namespace VECTOR

#endif /* VECTOR_H_ */
