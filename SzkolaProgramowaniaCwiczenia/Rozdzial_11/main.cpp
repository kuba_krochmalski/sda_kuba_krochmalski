#include "Vector.h"
#include "ComplexNumber.h"
#include <cstdlib>
#include <ctime>
#include <fstream>

void vectorTest();
void complexNumbersTest();
void randomWalking(double distance, double stepSize, int attempts);

int main()
{
//	vectorTest();
//	srand(time(NULL));
//	randomWalking(50.0, 2.0, 10);
	complexNumbersTest();

	return 0;
}

void vectorTest()
{
	VECTOR::Vector a;
	VECTOR::Vector b{3.0, 4.0};
	VECTOR::Vector c{6.0, 8.0, VECTOR::Vector::Mode::RECT};
	VECTOR::Vector d{10.0, 30.0, VECTOR::Vector::Mode::POLAR};

	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;
	std::cout << d << std::endl;

	a.switchToPolar();
	b.switchToPolar();
	c.switchToPolar();
	d.switchToRect();

	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;
	std::cout << d << std::endl;

	std::cout << a + b + c << std::endl;
	std::cout << c - b << std::endl;
	std::cout << b - c << std::endl;
	std::cout << b * 10 << std::endl;
	std::cout << -5 * b << std::endl;
}

void complexNumbersTest()
{
	ComplexNumber a{3.0, 4.0};
	ComplexNumber c{10.0, 12.0};

	std::cout << "c to " << c << std::endl;
	std::cout << "sprzezona z c to " << ~c << std::endl;
	std::cout << "a to " << a << std::endl;
	std::cout << "a + c wynosi " << a + c << std::endl;
	std::cout << "a - c wynosi " << a - c << std::endl;
	std::cout << "a * c wynosi " << a * c << std::endl;
	std::cout << "2 * c wynosi " << 2 * c << std::endl;
}

void randomWalking(double distance, double stepSize, int attempts)
{
	VECTOR::Vector result{0.0, 0.0};
	VECTOR::Vector step;
	unsigned long long steps = 0;
	double direction;
	std::ofstream file;
	file.open("randomWalk.log");

	file << "Dystans do przejscia: " << distance << ", dlugosc kroku: " << stepSize << std::endl;

	for(int i = 0; i < attempts; ++i)
	{
		file << "<===== PROBA NR " << i + 1 << " =====>" << std::endl;
		file << steps << ". (x, y) = (" << result.getX() << ", " << result.getY() << ")" << std::endl;

		while(result.getLength() < distance)
		{
			direction = rand() % 360;
			step.reset(stepSize, direction, VECTOR::Vector::Mode::POLAR);
			result = result + step;
			steps++;
			file << steps << ". (x, y) = (" << result.getX() << ", " << result.getY() << ")" << std::endl;
		}

		file << "Po " << steps << " krokach, najebaniec doszedl do celu." << std::endl;
		file << result << std::endl;
		result.switchToPolar();
		file << result << std::endl;
		file << "Srednia dlugosc kroku: " << result.getLength() / steps << std::endl << std::endl;

		steps = 0;
		result.reset(0.0, 0.0);
	}

	file.close();
}
