#include "Queue.h"


void Client::set(long time)
{
	mArriveTime = time;
	mTimeToProcess = rand() % 3 + 1;
}

Queue::Queue(int size)
: mMaxSize(size), mFront(nullptr), mTail(nullptr), mSize(0) {}

Queue::~Queue()
{
	Node* temp;
	while(mFront != nullptr)
	{
		temp = mFront;
		mFront = mFront->next;
		delete temp;
	}
}

bool Queue::isEmpty() const
{
	return (mSize == 0);
}

bool Queue::isFull() const
{
	return (mSize == mMaxSize);
}

bool Queue::push(const Client& c)
{
	if(isFull())
	{
		return false;
	}

	Node* newNode = new Node;
	newNode->customer = c;
	newNode->next = nullptr;
	mSize++;

	if(mFront == nullptr)
	{
		mFront = newNode;
	}
	else
	{
		mTail->next = newNode;
	}
	mTail = newNode;
	return true;
}

bool Queue::pop(Client& c)
{
	if(isEmpty())
	{
		return false;
	}

	c = mFront->customer;
	mSize--;

	Node* temp = mFront;
	mFront = mFront->next;
	delete temp;

	if(isEmpty())
	{
		mTail = nullptr;
	}
	return true;
}

int Queue::getSize() const
{
	return mSize;
}

