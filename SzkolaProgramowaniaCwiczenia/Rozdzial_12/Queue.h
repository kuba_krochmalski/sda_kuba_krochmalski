#ifndef QUEUE_H_
#define QUEUE_H_
#include <cstdlib>

class Client
{
private:
	long mArriveTime;
	int mTimeToProcess;
public:
	Client() { mArriveTime = mTimeToProcess = 0; }
	void set(long time);
	long getArriveTime() const { return mArriveTime; }
	int getTimeToProcess() const { return mTimeToProcess; }
};

class Queue
{
private:
	struct Node
	{
		Client customer;
		Node* next;
	};

	static const int MAX = 10;
	const int mMaxSize;
	Node* mFront;
	Node* mTail;
	int mSize;

public:
	Queue(int size = MAX);
	~Queue();
	Queue(const Queue& other) = delete;
	Queue& operator=(const Queue& other) = delete;

	bool isEmpty() const;
	bool isFull() const;
	bool push(const Client& c);
	bool pop(Client& c);
	int getSize() const;
};

#endif /* QUEUE_H_ */
