#include "Stack.h"
#include <iostream>

Stack::Stack(int size)
: mStackSize{size}
, mTop{0}
{
	mStack = new Customer[mStackSize];
}

Stack::Stack(const Stack& other)
: mStackSize{other.mStackSize}
, mTop{other.mTop}
{
	mStack = new Customer[mStackSize];
	for(int i = 0; i < mStackSize; ++i)
	{
		mStack[i] = other.mStack[i];
	}
}

Stack::~Stack()
{
	delete [] mStack;
}

bool Stack::isEmpty() const
{
	return (mTop == 0);
}
bool Stack::isFull() const
{
	return (mTop == mStackSize);
}
bool Stack::push(const Customer& c)
{
	if(isFull())
	{
		std::cout << "Stack is full." << std::endl;
		return false;
	}
	else
	{
		mStack[mTop++] = c;
		return true;
	}
}
bool Stack::pop(Customer& c)
{
	if(isEmpty())
	{
		std::cout << "Stack is empty." << std::endl;
		c.name = "No value";
		c.payment = 0.0;
		return false;
	}
	else
	{
		c = mStack[--mTop];
		return true;
	}
}

Stack& Stack::operator=(const Stack& other)
{
	delete [] mStack;

	mStackSize = other.mStackSize;
	mTop = other.mTop;

	mStack = new Customer[mStackSize];
	for(int i = 0; i < mStackSize; ++i)
	{
		mStack[i] = other.mStack[i];
	}

	return *this;
}
