#ifndef STACK_H_
#define STACK_H_
#include <string>

struct Customer
{
	std::string name;
	double payment;
};

class Stack
{
private:
	static const int MAX = 10;
	Customer* mStack;
	int mStackSize;
	int mTop;

public:
	Stack(int size = MAX);
	Stack(const Stack& other);
	~Stack();
	bool isEmpty() const;
	bool isFull() const;
	bool push(const Customer& c);
	bool pop(Customer& c);
	Stack& operator=(const Stack& other);
};

#endif /* STACK_H_ */

