#include "String.h"
#include <cstring>
#include <cctype>

int String::mStringCounter = 0;

String::String(const char* str)
{
	mLength = std::strlen(str);
	mStr = new char[mLength + 1];
	std::strcpy(mStr, str);
	mStringCounter++;
}

String::String(const String& other)
{
	mLength = other.mLength;
	mStr = new char[mLength + 1];
	std::strcpy(mStr, other.mStr);
	mStringCounter++;
}

String::~String()
{
	delete [] mStr;
	mStringCounter--;
}

int String::length() const
{
	return mLength;
}

void String::stringLow()
{
	int i = 0;
	while(mStr[i] != '\0')
	{
		mStr[i] = tolower(mStr[i]);
		++i;
	}
}

void String::stringUp()
{
	int i = 0;
	while(mStr[i] != '\0')
	{
		mStr[i] = toupper(mStr[i]);
		++i;
	}
}

int String::charCount(const char ch) const
{
	int i = 0;
	int count = 0;
	while(mStr[i] != '\0')
	{
		if((mStr[i] == tolower(ch)) || (mStr[i] == toupper(ch)))
		{
			++count;
		}
		++i;
	}
	return count;
}

String& String::operator=(const String& other)
{
	if(this == &other) {return *this;}
	else
	{
		delete [] mStr;
		mLength = other.mLength;
		mStr = new char[mLength + 1];
		std::strcpy(mStr, other.mStr);
		return *this;
	}
}

String& String::operator=(const char* str)
{
	delete [] mStr;
	mLength = std::strlen(str);
	mStr = new char[mLength + 1];
	std::strcpy(mStr, str);
	return *this;
}

String String::operator+(const String& other)
{
	char temp[mLength + other.mLength + 1];
	std::strcpy(temp, mStr);
	std::strcat(temp, other.mStr);
	return String{temp};
}

char& String::operator[](int i)
{
	return mStr[i];
}

const char& String::operator[](int i) const
{
	return mStr[i];
}

bool operator<(const String& first, const String& second)
{
	return (std::strcmp(first.mStr, second.mStr) < 0);
}

bool operator>(const String& first, const String& second)
{
	return !(first < second);
}

bool operator==(const String& first, const String& second)
{
	return (std::strcmp(first.mStr, second.mStr) == 0);
}

std::ostream& operator<<(std::ostream& out, const String& str)
{
	out << str.mStr;
	return out;
}

std::istream& operator>>(std::istream& in, String& str)
{
	char temp[String::mLimit];
	in.get(temp, String::mLimit);
	if(in)
	{
		str = temp;
	}

	while(in && in.get() != '\n'){ continue; }

	return in;
}

int String::count()
{
	return mStringCounter;
}
