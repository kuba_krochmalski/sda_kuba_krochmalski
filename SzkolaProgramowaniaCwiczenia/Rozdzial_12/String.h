#ifndef STRING_H_
#define STRING_H_
#include <iostream>

class String
{
private:
	char* mStr;
	int mLength;
	static int mStringCounter;
	static const int mLimit = 80;

public:
	//--- Constructors & Destructors
	String(const char* str = "#Empty String#");
	String(const String& other);
	~String();
	//--- Other methods
	int length() const;
	void stringLow();
	void stringUp();
	int charCount(const char ch) const;
	//--- Operators
	String& operator=(const String& other);
	String& operator=(const char* str);
	String operator+(const String& other);
	char& operator[](int i);
	const char& operator[](int i) const;
	//--- Friend functions
	friend bool operator<(const String& first, const String& second);
	friend bool operator>(const String& first, const String& second);
	friend bool operator==(const String& first, const String& second);
	friend std::ostream& operator<<(std::ostream& out, const String& str);
	friend std::istream& operator>>(std::istream& in, String& str);
	//--- Static methods
	static int count();
};

#endif /* STRING_H_ */
