#include "String.h"
#include "Stack.h"
#include "Queue.h"
#include <ctime>

const int MIN_PER_HR = 60;
bool newCustomer(double x);

void testString();
void testStack();

int main()
{
	std::srand(std::time(0));

	std::cout << "=== Symulacja kolejki ===" << std::endl;
	std::cout << "Podaj maksymalna dlugosc kolejki: " << std::flush;
	int qsize;
	std::cin >> qsize;
	Queue line{qsize};

	std::cout << "Podaj czas symulacji w godzinach: " << std::flush;
	int hours;
	std::cin >> hours;
	long cycleLimit = hours * MIN_PER_HR;

	std::cout << "Podaj srednia liczbe klientow na godzine: " << std::flush;
	double perHour;
	std::cin >> perHour;
	double minPerCust = MIN_PER_HR / perHour;

	Client temp;
	long turnAways = 0;
	long customers = 0;
	long served = 0;
	long sumLine = 0;
	int waitTime = 0;
	long totalWaitTime = 0;

	for(int cycle = 0; cycle < cycleLimit; ++cycle)
	{
		if(newCustomer(minPerCust))
		{
			if(line.isFull())
			{
				turnAways++;
			}
			else
			{
				temp.set(cycle);
				line.push(temp);
				customers++;
			}
		}

		if(waitTime <= 0 && !line.isEmpty())
		{
			line.pop(temp);
			waitTime = temp.getTimeToProcess();
			totalWaitTime += cycle - temp.getArriveTime();
			served++;
		}

		if(waitTime > 0)
		{
			waitTime--;
		}

		sumLine += line.getSize();
	}

	if(customers > 0)
	{
		std::cout << "==================================" << std::endl;
		std::cout << "===========PODSUMOWANIE===========" << std::endl;
		std::cout << "Liczba przyjetych klientow: " << customers << std::endl;
		std::cout << "Liczba obsluzonych klientow: " << served << std::endl;
		std::cout << "Liczba odeslanych klientow: " << turnAways << std::endl;
		std::cout << "Srednia dlugosc kolejki: ";
		std::cout.precision(2);
		std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
		std::cout.setf(std::ios_base::showpoint);
		std::cout << static_cast<double>(sumLine) / cycleLimit << std::endl;
		std::cout << "Sredni czas oczekiwania: "
				  << static_cast<double>(totalWaitTime) / served << " minut." << std::endl;

	}
	else
	{
		std::cout << "==================================" << std::endl;
		std::cout << "===========PODSUMOWANIE===========" << std::endl;
		std::cout << "Brak klientow!" << std::endl;
	}

	return 0;
}

bool newCustomer(double x)
{
	return ((std::rand() * x / RAND_MAX) < 1);
}

void testString()
{
	String dupa;
	String dupa2{"aaa"};
	String dupa3{"bbb"};

	std::cout << dupa << std::endl;
	std::cout << dupa2 << std::endl;
	std::cout << dupa3 << std::endl;

	std::cout << dupa2 + dupa3 << std::endl;
	dupa2.stringUp();
	dupa3.stringUp();
	std::cout << dupa2 + dupa3 << std::endl;
	dupa3.stringLow();
	std::cout << dupa2 + dupa3 << std::endl;

	String dupa4{"AbabbAaAAba"};
	std::cout << dupa4.charCount('a') << std::endl;
}

void testStack()
{
	Stack customers{4};

	std::cout << customers.isEmpty() << std::endl;
	std::cout << customers.isFull() << std::endl;

	customers.push(Customer{"Andrzej", 13.75});
	customers.push(Customer{"Janusz", 15.55});
	customers.push(Customer{"Zbigniew", 8.99});
	customers.push(Customer{"Mieczyslaw", 21.25});

	customers.push(Customer{"Waclaw", 5.80});

	std::cout << customers.isEmpty() << std::endl;
	std::cout << customers.isFull() << std::endl;

	Stack newCustomers{customers};

	Customer popped;
	std::cout << "NEW CUSTOMERS" << std::endl;
	newCustomers.pop(popped);
	std::cout << popped.name << ": " << popped.payment << std::endl;
	newCustomers.pop(popped);
	std::cout << popped.name << ": " << popped.payment << std::endl;

	Stack oldCustomers;
	std::cout << "OLD CUSTOMERS" << std::endl;
	oldCustomers = newCustomers;
	oldCustomers.pop(popped);
	std::cout << popped.name << ": " << popped.payment << std::endl;
	oldCustomers.pop(popped);
	std::cout << popped.name << ": " << popped.payment << std::endl;

	std::cout << oldCustomers.isEmpty() << std::endl;
}

