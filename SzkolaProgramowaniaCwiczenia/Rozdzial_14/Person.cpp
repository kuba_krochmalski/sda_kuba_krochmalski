#include "Person.hpp"
#include <iostream>

Person::Person(std::string imie, std::string nazwisko)
: m_imie(imie)
, m_nazwisko(nazwisko)
{std::cout << "P" << std::endl;}

void Person::show() const
{
	std::cout << m_imie << " " << m_nazwisko << std::endl;
}

Gunslinger::Gunslinger(std::string imie, std::string nazwisko, int cuts)
: Person(imie, nazwisko)
, m_cuts(cuts)
{std::cout << "G" << std::endl;}

void Gunslinger::draw() const
{
	std::cout << "Wyciaga rewolwer!" << std::endl;
}

void Gunslinger::show() const
{
	Person::show();
	std::cout << "Naciecia: " << m_cuts << std::endl;
}

PokerPlayer::PokerPlayer(std::string imie, std::string nazwisko)
: Person(imie, nazwisko)
{std::cout << "PP" << std::endl;}

void PokerPlayer::draw() const
{
	std::cout << "Wyciaga karte!" << std::endl;
}

void PokerPlayer::show() const
{
	Person::show();
}

BadDude::BadDude(std::string imie, std::string nazwisko, int cuts)
: Gunslinger(imie, nazwisko, cuts), PokerPlayer(imie, nazwisko)
{std::cout << "BD" << std::endl;}

void BadDude::show() const
{
	Gunslinger::show();
}

void BadDude::GDraw() const
{
	Gunslinger::draw();
}

void BadDude::CDraw() const
{
	PokerPlayer::draw();
}
