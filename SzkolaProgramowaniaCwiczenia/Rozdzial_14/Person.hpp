#ifndef PERSON_HPP_
#define PERSON_HPP_

#include <string>

class Person
{
private:
	std::string m_imie;
	std::string m_nazwisko;

public:
	Person(std::string imie = "John", std::string nazwisko = "Doe");
	virtual ~Person() = default;
	virtual void show() const;
};

class Gunslinger : virtual public Person
{
private:
	int m_cuts;
public:
	Gunslinger(std::string imie = "John", std::string nazwisko = "Doe", int cuts = 4);
	~Gunslinger() = default;
	void draw() const;
	void show() const override;
};

class PokerPlayer : virtual public Person
{
private:
public:
	PokerPlayer(std::string imie = "John", std::string nazwisko = "Doe");
	~PokerPlayer() = default;
	void draw() const;
	void show() const override;
};

class BadDude : public Gunslinger, public PokerPlayer
{
private:
public:
	BadDude(std::string imie = "John", std::string nazwisko = "Doe", int cuts = 4);
	~BadDude() = default;
	void show() const override;
	void GDraw() const;
	void CDraw() const;
};

#endif /* PERSON_HPP_ */
