#include "Wine.hpp"
#include <iostream>

Wine::Wine(const char* name, int vin, const int years[], const int bottles[])
: m_name{name}
, m_stock{std::valarray<int>(vin), std::valarray<int>(vin)}
, m_vintages{vin}
{
	for(int i = 0; i < vin; ++i)
	{
		m_stock.first[i] = years[i];
		m_stock.second[i] = bottles[i];
	}
}

Wine::Wine(const char* name, int vin)
: m_name{name}
, m_stock{std::valarray<int>(vin), std::valarray<int>(vin)}
, m_vintages{vin}
{}

void Wine::getBottles()
{
	std::cout << "Podaj dane o winie " << m_name << " dla " << m_vintages << " rocznikow." << std::endl;

	for(int i = 0; i < m_vintages; ++i)
	{
		std::cout << "Podaj rocznik: " << std::flush;
		std::cin >> m_stock.first[i];
		std::cout << "Podaj liczbe butelek: " << std::flush;
		std::cin >> m_stock.second[i];
	}
}

const std::string& Wine::getName()
{
	return m_name;
}

int Wine::sum()
{
	return m_stock.second.sum();
}

void Wine::show()
{
	std::cout << "Wino: " << m_name << std::endl;
	std::cout << "\tRocznik \tButelki" << std::endl;

	for(int i = 0; i < m_vintages; ++i)
	{
		std::cout << "\t" << m_stock.first[i]
				  << "\t\t" << m_stock.second[i] << std::endl;
	}
}

PrivateWine::PrivateWine(const char* name, int vin, const int years[], const int bottles[])
: std::string{name}
, std::pair<std::valarray<int>, std::valarray<int>>{std::valarray<int>(vin), std::valarray<int>(vin)}
, m_vintages{vin}
{
	for(int i = 0; i < vin; ++i)
	{
		static_cast<std::pair<std::valarray<int>, std::valarray<int>>&>(*this).first[i] = years[i];
		static_cast<std::pair<std::valarray<int>, std::valarray<int>>&>(*this).second[i] = bottles[i];
	}
}

PrivateWine::PrivateWine(const char* name, int vin)
: std::string{name}
, std::pair<std::valarray<int>, std::valarray<int>>{std::valarray<int>(vin), std::valarray<int>(vin)}
, m_vintages{vin}
{}

void PrivateWine::show()
{
	std::cout << "Wino: " << static_cast<std::string>(*this) << std::endl;
	std::cout << "\tRocznik \tButelki" << std::endl;

	for(int i = 0; i < m_vintages; ++i)
	{
		std::cout << "\t" << static_cast<std::pair<std::valarray<int>, std::valarray<int>>&>(*this).first[i]
				  << "\t\t" << static_cast<std::pair<std::valarray<int>, std::valarray<int>>&>(*this).second[i] << std::endl;
	}
}

int PrivateWine::sum()
{
	return (static_cast<std::pair<std::valarray<int>, std::valarray<int>>&>(*this).second.sum());
}
