#ifndef WINE_HPP_
#define WINE_HPP_
#include <string>
#include <valarray>

class Wine
{
private:
	std::string m_name;
	std::pair<std::valarray<int>, std::valarray<int>> m_stock;
	int m_vintages;
public:
	Wine(const char* name, int vin, const int years[], const int bottles[]);
	Wine(const char* name, int vin);
	~Wine() = default;

	void getBottles();
	const std::string& getName();
	int sum();
	void show();
};

class PrivateWine : private std::string,
					private std::pair<std::valarray<int>, std::valarray<int>>
{
private:
	int m_vintages;
public:
	PrivateWine(const char* name, int vin, const int years[], const int bottles[]);
	PrivateWine(const char* name, int vin);
	~PrivateWine() = default;

	void getBottles();
	const std::string& getName();
	int sum();
	void show();
};

#endif /* WINE_HPP_ */
