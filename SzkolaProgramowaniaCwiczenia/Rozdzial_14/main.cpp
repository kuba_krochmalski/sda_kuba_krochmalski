#include <iostream>
#include <array>
#include <string>
#include <stack>
#include <vector>
#include "Wine.hpp"
#include "Person.hpp"

int main()
{
	/*std::array<std::string, 3> arr = {"dup1", "dup2", "dup3"};

	std::stack<std::array<std::string, 3>, std::vector<std::array<std::string, 3>>> st;
	st.push(arr);

	std::cout << st.top()[0] << std::endl;
	std::cout << st.top()[1] << std::endl;
	std::cout << st.top()[2] << std::endl;
	*/

	/*
	std::cout << "Podaj nazwe wina: " << std::flush;
	char label[40];
	std::cin.getline(label, 40);
	std::cout << "Podaj liczbe rocznikow: " << std::flush;
	int noOfYears;
	std::cin >> noOfYears;

	Wine wino1(label, noOfYears);
	wino1.getBottles();
	wino1.show();

	const int YRS = 3;
	int y[YRS] = {1991, 1993, 1997};
	int b[YRS] = {17, 35, 68};

	Wine wino2("Samuraj", YRS, y, b);
	wino2.show();

	std::cout << "Laczna liczba butelek wina " << wino2.getName()
			  << ": " << wino2.sum() << std::endl;
	*/

	/*
	Gunslinger c{"John", "Marston", 17};
	c.show();
	c.draw();

	PokerPlayer d;
	d.show();
	d.draw();

	BadDude e{"Zly", "Czlowiek", 5};
	e.show();
	e.GDraw();
	e.CDraw();
	*/

	const int YRS = 3;
	int y[YRS] = {1991, 1993, 1997};
	int b[YRS] = {17, 35, 68};

	PrivateWine winko{"Komandos", YRS, y, b};
	winko.show();
	std::cout << "Lacznie win: " << winko.sum() << std::endl;

	return 0;
}
