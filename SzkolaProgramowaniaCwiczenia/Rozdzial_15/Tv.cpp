#include "Tv.hpp"
#include <iostream>

Tv::Tv(State st, Mode mod, int mc)
: MAXVOL{20}, MAXCHANNEL{mc}
, m_state{st}
, m_mode{mod}
, m_input{Input::TV}
, m_volume{5}
, m_channel{1}
{}

bool Tv::volup()
{
	if(m_volume < MAXVOL)
	{
		m_volume++;
		return true;
	}
	else
	{
		return false;
	}
}

bool Tv::voldown()
{
	if(m_volume > 0)
	{
		m_volume--;
		return true;
	}
	else
	{
		return false;
	}
}

void Tv::chanup()
{
	if(m_channel != MAXCHANNEL)
	{
		m_channel++;
	}
	else
	{
		m_channel = 1;
	}
}

void Tv::chandown()
{
	if(m_channel != 1)
	{
		m_channel--;
	}
	else
	{
		m_channel = MAXCHANNEL;
	}
}

void Tv::showSettings() const
{
	std::cout << "Telewizor jest " << (m_state == State::Off ? "wylaczony" : "wlaczony") << ".\n";
	if(m_state == State::On)
	{
		std::cout << "Program: " << m_channel << std::endl;
		std::cout << "Glosnosc: " << m_volume << std::endl;
		std::cout << "Tryb: " << (m_mode == Mode::Cable ? "Kabel" : "Antena") << std::endl;
		std::cout << "Wyjscie: " << (m_input == Input::TV ? "TV" : "DVD") << std::endl;
	}
}

bool Remote::setChannel(Tv& tv, int ch)
{
	if(ch < tv.MAXCHANNEL && ch > 1)
	{
		tv.m_channel = ch;
		return true;
	}
	else
	{
		return false;
	}
}
bad_mean::bad_mean(const std::string& msg, double a, double b)
: std::logic_error{msg}, m_a{a}, m_b{b}
{}

void bad_mean::sayWhat() const
{
	std::cout << std::logic_error::what() << " a = " << m_a << ", b = " << m_b << std::endl;
}

bad_hmean::bad_hmean(double a, double b)
: bad_mean{"Niepoprawne argumenty funkcji hmean()! (a == -b)", a, b}
{}

bad_gmean::bad_gmean(double a, double b)
: bad_mean{"Niepoprawne argumenty funkcji gmean()! (a < 0 || b < 0)", a, b}
{}

void bad_hmean::no_elo()
{
	std::cout << "no elo xD" << std::endl;
}

void bad_gmean::no_siema()
{
	std::cout << "no siema xD" << std::endl;
}
