#ifndef TV_HPP_
#define TV_HPP_

#include <stdexcept>

class Remote;

class Tv
{
public:
	friend class Remote;

	enum class State {On, Off};
	enum class Mode {Antena, Cable};
	enum class Input {TV, DVD};


	Tv(State st = State::Off, Mode mod = Mode::Cable, int mc = 125);
	~Tv() = default;

	void power() {m_state = (m_state == State::On) ? State::Off : State::On;}
	bool isOn() {return m_state == State::On;}
	bool volup();
	bool voldown();
	void chanup();
	void chandown();
	void changeMode() {m_mode = (m_mode == Mode::Antena) ? Mode::Cable : Mode::Antena;}
	void changeInput() {m_input = (m_input == Input::TV) ? Input::DVD : Input::TV;}
	void showSettings() const;

private:
	const int MAXVOL;
	const int MAXCHANNEL;
	State m_state;
	Mode m_mode;
	Input m_input;
	int m_volume;
	int m_channel;
};

class Remote
{
public:
	enum class Mode {TV, DVD};

	Remote(Mode mod = Mode::TV): m_mode{mod}{};
	~Remote() = default;

	void power(Tv& tv) {tv.power();}
	bool volup(Tv& tv) {return tv.volup();}
	bool voldown(Tv& tv) {return tv.voldown();}
	void chanup(Tv& tv) {tv.chanup();}
	void chandown(Tv& tv) {tv.chandown();}
	void changeMode(Tv& tv) {tv.changeMode();}
	void changeInput(Tv& tv) {tv.changeInput();}
	bool setChannel(Tv& tv, int ch);

private:
	Mode m_mode;
};

class bad_mean : public std::logic_error
{
private:
	double m_a;
	double m_b;
public:
	bad_mean(const std::string& msg, double a, double b);
	~bad_mean() = default;

	void sayWhat() const;
};

class bad_hmean : public bad_mean
{
public:
	bad_hmean(double a, double b);
	~bad_hmean() = default;

	void no_elo();
};

class bad_gmean : public bad_mean
{
public:
	bad_gmean(double a, double b);
	~bad_gmean() = default;

	void no_siema();
};

#endif /* TV_HPP_ */
