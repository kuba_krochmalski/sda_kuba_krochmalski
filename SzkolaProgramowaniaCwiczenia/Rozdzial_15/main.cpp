#include "Tv.hpp"
#include <cmath>
#include <iostream>

double hmean(double a, double b);
double gmean(double a, double b);

int main()
{
	/*
	Tv tele(Tv::State::On, Tv::Mode::Cable);
	tele.showSettings();

	Remote pilot;
	pilot.setChannel(tele, 120);

	tele.showSettings();
	*/

	try
	{
		std::cout << hmean(2.0, 3.0) << std::endl;
		std::cout << gmean(2.0, -3.0) << std::endl;
	}

	catch(bad_mean& ex)
	{
		ex.sayWhat();

		bad_hmean* hPtr;
		bad_gmean* gPtr;

		if(hPtr = dynamic_cast<bad_hmean*>(&ex))
		{
			hPtr->no_elo();
		}
		else if(gPtr = dynamic_cast<bad_gmean*>(&ex))
		{
			gPtr->no_siema();
		}
	}

	return 0;
}

double hmean(double a, double b)
{
	if (a == -b)
	{
		throw bad_hmean(a, b);
	}
	return 2.0 * a * b / (a + b);

}

double gmean(double a, double b)
{
	if (a < 0 || b < 0)
	{
		throw bad_gmean(a, b);
	}
	return std::sqrt(a * b);
}
