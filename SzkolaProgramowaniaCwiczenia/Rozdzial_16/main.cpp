#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

void czyPalindrom();
template <typename Type>
std::vector<Type> reduce(Type ar[], int n);

int main()
{
//	czyPalindrom();

	const int COUNT = 20;
	long array[COUNT] = {1, 7, 6, 30, 6, 7, 8, 10, 11, 13,
						30, 4, 3, 1, 0, 13, 16, 21, 31, 45};
	std::string arr[6] = {"dupa", "alfred", "dupa", "ryszard", "bogdan", "zbigniew"};

	auto reduced = reduce(array, COUNT);
	std::for_each(reduced.begin(), reduced.end(), [](auto x){std::cout << x << " ";});
	std::cout << "\n";

	auto reducedArr = reduce(arr, 6);
	std::for_each(reducedArr.begin(), reducedArr.end(), [](auto x){std::cout << x << " ";});

	return 0;
}

void czyPalindrom()
{
	std::cout << "Podaj slowo: " << std::flush;
	std::string word;
	std::getline(std::cin, word);

	while(word.find(' ') != std::string::npos)
	{
		word.erase(word.find(' '), 1);
	}

	std::string reversedWord;
	for(auto it = word.rbegin(); it != word.rend(); ++it)
	{
		reversedWord += *it;
	}
	if(word == reversedWord) {std::cout << "tak" << std::endl;}
	else {std::cout << "nie" << std::endl;}
}

template <typename Type>
std::vector<Type> reduce(Type ar[], int n)
{
	std::vector<Type> v;
	std::for_each(ar, ar+n, [&](Type x){v.push_back(x);});
	std::sort(v.begin(), v.end());
	auto last = std::unique(v.begin(), v.end());
	v.erase(last, v.end());

	return v;
}
