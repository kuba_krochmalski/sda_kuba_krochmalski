#include <iostream>
#include <string>
using namespace std;

void printIntArr(int tab[], int size)
{
	for(int i = 0; i < size; ++i)
	{
		std::cout << tab[i] << ", ";
	}
	std::cout << std::endl;
}

void fillIntArr(int tab[], int size, int value)
{
	for(int i = 0; i < size; ++i)
	{
		tab[i] = value;
	}
}

void printIntArrPtr(int* first, int* last)
{
	int* end = last + 1;

	do
	{
		std::cout << *first << ", ";
		++first;
	}while(first != end);
	std::cout << std::endl;
}

void fillIntArrPrt(int* first, int* last, int value)
{
	int* end = last + 1;

	do
	{
		*first = value;
		++first;
	}while(first != end);
}

int replace(char* str, char c1, char c2)
{
	int replaces = 0;

	while(*str != '\0')
	{
		if(*str == c1)
		{
			*str = c2;
			++replaces;
		}
		++str;
	}

	return replaces;
}

int funkcja(std::string& napis, int* intPtr) //randomowa funkcja
{
	std::cout << napis << " ---  " << *intPtr << std::endl;
	return *intPtr + 5;
}

double dodaj(double x, double y)
{
	return x + y;
}

double odejmij(double x, double y)
{
	return x - y;
}

double pomnoz(double x, double y)
{
	return x * y;
}

double podziel(double x, double y)
{
	return x / y;
}

double oblicz(double x, double y, double (*pf)(double, double))
{
	return pf(x, y);
}

int main()
{
	int tab[5];
	fillIntArr(tab, 5, 13);
	printIntArr(tab, 5);

	int tab2[12];
	fillIntArrPrt(tab2, tab2+11, 3);
	printIntArrPtr(tab2, tab2+11);

	char str[] = "ananas";
	std::cout << replace(str, 'a', 'o') << " replaces ---> ";
	std::cout << str << std::endl;

	int liczba = 7;
	int* int_p = &liczba;
	std::string tekst = "dupa";

	int (*p1)(std::string& napis, int* intPtr) = funkcja; //p1 to wskaznik do funkcji - przypisanie do funkcja()

	std::cout << p1(tekst, int_p) << std::endl;

	typedef int (*p_fun)(std::string&, int*); //p_fun to nowy typ - wskaznik do funkcji

	p_fun p2 = funkcja; //p2 to taki sam wskaznik jak p1

	std::cout << p2(tekst, int_p) << std::endl;

	p_fun ap[5]; //tablica 5 wskaznikow do funkcji typu p_fun
	ap[0] = p1; //przypisanie
	ap[1] = p2; //przypisanie

	std::cout << ap[0](tekst, int_p) << std::endl; //wywolania poprzez odwolanie do elementow tablicy
	std::cout << ap[1](tekst, int_p) << std::endl; // -//-------

	p_fun (*arrPtr)[5] = &ap; //arrPtr to wskaznik na 5-elementowa tablice wskaznikow do funkcji typu p_fun

	double (*pfArr[4])(double, double);
	pfArr[0] = dodaj;
	pfArr[1] = odejmij;
	pfArr[2] = pomnoz;
	pfArr[3] = podziel;

	for(int i = 0; i < 4; ++i)
	{
		std::cout << pfArr[i](7.0, 3.0) << std::endl;
	}
}
