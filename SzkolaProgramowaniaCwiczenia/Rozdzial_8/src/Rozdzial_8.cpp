#include <iostream>

struct pudelko
{
	std::string producent;
	float wysokosc;
	float szerokosc;
	float dlugosc;
	float pojemnosc;
};

template<typename T>
T max(T const& a, T const& b)
{
	return a > b ? a : b;
}

template<> pudelko max(pudelko const& a, pudelko const& b)
{
	return a.pojemnosc > b.pojemnosc ? a : b;
}

pudelko max(pudelko const& a, pudelko const& b)
{
	return a.pojemnosc < b.pojemnosc ? a : b;
}

template<int N>
struct Factorial
{
	enum { val = N * Factorial<N-1>::val };
};

template<>
struct Factorial<0>
{
	enum { val = 1 };
};

int main()
{
	std::cout << max(3, 5) << std::endl;
	std::cout << max(2.5, 1.7) << std::endl;
	std::cout << max(3.6f, 5.9f) << std::endl;

	pudelko p1 = {"mini_pudelka", 1.0, 1.0, 1.0, 1.0};
	pudelko p2 = {"wielkie_pudla", 2.0, 2.0, 2.0, 8.0};

	std::cout << max(p1, p2).producent << std::endl;
	std::cout << max<>(p1, p2).producent << std::endl;

	std::cout << Factorial<6>::val << std::endl;

	return 0;
}
