#include "golf.h"
#include <iostream>

void setGolf(golf& g, std::string name, int hc)
{
	g.fullname = name;
	g.handicap = hc;
}

void setGolf(golf& g)
{
	std::cout << "Podaj imie gracza: " << std::flush;
	std::cin >> g.fullname;
	std::cout << "Podaj handicap: " << std::flush;
	std::cin >> g.handicap;
}

void setHandicap(golf& g, int hc)
{
	g.handicap = hc;
}

void showGolf(const golf& g)
{
	std::cout << "Imie gracza: " << g.fullname << std::endl;
	std::cout << "Handicap gracza: " << g.handicap << std::endl;
}
