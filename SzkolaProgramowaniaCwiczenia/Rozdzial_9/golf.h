#ifndef GOLF_H_
#define GOLF_H_

#include <string>

struct  golf
{
	std::string fullname;
	int handicap;
};

void setGolf(golf& g, std::string name, int hc);
void setGolf(golf& g);
void setHandicap(golf& g, int hc);
void showGolf(const golf& g);

#endif /* GOLF_H_ */
