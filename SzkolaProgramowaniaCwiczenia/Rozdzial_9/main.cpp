#include "golf.h"
#include "sales.h"
#include <iostream>

int main()
{
	golf jan;
	//golf gosciu;
	golf bozenka = {"Bozenka", 24};

	setGolf(jan, "Jan", 18);
	//setGolf(gosciu);

	showGolf(jan);
	//showGolf(gosciu);
	showGolf(bozenka);

	setHandicap(jan, 15);

	showGolf(jan);
	std::cout << "<===========================>" << std::endl << std::endl;

	golf golfBuffer[20];
	std::cout << golfBuffer << std::endl;

	golf* gArr = new (golfBuffer) golf[2]; //bez DELETE!
	gArr[0].fullname = "Kuba";
	gArr[0].handicap = 10;
	gArr[1].fullname = "Marcin";
	gArr[1].handicap = 13;
	std::cout << gArr << std::endl;

	for(int i = 0; i < 2; ++i)
	{
		std::cout << "Imie: " << gArr[i].fullname << std::endl;
		std::cout << "Handicap: " << gArr[i].handicap << std::endl;
		std::cout << "- - - - - - - - - - - - - - - -" << std::endl;
	}

	std::cout << "<===========================>" << std::endl << std::endl;

	Sales::Sales sales2016;
	Sales::Sales sales2017;

	double tab[3] = {12.3, 13.7, 14.2};

	Sales::setSales(sales2016);
	Sales::setSales(sales2017, tab, 3);

	Sales::showSales(sales2016);
	Sales::showSales(sales2017);

	return 0;
}
