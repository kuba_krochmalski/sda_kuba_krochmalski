#ifndef SALES_H_
#define SALES_H_

namespace Sales
{
	const int QUARTERS = 4;

	struct Sales
	{
		double sales[QUARTERS];
		double average;
		double max;
		double min;
	};

	void setSales(Sales& s);
	void setSales(Sales& s, const double ar[], int arSize);
	void showSales(const Sales& s);
}

#endif /* SALES_H_ */
