/*
 * CCezarTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "CCezarTekst.hpp"
#include <iostream>
#include <string>

CCezarTekst::CCezarTekst()
: CSzyfrowanyTekst()
{
	std::cout << "CCezarTekst()" << std::endl;

}

CCezarTekst::CCezarTekst(std::string lancuch)
: CSzyfrowanyTekst(lancuch)
{
	std::cout << "CCezarTekst()" << std::endl;

}

CCezarTekst::~CCezarTekst()
{
	std::cout << "~CCezarTekst()" << std::endl;
}

void CCezarTekst::szyfruj()
{
	if(mCzyZaszyfrowany == false)
	{
		for (unsigned int i = 0; i < mLancuch.length(); ++i)
		{
			mLancuch[i] += 3;
		}

		mCzyZaszyfrowany = true;
	}
}

void CCezarTekst::wypisz()
{
	std::cout << "Cezar tekst: " << mLancuch << std::endl;
}

