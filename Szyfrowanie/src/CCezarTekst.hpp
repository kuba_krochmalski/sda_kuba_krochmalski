/*
 * CCezarTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CCEZARTEKST_HPP_
#define CCEZARTEKST_HPP_
#include "CSzyfrowanyTekst.hpp"

class CCezarTekst : public CSzyfrowanyTekst
{
public:
	CCezarTekst();
	CCezarTekst(std::string lancuch);
	virtual ~CCezarTekst();
	void szyfruj();
	void wypisz();
};

#endif /* CCEZARTEKST_HPP_ */
