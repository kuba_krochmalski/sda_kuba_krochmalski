/*
 * CPodstawieniowyTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "CPodstawieniowyTekst.hpp"
#include <iostream>

CPodstawieniowyTekst::CPodstawieniowyTekst()
: CSzyfrowanyTekst()
{
	std::cout << "CPodstawieniowyTekst()" << std::endl;

}

CPodstawieniowyTekst::CPodstawieniowyTekst(std::string lancuch)
: CSzyfrowanyTekst(lancuch)
{
	std::cout << "CPodstawieniowyTekst()" << std::endl;

}

CPodstawieniowyTekst::~CPodstawieniowyTekst()
{
	std::cout << "~CPodstawieniowyTekst()" << std::endl;
}

void CPodstawieniowyTekst::szyfruj()
{
	if(mCzyZaszyfrowany == false)
	{
		for (unsigned int i = 0; i < mLancuch.length(); ++i)
		{
			mLancuch[i] = zamien(mLancuch[i]);
		}

		mCzyZaszyfrowany = true;
	}
}

void CPodstawieniowyTekst::wypisz()
{
	std::cout << "Podstawiony tekst: " << mLancuch << std::endl;
}

char CPodstawieniowyTekst::zamien(char ch)
{
	char nowyChar = 'a';

	for (int i = 97; i < 123; i++)
	{
		if (int(ch) == i)
		{
			char znak = 'a';
			znak = 122 + 97 - i;
			nowyChar = char(znak);
		}
	}

	return nowyChar;
}

