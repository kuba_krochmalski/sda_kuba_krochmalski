/*
 * CPodstawieniowyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CPODSTAWIENIOWYTEKST_HPP_
#define CPODSTAWIENIOWYTEKST_HPP_
#include "CSzyfrowanyTekst.hpp"

class CPodstawieniowyTekst : public CSzyfrowanyTekst
{
public:
	CPodstawieniowyTekst();
	CPodstawieniowyTekst(std::string lancuch);
	virtual ~CPodstawieniowyTekst();
	void szyfruj();
	void wypisz();
	char zamien(char ch);
};

#endif /* CPODSTAWIENIOWYTEKST_HPP_ */
