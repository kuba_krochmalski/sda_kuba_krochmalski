/*
 * CSzyfrowanyTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "CSzyfrowanyTekst.hpp"
#include <iostream>

CSzyfrowanyTekst::CSzyfrowanyTekst()
: CWypisywalny()
, mCzyZaszyfrowany(false)
{
	std::cout << "CSzyfrowanyTekst()" << std::endl;

}

CSzyfrowanyTekst::CSzyfrowanyTekst(std::string lancuch)
: CWypisywalny(lancuch)
, mCzyZaszyfrowany(false)
{
	std::cout << "CSzyfrowanyTekst()" << std::endl;

}

CSzyfrowanyTekst::~CSzyfrowanyTekst()
{
	std::cout << "~CSzyfrowanyTekst()" << std::endl;
}

