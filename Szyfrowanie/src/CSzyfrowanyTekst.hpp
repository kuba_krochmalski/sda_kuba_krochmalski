/*
 * CSzyfrowanyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CSZYFROWANYTEKST_HPP_
#define CSZYFROWANYTEKST_HPP_
#include "CWypisywalny.hpp"

class CSzyfrowanyTekst : public CWypisywalny
{
protected:
	bool mCzyZaszyfrowany;

public:
	CSzyfrowanyTekst();
	CSzyfrowanyTekst(std::string lancuch);
	virtual ~CSzyfrowanyTekst();
	virtual void szyfruj() = 0;
	virtual void wypisz() = 0;
};

#endif /* CSZYFROWANYTEKST_HPP_ */
