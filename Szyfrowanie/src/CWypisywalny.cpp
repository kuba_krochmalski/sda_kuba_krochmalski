/*
 * CWypisywalny.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "CWypisywalny.hpp"
#include <algorithm>
#include <iostream>


CWypisywalny::CWypisywalny()
: mLancuch("dupa")
{
	std::cout << "Tworze " << mLancuch << std::endl;

}

CWypisywalny::~CWypisywalny()
{
	std::cout << "Niszcze " << mLancuch << std::endl;
}

CWypisywalny::CWypisywalny(std::string lancuch)
{
	std::transform(lancuch.begin(), lancuch.end(), lancuch.begin(), ::tolower);

	mLancuch = lancuch;

	std::cout << "Tworze " << mLancuch << std::endl;
}

void CWypisywalny::wypisz()
{
	std::cout << mLancuch << std::endl;
}
