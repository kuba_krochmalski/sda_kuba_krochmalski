/*
 * CWypisywalny.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CWYPISYWALNY_HPP_
#define CWYPISYWALNY_HPP_
#include <string>

class CWypisywalny
{
protected:
	std::string mLancuch;

public:
	CWypisywalny();
	CWypisywalny(std::string lancuch);
	virtual ~CWypisywalny();
	virtual void wypisz() = 0;

	const std::string& getLancuch() const
	{
		return mLancuch;
	}

	void setLancuch(const std::string& lancuch)
	{
		mLancuch = lancuch;
	}
};

#endif /* CWYPISYWALNY_HPP_ */
