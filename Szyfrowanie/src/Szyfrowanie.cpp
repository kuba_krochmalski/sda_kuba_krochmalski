
#include <iostream>
#include "CWypisywalny.hpp"
#include "CSzyfrowanyTekst.hpp"
#include "CCezarTekst.hpp"
#include "CPodstawieniowyTekst.hpp"
using namespace std;

int main()
{

//	CCezarTekst c("abcd");
//	c.wypisz();
//	c.szyfruj();
//	c.wypisz();
//
//	CPodstawieniowyTekst p("abcd");
//	p.wypisz();
//	p.szyfruj();
//	p.wypisz();

	CSzyfrowanyTekst* tablica[5];
	tablica[0] = new CCezarTekst("dupa");
	tablica[1] = new CCezarTekst("krowa");
	tablica[2] = new CPodstawieniowyTekst("kaczka");
	tablica[3] = new CPodstawieniowyTekst("wonsz");
	tablica[4] = new CPodstawieniowyTekst("krokodyl");

	for (int i = 0; i < 5; i++)
	{
		tablica[i]->szyfruj();
		tablica[i]->wypisz();
	}

	CWypisywalny* tab[5];
	tab[0] = new CCezarTekst("dupa");
	tab[1] = new CCezarTekst("krowa");
	tab[2] = new CPodstawieniowyTekst("kaczka");
	tab[3] = new CPodstawieniowyTekst("wonsz");
	tab[4] = new CPodstawieniowyTekst("krokodyl");

	for (int i = 0; i < 5; i++)
	{
		tab[i]->wypisz();
	}


	delete tablica[0];
	delete tablica[1];
	delete tablica[2];
	delete tablica[3];
	delete tablica[4];
	delete tab[0];
	delete tab[1];
	delete tab[2];
	delete tab[3];
	delete tab[4];

	return 0;
}
