/*
 * CKalkulator.hpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#ifndef CKALKULATOR_HPP_
#define CKALKULATOR_HPP_

class CKalkulator
{
public:
	int add(int x, int y);
	int subtract(int x, int y);
	double multi(double x, double y);
	float power(float x, int y);
	int modulo(int x, int y);

	CKalkulator();
	virtual ~CKalkulator();
};

#endif /* CKALKULATOR_HPP_ */
