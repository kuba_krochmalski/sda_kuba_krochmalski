/*
 * Kalkulator_test.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"

#include "CKalkulator.hpp"
#include "Kalkkulator2.cpp"

TEST(KalkulatorTest, AddTest)
{
	CKalkulator c;
	EXPECT_EQ(4, c.add(2,2));
	EXPECT_EQ(1, c.add(0,1));
	EXPECT_EQ(0, c.add(-2,2));
	EXPECT_EQ(-4, c.add(-2,-2));
}

TEST(KalkulatorTest, SubtractTest)
{
	CKalkulator c;
	EXPECT_EQ(0, c.subtract(2,2));
	EXPECT_EQ(-1, c.subtract(0,1));
	EXPECT_EQ(4, c.subtract(7,3));
	EXPECT_EQ(0, c.subtract(-2,-2));
}

TEST(KalkulatorTest, MultiTest)
{
	CKalkulator c;
	EXPECT_EQ(6.0, c.multi(2.0,3.0));
	EXPECT_EQ(0.0, c.multi(0.0,7.0));
	EXPECT_EQ(-6.0, c.multi(-1.0,6.0));
	EXPECT_EQ(4.0, c.multi(-2.0,-2.0));
}

TEST(KalkulatorTest, PowerTest)
{
	CKalkulator c;
	EXPECT_EQ(1.0, c.power(3.0,0));
	EXPECT_EQ(3.0, c.power(3.0,1));
	EXPECT_EQ(6.25, c.power(2.5,2));
	EXPECT_EQ(-8.0, c.power(-2.0,3));
}

TEST(KalkulatorTest, ModuloTest)
{
	CKalkulator c;
	EXPECT_EQ(0, c.modulo(10,5));
	EXPECT_EQ(1, c.modulo(10,3));
	EXPECT_EQ(2, c.modulo(10,4));
	EXPECT_EQ(3, c.modulo(10,7));
}

TEST(Kalkulator2Test, ConstructorTest)
{
	Calculator c(2,3);
	EXPECT_EQ(0, c.getResult());
}

TEST(Kalkulator2Test, AddTest)
{
	Calculator c1(2,3);
	Calculator c2(4,8);
	Calculator c3(12,15);
	EXPECT_EQ(5, c1.add());
	EXPECT_EQ(5, c1.getResult());
	EXPECT_EQ(12, c2.add());
	EXPECT_EQ(12, c2.getResult());
	EXPECT_EQ(27, c3.add());
	EXPECT_EQ(27, c3.getResult());
}

TEST(Kalkulator2Test, AddTestNegative)
{
	Calculator c(-2,-3);
	EXPECT_EQ(-5, c.add());
	EXPECT_EQ(-5, c.getResult());
}

TEST(Kalkulator2Test, SubtractTest)
{
	Calculator c1(7,3);
	c1.substr();
	Calculator c2(3,7);
	c2.substr();
	EXPECT_EQ(4, c1.getResult());
	EXPECT_EQ(-4, c2.getResult());
}

int main(int argc, char **argv)
{

	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();

}


