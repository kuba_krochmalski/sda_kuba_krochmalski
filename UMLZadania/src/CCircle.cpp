/*
 * CCircle.cpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#include "CCircle.hpp"

CCircle::CCircle()
{
	// TODO Auto-generated constructor stub

}

const std::string& CCircle::getColor() const
{
	return mColor;
}

void CCircle::setColor(const std::string& color)
{
	mColor = color;
}

double CCircle::getRadius() const
{
	return mRadius;
}

void CCircle::setRadius(double radius)
{
	mRadius = radius;
}

CCircle::~CCircle()
{
	// TODO Auto-generated destructor stub
}

