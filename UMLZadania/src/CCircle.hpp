/*
 * CCircle.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CCIRCLE_HPP_
#define CCIRCLE_HPP_
#include <string>

class CCircle
{
private:
	double mRadius;
	std::string mColor;

public:
	CCircle();
	CCircle(double radius);
	CCircle(double radius, std::string color);
	virtual ~CCircle();

	const std::string& getColor() const;
	void setColor(const std::string& color);
	double getRadius() const;
	void setRadius(double radius);

	double getArea();
	std::string toString();
};

#endif /* CCIRCLE_HPP_ */
