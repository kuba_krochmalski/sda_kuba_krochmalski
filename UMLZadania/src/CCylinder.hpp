/*
 * CCylinder.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CCYLINDER_HPP_
#define CCYLINDER_HPP_
#include "CCircle.hpp"

class CCylinder : public CCircle
{
private:
	double height;

public:
	CCylinder();
	CCylinder(double radius);
	CCylinder(double radius, double height);
	CCylinder(double radius, double height, std::string color);
	virtual ~CCylinder();

	double getHeight() const;
	void setHeight(double height);

	double getVolume();
};

#endif /* CCYLINDER_HPP_ */
