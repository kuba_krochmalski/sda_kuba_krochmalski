/*
 * CMyPoint.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CMYPOINT_HPP_
#define CMYPOINT_HPP_

class CMyPoint
{
private:
	int x;
	int y;

public:
	CMyPoint();
	virtual ~CMyPoint();
};

#endif /* CMYPOINT_HPP_ */
