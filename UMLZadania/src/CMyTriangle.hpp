/*
 * CMyTriangle.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CMYTRIANGLE_HPP_
#define CMYTRIANGLE_HPP_
#include "CMyPoint.hpp"
#include <string>

class CMyTriangle
{
private:
	CMyPoint mV1;
	CMyPoint mV2;
	CMyPoint mV3;

public:
	CMyTriangle(int x1, int y1,
				int x2, int y2,
				int x3, int y3);

	CMyTriangle(CMyPoint v1, CMyPoint v2, CMyPoint v3);
	std::string toString();
	double getPerimeter();
	std::string getType();

	virtual ~CMyTriangle();
};

#endif /* CMYTRIANGLE_HPP_ */
