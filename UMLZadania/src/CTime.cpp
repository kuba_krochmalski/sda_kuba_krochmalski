/*
 * CTime.cpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#include "CTime.hpp"

CTime::CTime(int hour, int minute, int second)
: mHour(hour)
, mMinute(minute)
, mSecond(second)
{
	// TODO Auto-generated constructor stub

}

int CTime::getHour() const
{
	return mHour;
}

void CTime::setHour(int hour)
{
	mHour = hour;
}

int CTime::getMinute() const
{
	return mMinute;
}

void CTime::setMinute(int minute)
{
	mMinute = minute;
}

int CTime::getSecond() const
{
	return mSecond;
}

void CTime::setSecond(int second)
{
	mSecond = second;
}

void CTime::setTime(int hour, int minute, int second)
{
	mHour = hour;
	mMinute = minute;
	mSecond = second;
}

CTime::~CTime()
{
	// TODO Auto-generated destructor stub
}

