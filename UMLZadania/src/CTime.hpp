/*
 * CTime.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CTIME_HPP_
#define CTIME_HPP_
#include <string>

class CTime
{
private:
	int mHour;
	int mMinute;
	int mSecond;

public:
	CTime(int hour, int minute, int second);
	virtual ~CTime();

	int getHour() const;
	void setHour(int hour);
	int getMinute() const;
	void setMinute(int minute);
	int getSecond() const;
	void setSecond(int second);
	void setTime(int hour, int minute, int second);

	std::string toString();
	CTime nextSecond();
	CTime previousSecond();
};

#endif /* CTIME_HPP_ */
