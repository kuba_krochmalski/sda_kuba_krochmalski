/*
 * CAutomat.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "CAutomat.hpp"
#include <cstdio>
#include <cmath>
#include <iostream>

using namespace std;

CAutomat::CAutomat(double reszta)
: mReszta(reszta)
{
	mNominaly[0] = new Pieniadz(500.0, 1);
	mNominaly[1] = new Pieniadz(200.0, 1);
	mNominaly[2] = new Pieniadz(100.0, 1);
	mNominaly[3] = new Pieniadz(50.0, 1);
	mNominaly[4] = new Pieniadz(20.0, 1);
	mNominaly[5] = new Pieniadz(10.0, 1);
	mNominaly[6] = new Pieniadz(5.0, 1);
	mNominaly[7] = new Pieniadz(2.0, 1);
	mNominaly[8] = new Pieniadz(1.0, 1000);
	mNominaly[9] = new Pieniadz(0.5, 1);
	mNominaly[10] = new Pieniadz(0.2, 1);
	mNominaly[11] = new Pieniadz(0.1, 1);
	mNominaly[12] = new Pieniadz(0.05, 1);
	mNominaly[13] = new Pieniadz(0.02, 1);
	mNominaly[14] = new Pieniadz(0.01, 1000);

	mIlosc[0] = 0;
	mIlosc[1] = 0;
	mIlosc[2] = 0;
	mIlosc[3] = 0;
	mIlosc[4] = 0;
	mIlosc[5] = 0;
	mIlosc[6] = 0;
	mIlosc[7] = 0;
	mIlosc[8] = 0;
	mIlosc[9] = 0;
	mIlosc[10] = 0;
	mIlosc[11] = 0;
	mIlosc[12] = 0;
	mIlosc[13] = 0;
	mIlosc[14] = 0;
}

CAutomat::~CAutomat()
{
	for (int i  = 0; i < 15; ++i)
	{
		delete mNominaly[i];
	}
}

void CAutomat::wydaj()
{
	cout << "WYDAJE: " << mReszta << " zl" << endl << endl;
	double iloraz;
	double pozostalosc = mReszta;

	for (int i = 0; i < 15; ++i)
	{
		iloraz = pozostalosc/mNominaly[i]->getWartosc();
		mIlosc[i] = floor(iloraz);

		if(iloraz > 1)
		{
			if(mNominaly[i]->getIlosc() >= mIlosc[i])
			{
				pozostalosc = pozostalosc - (mIlosc[i] * mNominaly[i]->getWartosc());
				cout << "wydaje " << mIlosc[i] << " x " << mNominaly[i]->getWartosc() << " zl" << endl;
				if (pozostalosc > 0.005)
				{
					cout << "pozostalo: " << pozostalosc << " zl" << endl;
				}
				cout << "-----------------------" << endl;
			}

			else
			{
				pozostalosc = pozostalosc - (mNominaly[i]->getIlosc() * mNominaly[i]->getWartosc());
				cout << "wydaje " << mNominaly[i]->getIlosc() << " x " << mNominaly[i]->getWartosc() << " zl" << endl;
				if (pozostalosc > 0.005)
				{
					cout << "pozostalo: " << pozostalosc << " zl" << endl;
				}
				cout << "-----------------------" << endl;
			}
		}
	}
}

