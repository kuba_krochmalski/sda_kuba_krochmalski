/*
 * CAutomat.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CAUTOMAT_HPP_
#define CAUTOMAT_HPP_
#include "Pieniadz.hpp"

class CAutomat
{
private:
	double mReszta;
	Pieniadz* mNominaly[15];
	int mIlosc[15];

public:
	CAutomat(double reszta);
	virtual ~CAutomat();
	void wydaj();
};

#endif /* CAUTOMAT_HPP_ */
