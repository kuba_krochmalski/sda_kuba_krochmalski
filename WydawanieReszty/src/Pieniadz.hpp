/*
 * Pieniadz.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PIENIADZ_HPP_
#define PIENIADZ_HPP_

class Pieniadz
{
private:
	double mWartosc;
	int mIlosc;

public:
	Pieniadz(double wartosc, int ilosc);
	virtual ~Pieniadz();

	int getIlosc() const
	{
		return mIlosc;
	}

	double getWartosc() const
	{
		return mWartosc;
	}
};

#endif /* PIENIADZ_HPP_ */
