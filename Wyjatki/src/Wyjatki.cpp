//============================================================================
// Name        : Wyjatki.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

double dziel(double a, double b)
{
	if(b == 0)
	{
		string error = "Nie dziel przez 0!";
		throw error;
	}
	else
	{
		return a/b;
	}
}

int main()
{
	double a, b;

	cout << "Podaj pierwsza liczbe: " << flush;
	cin >> a;

	cout << "Podaj druga liczbe: " << flush;
	cin >> b;

	try
	{
		cout << "Wynik dzielenia: " << dziel(a, b) << endl;
	}
	catch(string& error)
	{
		cout << error << endl;
	}

	return 0;
}
