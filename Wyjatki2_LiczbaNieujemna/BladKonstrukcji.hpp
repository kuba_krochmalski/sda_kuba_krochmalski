/*
 * BladKonstrukcji.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef BLADKONSTRUKCJI_HPP_
#define BLADKONSTRUKCJI_HPP_
#include <exception>
#include <iostream>

struct BladKonstrukcji : public std::exception
{
private:
	int blednaLiczba;

public:

	BladKonstrukcji(int liczba);
	virtual const char* what() const throw();

	int getBlednaLiczba() const
	{
		return blednaLiczba;
	}
};

#endif /* BLADKONSTRUKCJI_HPP_ */
