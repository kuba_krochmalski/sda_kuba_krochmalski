/*
 * LiczbaNieujemna.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#include "LiczbaNieujemna.hpp"
#include <string>
#include <exception>
#include <iostream>
#include "BladKonstrukcji.hpp"
#include "ZleArgumenty.hpp"


LiczbaNieujemna::LiczbaNieujemna(int v)
: value(v)
{
	if(v < 0)
	{
		throw BladKonstrukcji(v);
	}
}

int LiczbaNieujemna::getValue() const
{
	return value;
}

void LiczbaNieujemna::setValue(int value)
{
	this->value = value;
}

LiczbaNieujemna::~LiczbaNieujemna()
{
	// TODO Auto-generated destructor stub
}

LiczbaNieujemna LiczbaNieujemna::operator- (LiczbaNieujemna druga)
{
	if(druga.getValue() > this->getValue())
	{
		throw ZleArgumenty();
	}
	else
	{
		return LiczbaNieujemna(this->getValue() - druga.getValue());
	}
}

LiczbaNieujemna LiczbaNieujemna::operator*(LiczbaNieujemna druga)
{
	return LiczbaNieujemna(this->getValue() * druga.getValue());
}

LiczbaNieujemna LiczbaNieujemna::operator/(LiczbaNieujemna druga)
{
	if(druga.getValue() == 0)
	{
		throw ZleArgumenty();
	}
	else
	{
		return LiczbaNieujemna(this->getValue() / druga.getValue());
	}
}

