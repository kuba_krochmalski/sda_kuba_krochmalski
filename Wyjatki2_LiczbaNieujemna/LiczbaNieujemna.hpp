/*
 * LiczbaNieujemna.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef LICZBANIEUJEMNA_HPP_
#define LICZBANIEUJEMNA_HPP_

class LiczbaNieujemna
{
private:
	int value;
	
public:
	LiczbaNieujemna(int v);
	virtual ~LiczbaNieujemna();

	int getValue() const;
	void setValue(int value);

	LiczbaNieujemna operator- (LiczbaNieujemna druga);
	LiczbaNieujemna operator* (LiczbaNieujemna druga);
	LiczbaNieujemna operator/ (LiczbaNieujemna druga);
};

#endif /* LICZBANIEUJEMNA_HPP_ */
