/*
 * main.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>
#include <exception>
#include "LiczbaNieujemna.hpp"
#include "BladKonstrukcji.hpp"
#include "ZleArgumenty.hpp"

int main()
{
	try
	{
		LiczbaNieujemna pierwsza(-2);
		LiczbaNieujemna druga(0);
		LiczbaNieujemna trzecia(6);

		try
		{
			std::cout << (pierwsza/druga).getValue() << std::endl;
		}
		catch(ZleArgumenty& error)
		{
			std::cout << error.what() << std::endl;
		}

		try
		{
			std::cout << (pierwsza-trzecia).getValue() << std::endl;
		}
		catch (ZleArgumenty& error)
		{
			std::cout << error.what() << std::endl;
		}
	}

	catch (BladKonstrukcji& error)
	{
		std::cout << error.what() << " (" << error.getBlednaLiczba() << ")" << std::endl;
	}

	return 0;
}



