/*
 * AircraftException.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef AIRCRAFTEXCEPTION_HPP_
#define AIRCRAFTEXCEPTION_HPP_
#include <exception>

class AircraftException : public std::exception
{
public:
  AircraftException(const char* errMessage)
  : m_ErrMessage(errMessage)
{

}

  // overriden what() method from exception class
  const char* what() const throw() { return m_ErrMessage; }

private:
  const char* m_ErrMessage;

};

#endif /* AIRCRAFTEXCEPTION_HPP_ */
