/*
 * NoRunWay.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef NORUNWAY_HPP_
#define NORUNWAY_HPP_
#include "AircraftException.hpp"

class NoRunWay : public AircraftException
{
public:
  NoRunWay(const char* errMessage)
  : AircraftException(errMessage)
{

}

};

#endif /* NORUNWAY_HPP_ */
