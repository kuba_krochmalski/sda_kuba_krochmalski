/*
 * WingBroken.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef WINGBROKEN_HPP_
#define WINGBROKEN_HPP_
#include "AircraftException.hpp"

class WingBroken : public AircraftException
{
public:
  WingBroken(const char* errMessage)
  : AircraftException(errMessage)
{

}

};

#endif /* WINGBROKEN_HPP_ */
