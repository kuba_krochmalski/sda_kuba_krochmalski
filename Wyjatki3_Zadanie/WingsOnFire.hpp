/*
 * WingsOnFire.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef WINGSONFIRE_HPP_
#define WINGSONFIRE_HPP_
#include "AircraftException.hpp"

class WingsOnFire : public AircraftException
{
public:
  WingsOnFire(const char* errMessage)
  : AircraftException(errMessage)
{

}

};

#endif /* WINGSONFIRE_HPP_ */
