#include <iostream>
#include <iostream>
#include <exception>
#include "WingsOnFire.hpp"
#include "AircraftException.hpp"
#include "WingBroken.hpp"
#include "NoRunWay.hpp"

using namespace std;

//enum AircraftError
//{
//  WingsOnFire = 1,
//  WingBroken = 2,
//  NoRunway = 3,
//  Crahed = 4
//};


int main()
{
  try
  {
    throw WingsOnFire("pali sie!");
    throw WingBroken("nie ma skrzydla!");
    throw NoRunWay("nie ma rozbiegu!");
  }
  catch (WingsOnFire& e)
  {
	  cout << e.what() << endl;
  }
  catch (WingBroken& e)
  {
	  cout << e.what() << endl;
  }
  catch (NoRunWay& e)
  {
	  cout << e.what() << endl;
  }
  catch (AircraftException& e)
  {
		cout << e.what() << endl;
  }

  return 0;
}




