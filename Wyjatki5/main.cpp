#include <iostream>
#include <string>
#include <typeinfo>

using namespace std;

int main()
{
	string strg1("Test");
	string strg2("ing");

	try
	{
		strg1.append(strg2, 4, 2);
	}
	catch(std::out_of_range& ex)
	{
		cout << ex.what() << endl;
	}
	catch(std::exception& ex)
	{
		cout << typeid(ex).name() << endl;
		cout << ex.what() << endl;
	}

	cout << strg1 << endl;

	return 0;
}


