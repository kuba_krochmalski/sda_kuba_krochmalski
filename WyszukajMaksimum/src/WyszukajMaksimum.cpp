//============================================================================
// Name        : WyszukajMaksimum.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
	int size = 10;
	int tab[size] = {1 ,3 ,8 ,1, 13, 4, 2, 67, 24, 31};
	int max = tab[0];

	for (int i = 1; i < size; ++i)
	{
		max = (tab[i] > max) ? tab[i] : max;
	}

	cout << max << endl;

	return 0;
}
