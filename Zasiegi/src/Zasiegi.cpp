//============================================================================
// Name        : Zasiegi.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int zmiennaGlobalna = 50;

void zwieksz()
{
	zmiennaGlobalna += 3;
	cout << "Zwiekszam." << endl;
}

void zmniejsz()
{
	zmiennaGlobalna -= 2;
	cout << "Zmniejszam." << endl;
}

void wypisz()
{
	cout << "Zmienna globalna: " << zmiennaGlobalna << endl;
}

int main()
{
	wypisz();
	zwieksz();
	zmniejsz();
	wypisz();
	zwieksz();
	zmniejsz();
	wypisz();

	return 0;
}
