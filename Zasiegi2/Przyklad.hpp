/*
 * Przyklad.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef PRZYKLAD_HPP_
#define PRZYKLAD_HPP_
#include <string>

extern int global_counter;
extern float global_float;
extern std::string global_string;

void zmienString(std::string tekst);
void zmienFloat(float liczba);

#endif /* PRZYKLAD_HPP_ */
