/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Przyklad.hpp"
#include <iostream>

int global_counter;
float global_float;
std::string global_string;

int main()
{
	global_counter = 0;
	global_float = 1.23f;
	global_string = "patataj";

	zmienString("a");
	zmienFloat(0.01);

	std::cout << global_float << std::endl;
	std::cout << global_string << std::endl;
	std::cout << global_counter << std::endl;

	return 0;
}



