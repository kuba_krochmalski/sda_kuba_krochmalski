/*
 * Circle.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_
#include "Shape.hpp"

class Circle : public Shape
{
protected:
	float mRadius;

public:
	Circle();
	Circle(float r);
	virtual ~Circle();
	virtual float pole();
	virtual std::string getNazwa();

};

#endif /* CIRCLE_HPP_ */
