/*
 * Cylinder.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Cylinder.hpp"

Cylinder::Cylinder()
: Circle()
, mWysokosc(1.0)
{
	// TODO Auto-generated constructor stub

}

Cylinder::Cylinder(float h)
: Circle()
, mWysokosc(h)
{
	// TODO Auto-generated constructor stub

}

Cylinder::Cylinder(float r, float h)
: Circle(r)
, mWysokosc(h)
{
	// TODO Auto-generated constructor stub

}

Cylinder::~Cylinder()
{
	// TODO Auto-generated destructor stub
}

float Cylinder::pole()
{
	return 2*(mRadius*mRadius*3.1415926) + mWysokosc*(2*mRadius*3.1415926);
}

std::string Cylinder::getNazwa()
{
	return "Cylinder";
}
