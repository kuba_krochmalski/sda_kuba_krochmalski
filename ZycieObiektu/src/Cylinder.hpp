/*
 * Cylinder.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CYLINDER_HPP_
#define CYLINDER_HPP_
#include "Circle.hpp"

class Cylinder : public Circle
{
private:
	float mWysokosc;

public:
	Cylinder();
	Cylinder(float h);
	Cylinder(float r, float h);
	virtual ~Cylinder();
	float pole();
	std::string getNazwa();
};

#endif /* CYLINDER_HPP_ */
