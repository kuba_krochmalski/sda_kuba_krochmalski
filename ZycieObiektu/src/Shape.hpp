/*
 * Shape.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_
#include <string>

class Shape
{
public:
	Shape();
	virtual ~Shape();
	virtual float pole() = 0;
	virtual std::string getNazwa() = 0;

};

#endif /* SHAPE_HPP_ */
