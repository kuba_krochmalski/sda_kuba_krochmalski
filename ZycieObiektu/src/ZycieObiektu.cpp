
#include <iostream>
#include "Shape.hpp"
#include "Circle.hpp"
#include "Cylinder.hpp"

using namespace std;

int main()
{
	Cylinder cylinder(1, 4);
	Circle kolo(1);

	Shape* wsk = &cylinder;
	std::cout << wsk->getNazwa() << std::endl;
	std::cout << wsk->pole() << std::endl;
	std::cout << "----------------" << std::endl;

	wsk = &kolo;
	std::cout << wsk->getNazwa() << std::endl;
	std::cout << wsk->pole() << std::endl;





}
