/*
 * MaszynaMnozaca.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef MASZYNAMNOZACA_HPP_
#define MASZYNAMNOZACA_HPP_

class MaszynaMnozaca
{
protected:
	float mMnoznik;

public:
	MaszynaMnozaca(float x);
	virtual ~MaszynaMnozaca() = default;
	virtual float pomnoz(float x) final;
	float pomnoz(int) = delete;
};

#endif /* MASZYNAMNOZACA_HPP_ */
