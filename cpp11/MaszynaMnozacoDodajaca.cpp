/*
 * MaszynaMnozacoDodajaca.cpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#include "MaszynaMnozacoDodajaca.hpp"

MaszynaMnozacoDodajaca::MaszynaMnozacoDodajaca(float x, float y)
: MaszynaMnozaca(x)
, mDodajnik(y)
{
}


float MaszynaMnozacoDodajaca::dodaj(float x)
{
	return mDodajnik + x;
}

