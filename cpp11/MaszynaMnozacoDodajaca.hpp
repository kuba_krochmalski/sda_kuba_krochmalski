/*
 * MaszynaMnozacoDodajaca.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef MASZYNAMNOZACODODAJACA_HPP_
#define MASZYNAMNOZACODODAJACA_HPP_
#include "MaszynaMnozaca.hpp"

class MaszynaMnozacoDodajaca : public MaszynaMnozaca
{
private:
	float mDodajnik;

public:
	MaszynaMnozacoDodajaca(float x, float y);
	virtual ~MaszynaMnozacoDodajaca() = default;
	float dodaj(float x);
};

#endif /* MASZYNAMNOZACODODAJACA_HPP_ */
