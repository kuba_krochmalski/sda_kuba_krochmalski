/*
 * Octopus.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef OCTOPUS_HPP_
#define OCTOPUS_HPP_

class Octopus
{
public:
	Octopus() = default;
	Octopus(const Octopus&) = delete;
	Octopus& operator=(const Octopus&) = delete;

	virtual ~Octopus();
	void mackuj();
};

#endif /* OCTOPUS_HPP_ */
