/*
 * main.cpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include "Fish.hpp"
#include "GoldFish.hpp"
#include "Octopus.hpp"
#include "MaszynaMnozaca.hpp"
#include "MaszynaMnozacoDodajaca.hpp"
#define POTEGA_DWOJKI(x) pow(2, x)

constexpr int potegaDwojki(int x)
{
	return pow(2, x);
}

constexpr int silnia(int x)
{
	return ((x <= 1) ? 1 : silnia(x - 1)*x);
}

void Hello()
{
	std::cout << "Hello world!" << std::endl;
}

int main()
{
//	int a = POTEGA_DWOJKI(0);
//	int b = POTEGA_DWOJKI(1);
//	int c = POTEGA_DWOJKI(2);
//	int d = POTEGA_DWOJKI(3);
//	int e = POTEGA_DWOJKI(4);

//	int a = potegaDwojki(0);
//	int b = potegaDwojki(1);
//	int c = potegaDwojki(2);
//	int d = potegaDwojki(3);
//	int e = potegaDwojki(4);

	int a = silnia(0);
	int b = silnia(1);
	int c = silnia(2);
	int d = silnia(3);
	int e = silnia(4);

	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;
	std::cout << d << std::endl;
	std::cout << e << std::endl;

	Fish fish;
	fish.swim(5);

	GoldFish gold;
	gold.swim(5);

	Octopus octo;
	octo.mackuj();

//	Octopus octo2 = octo;

	MaszynaMnozaca razy10(10.0);
	std::cout << razy10.pomnoz(2.3f) << std::endl;
	std::cout << razy10.pomnoz(3.4f) << std::endl;
//	std::cout << razy10.pomnoz(2) << std::endl;

	MaszynaMnozacoDodajaca maszyna(5.0, 3.0);
	std::cout << maszyna.dodaj(4.0) << std::endl;

	std::vector<int> liczbyCalkowite;
	for(int i = 0; i < 100; ++i)
	{
		liczbyCalkowite.push_back(i + 1);
	}

	for(auto it = liczbyCalkowite.begin(); it != liczbyCalkowite.end(); advance(it, 2))
	{
		std::cout << *it << " ";
	}

	std::cout << std::endl;

	for(auto& val : liczbyCalkowite)
	{
		std::cout << val << " ";
	}

	std::cout << std::endl;

	float tab[10] = {1.7, 2.3, 9.9, 5.6, 7.0, 6.1, 6.6, 0.8, 1.8, 2.5};

	for(auto& val : tab)
	{
		std::cout << val << " | ";
	}

	std::cout << std::endl;

	std::list<int> listaLiczbC;
	for(int i = 0; i < 10; ++i)
	{
		listaLiczbC.push_back(i + 1);
	}

	for(auto& val : listaLiczbC)
	{
		val *= 2;
	}

	for(auto& val : listaLiczbC)
	{
		std::cout << val << " ";
	}

	std::cout << std::endl;

	auto lambda = [](){std::cout << "Hello world!" << std::endl;};
	auto dodawanie = [](int x, int y){return x+y;};

	Hello();
	lambda();
	std::cout << dodawanie(4, 7) << std::endl;

	return 0;
}



