#include <iostream>
#include <string>
#include <vector>
#include <tuple>

using namespace std;

class KontoBankowe
{
public:
	string imieWlasciciela;
	float stanKonta;
	float oprocentowanie;

	KontoBankowe(string imie, float stan, float procent)
	: imieWlasciciela(imie)
	, stanKonta(stan)
	, oprocentowanie(procent)
	{
	}

	KontoBankowe(string imie)
	: KontoBankowe(imie, 0, 0)
	{
	}

	KontoBankowe()
	: KontoBankowe("", 0, 0)
	{
	}

	~KontoBankowe() = default;
};

class KontoBankoweVIP : public KontoBankowe
{
	using KontoBankowe::KontoBankowe;

public:
	void wejdzBezKolejki()
	{
		cout << "wchodze..." << endl;
	}
};

enum class KolorKota
{
	BIALY,
	CZARNY,
	TRICOLOR
};

enum class KolorPsa
{
	BIALY,
	CZARNY
};

tuple<string, float, int> dajPacjenta(int id)
{
	if(id == 1)
	{
		return make_tuple("Tomek", 1.75, 98);
	}
	else
	{
		return make_tuple("Wojtek", 1.94, 115);
	}
}

int main()
{
	KontoBankoweVIP k1("Kuba", 1000000.0f, 2.0f);
	KontoBankowe k2("Waclaw");
	KontoBankowe k3;

	cout << k1.imieWlasciciela << " " << k1.stanKonta << " " << k1.oprocentowanie << endl;
	k1.wejdzBezKolejki();
	cout << k2.imieWlasciciela << " " << k2.stanKonta << " " << k2.oprocentowanie << endl;
	cout << k3.imieWlasciciela << " " << k3.stanKonta << " " << k3.oprocentowanie << endl;

	vector<KontoBankowe> v = {{"Jan", 200, 0.5}, {"Anna", 100, 0.5}, {"Henryk", 6000, 1.0}};

	for(auto it = v.begin(); it != v.end(); ++it)
	{
		cout << it->imieWlasciciela << " " << it->stanKonta << " " << it->oprocentowanie << endl;
	}

	KolorKota kolor = KolorKota::BIALY;
	KolorPsa kolor2 = KolorPsa::CZARNY;

	auto krotka = dajPacjenta(2);

	cout << get<0>(krotka) << endl;
	cout << get<1>(krotka) << endl;
	cout << get<2>(krotka) << endl;


	return 0;
}




