/*
 * main.cpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

void print(int x)
{
	cout << x << " ";
}

bool czyDodatnia(int x)
{
	return x > 0;
}

int main()
{
	vector<int> vec(201);

	iota(vec.begin(), vec.end(), -100);

	for_each(vec.begin(), vec.end(), print);
	cout << endl;

	cout << all_of(vec.begin(), vec.end(), czyDodatnia) << endl;
	cout << endl;

	auto czyPodzielna = [](int x)
		{
			return ((x % 3 == 0) && (x % 5 == 0) && (x % 30 == 0));
		};

	cout << any_of(vec.begin(), vec.end(), czyPodzielna) << endl;
	cout << endl;

	remove(vec.begin(), vec.end(), 0);
	for_each(vec.begin(), vec.end(), print);
	cout << endl;

	cout << any_of(vec.begin(), vec.end(), [](int x){return x == 0;}) << endl;

	cout << is_sorted(vec.begin(), vec.end()) << endl;

	vector<int> drugiVec;

	copy_if(vec.begin(), vec.end(), back_inserter(drugiVec), [](int x){return ((x > 90)||(x < -90));});
	for_each(drugiVec.begin(), drugiVec.end(), print);
	cout << endl;

	vector<int> trzeciVec(10);

	auto it = find(vec.begin(), vec.end(), 78);
	copy_n(it, 10, trzeciVec.begin());
	for_each(trzeciVec.begin(), trzeciVec.end(), print);








	return 0;
}



