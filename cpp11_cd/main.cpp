#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void print(int x)
{
	cout << x << " ";
}

struct mniejszeOd
{
	int liczba;

	mniejszeOd(int n)
	{
		liczba = n;
	}

	void operator()(const int& x)
	{
		if(x < liczba)
		{
			cout << x << " ";
		}
	}
};


int main()
{
	vector<int> v;

	for(int i = 0; i < 100; ++i)
	{
		v.push_back(i + 1);
	}

	for_each(v.begin(), v.end(), print);
	cout << endl;
	for_each(v.begin(), v.end(), [](int x){cout << x << " ";});
	cout << endl;
	for_each(v.begin(), v.end(), mniejszeOd(20));
	cout << endl;
	for_each(v.begin(), v.end(), [](int x){if(x < 20){cout << x << " ";}});
	cout << endl;

	int dzielnik = 5;
	int sumaLiczbParzystych = 0;

	auto podzielnePrzezDzielnik = [dzielnik](int x)
		{
			if(x % dzielnik == 0)
			{
				cout << x << " ";
			}
		};

	auto sumaParzystych = [&sumaLiczbParzystych](int x)
			{
				if(x % 2 == 0)
				{
					sumaLiczbParzystych += x;
				}
			};

	for_each(v.begin(), v.end(), podzielnePrzezDzielnik);
	cout << endl;

	for_each(v.begin(), v.end(), sumaParzystych);
	cout << endl;
	cout << sumaLiczbParzystych;
	cout << endl;

	sort(v.begin(), v.end(), [](int first, int second){return first > second;});
	for_each(v.begin(), v.end(), print);

	return 0;
}




