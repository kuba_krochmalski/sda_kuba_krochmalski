/*
 * Produkt.hpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */

#ifndef PRODUKT_HPP_
#define PRODUKT_HPP_
#include <string>

class Produkt
{
public:
	float mCena;
	std::string mNazwa;

public:
	Produkt(float cena, std::string nazwa);
	Produkt();
	virtual ~Produkt() = default;
};

#endif /* PRODUKT_HPP_ */
