/*
 * main.cpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>
#include <memory>
#include "Produkt.hpp"

using namespace std;

Produkt stworz(int klasaProduktu, string nazwa)
{
	switch(klasaProduktu)
	{
	case 1:
		return Produkt(1000.99, nazwa);
		break;
	case 2:
		return Produkt(500.0, nazwa);
		break;
	case 3:
		return Produkt(19.99, nazwa);
		break;
	default:
		return Produkt(0.0, "Barachlo");
		break;
	}
}

int main()
{
//	auto cos = stworz(1, "supercos");
//	cout << cos.mNazwa << " " << cos.mCena << endl;

	shared_ptr<Produkt> ptr1 = make_shared<Produkt>();
	shared_ptr<Produkt> ptr2 = make_shared<Produkt>(129.99, "Fajny produkt");

//	shared_ptr<Produkt[]> tabPtr = make_shared<Produkt[]>(3);

	return 0;
}



