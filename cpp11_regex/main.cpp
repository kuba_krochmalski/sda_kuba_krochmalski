/*
 * main.cpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */

#include <iostream>
#include <regex>
#include <string>

using namespace std;

int main()
{
	string email;
	regex warunek("^[a-z 0-9]+@[a-z]*.(pl|com)$");

	while(true)
	{
		cout << "Podaj e-mail: " << flush;
		cin >> email;

		if(regex_match(email, warunek))
		{
			cout << "E-mail poprawny!" << endl;
		}
		else
		{
			cout << "E-mail niepoprawny!" << endl;
		}
	}

	return 0;
}


