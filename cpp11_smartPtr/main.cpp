#include "Kurczak.hpp"
#include <iostream>
#include <memory>
#include <vector>

using namespace std;

void fun()
{
	Kurczak* kurczakPtr = new Kurczak();

	unique_ptr<Kurczak> uniqueKurczak(new Kurczak());
}

int main()
{
	vector<shared_ptr<Kurczak>> kurczaki;

	shared_ptr<Kurczak> ptr = make_shared<Kurczak>();

	kurczaki.push_back(ptr);
	kurczaki.push_back(ptr);
	kurczaki.push_back(ptr);
	kurczaki.push_back(ptr);

	cout << ptr.use_count() << endl;

	ptr.reset();

	cout << ptr.use_count() << endl;

	cout << kurczaki[0].use_count() << endl;

	kurczaki.clear();

	return 0;
}




