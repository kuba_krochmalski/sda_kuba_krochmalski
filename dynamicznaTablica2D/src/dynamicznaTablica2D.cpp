#include <iostream>

void pokaz(float** m, int maxX, int maxY);

int main()
{
	const int stala[4][5] = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12,
			13, 14, 15 }, { 16, 17, 18, 19, 20 }};

	std::cout << stala[1][4] << std::endl;
	std::cout << "--------------------------" << std::endl;

	float** macierz = NULL;

	macierz = new float*[4];

	for (int i=0; i<4; ++i)
	{
		macierz[i] = new float[5];
	}

	for (int i = 0; i < 4; ++i)
	{
		for (int k = 0; k < 5; ++k)
		{
			macierz[i][k] = stala[i][k]*1.5;
		}
	}

	pokaz(macierz, 5, 4);

	for (int i = 0; i < 4; ++i)
	{
		delete[] macierz[i];
	}

	delete[] macierz;

	std::cout << "--------------------------" << std::endl;
	pokaz(macierz, 5, 4);

	return 0;
}

void pokaz(float** m, int maxX, int maxY)
{
	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			std::cout << m[y][x] << ' ';
		}
		std::cout << std::endl;
	}

}
