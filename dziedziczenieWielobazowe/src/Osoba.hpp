/*
 * Osoba.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_

class Osoba
{
protected:
	long long mPESEL;

public:
	Osoba(long long pesel = 0);
	~Osoba();
	void podajPESEL();
};

#endif /* OSOBA_HPP_ */
