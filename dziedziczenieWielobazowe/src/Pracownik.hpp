/*
 * Pracownik.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACOWNIK_HPP_
#define PRACOWNIK_HPP_
#include<string>
#include "Osoba.hpp"

class Pracownik: public virtual Osoba
{
protected:
	float mPensja;
	std::string mZawod;

public:
	Pracownik(float pensja, std::string zawod, long long pesel);
	Pracownik(float pensja, std::string zawod);
	~Pracownik();
	virtual void pracuj();
};

#endif /* PRACOWNIK_HPP_ */
