/*
 * PracujacyStudent.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include "PracujacyStudent.hpp"
#include<iostream>
#include<string>

PracujacyStudent::PracujacyStudent(int numer, std::string kierunek, float pensja,
		std::string zawod, long long pesel)
: Osoba(pesel)
, Student(numer, kierunek)
, Pracownik(pensja, zawod)
{

}

void PracujacyStudent::uczSie()
{
	std::cout << "Ucze sie i pracuje" << std::endl;
}


void PracujacyStudent::pracuj()
{
	std::cout << "Pracuje i ucze sie" << std::endl;
}


