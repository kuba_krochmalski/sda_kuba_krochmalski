/*
 * PracujacyStudent.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACUJACYSTUDENT_HPP_
#define PRACUJACYSTUDENT_HPP_
#include "Student.hpp"
#include "Pracownik.hpp"
#include "Osoba.hpp"
#include <string>

class PracujacyStudent : public Student, public Pracownik
{
public:
	PracujacyStudent(int numer, std::string kierunek, float pensja,
			std::string zawod, long long pesel);

	void uczSie();
	void pracuj();


};

#endif /* PRACUJACYSTUDENT_HPP_ */
