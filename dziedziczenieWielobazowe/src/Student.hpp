/*
 * Student.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include<string>
#include "Osoba.hpp"

class Student: public virtual Osoba
{
protected:
	int mNrIndeksu;
	std::string mKierunek;

public:
	Student(int numer, std::string kierunek, long long pesel);
	Student(int numer, std::string kierunek);
	~Student();
	void podajNumer();
	virtual void uczSie();
};

#endif /* STUDENT_HPP_ */
