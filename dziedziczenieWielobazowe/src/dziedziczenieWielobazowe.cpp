
#include <iostream>
#include "PracujacyStudent.hpp"
#include "Osoba.hpp"

using namespace std;

class Ptaszek
{
public:
	void virtual spiewaj()
	{
		std::cout << "Cwir cwir cwir!" << std::endl;
	}
	virtual ~Ptaszek()
	{

	}
};

class Kukulka: public Ptaszek
{
public:
	void spiewaj()
	{
		std::cout << "Kuku kuku!" << std::endl;
	}
	virtual ~Kukulka()
	{

	}
};

int main()
{
	PracujacyStudent andrzej(12345678, "Sratatata", 1600.0, "KFC", 1246543152);
	Pracownik prac(1500.0, "Tynkarz", 91273292);
	Student stud(9876543, "Kuku", 728734637);

	andrzej.podajNumer();
	andrzej.pracuj();
	std::cout << "-------------------" << std::endl;

	prac.pracuj();
	prac.podajPESEL();
	std::cout << "-------------------" << std::endl;

	stud.podajNumer();
	stud.podajPESEL();
	std::cout << "-------------------" << std::endl;

//	Ptaszek *wsk;
//	Ptaszek ptak1;
//	Kukulka kukulka1;
//
//	wsk = &ptak1;
//	wsk->spiewaj();
//
//	wsk = &kukulka1;
//	wsk->spiewaj();

	Pracownik *wsk;
	wsk = &prac;
	wsk->pracuj();
	wsk = &andrzej;
	wsk->pracuj();

	std::cout << "-------------------" << std::endl;

	Student *wsk1;
	wsk1 = &stud;
	wsk1->uczSie();
	wsk1 = &andrzej;
	wsk1->uczSie();




	return 0;
}
