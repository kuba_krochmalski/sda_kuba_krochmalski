#ifndef DZIEDZICZENIE_HPP_

#define DZIEDZICZENIE_HPP_

#include <iostream>

using namespace std;

class Poczatek

{

protected:

	int mX;

	string mTekst;

public:

	void przedstawSie()

	{

		cout << "Jestem Poczatek \n";

	}

	Poczatek()

	:
			mX(0), mTekst("DUPA")

	{

		cout << "Poczatek\n";

	}

	Poczatek(int x)

	:
			mX(x), mTekst("DUPA")

	{

		cout << "Poczatek(x)\n";

	}

	Poczatek(int x, string tekst)

	:
			mX(x), mTekst(tekst)

	{

		cout << "Poczatek(x, string)\n";

	}

	~Poczatek()

	{

		cout << "~Poczatek\n";

	}

	void ustawX()

	{

		mX = 1;

	}

	void ustawTekst(string tekst)

	{

		mTekst = tekst;

	}

	void wypiszX()

	{

		std::cout << "Poczatek::X=" << mX << endl;

	}

	void wypiszTekst()

	{

		std::cout << "Poczatek::Tekst= " << mTekst << endl;

	}

};

class Srodek: public Poczatek

{

protected:

	double mX;

	string mTekst;

public:

	void przedstawSie()

	{

		cout << "Jestem Srodek \n";

	}

	Srodek()

	:
			Poczatek(99, "AAAAAAA")

			, mX(10.31), mTekst("DUPA")

	{

		cout << "Srodek\n";

	}

	Srodek(double x)

	:
			Poczatek(33, "BBBBBBB")

			, mX(x), mTekst("DUPA")

	{

		cout << "Srodek(x)\n";

	}

	Srodek(double x, string tekst)

	:
			Poczatek(33, "CCCCCCC")

			, mX(x), mTekst(tekst)

	{

		cout << "Srodek(x, string)\n";

	}

	~Srodek()

	{

		cout << "~Srodek\n";

	}

	void ustawX()

	{

		Poczatek::mX = 11;

		mX = 12.31;

	}

	void ustawTekst(string tekst)

	{

		Poczatek::mTekst = tekst;

		mTekst = tekst + tekst;

	}

	void wypiszX()

	{

		std::cout << "Poczatek::X=" << Poczatek::mX << "Srodek::X=" << mX
				<< endl;

	}

	void wypiszTekst()

	{

		std::cout << "Poczatek::Tekst= " << Poczatek::mTekst
				<< "Srodek::Tekst= " << mTekst << endl;

	}

};

class Koniec: public Srodek

{

protected:

	int mX;

public:

	void przedstawSie()

	{

		cout << "Jestem Koniec \n";

	}

	Koniec()

	:
			Srodek(3213.333)

			, mX(100)

	{

		cout << "Koniec\n";

	}

	Koniec(int x)

	:
			Srodek(3213.333)

			, mX(x)

	{

		cout << "Koniec(x)\n";

	}

	~Koniec()

	{

		cout << "~Koniec\n";

	}

	void ustawX()

	{

		Poczatek::mX = 111;

		Srodek::Poczatek::mX = 112.432;

		mX = 113;

	}

	void wypiszX()

	{

		std::cout << "Poczatek::X=" << Poczatek::mX << "Srodek::X="
				<< Srodek::mX << "Koniec::X=" << mX << endl;

	}

};

#endif /* DZIEDZICZENIE_HPP_ */
