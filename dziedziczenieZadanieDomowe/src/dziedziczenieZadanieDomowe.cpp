#include <iostream>

#include "dziedziczenie.hpp"

int main()

{

	Poczatek *pocz = new Poczatek(4, "DUPA"); // wywo�a konstruktor z parametrem

	Srodek *srod = new Srodek(222.12, "DUDUDU"); //wywo�a konstruktor z parametrem dla klasy Srodek i dla klasy Poczatek

	Koniec *koni = new Koniec(421); //wywo�a konstruktor z parametrem dla klasy Koniec, Srodek i Poczatek

	pocz->przedstawSie(); //wywo�anie metody przedstaw si� dla OBIEKTU klasy Poczatek

	srod->przedstawSie(); //wywo�anie metody przedstaw si� dla OBIEKTU klasy Srodek

	koni->przedstawSie(); //wywo�anie metody przedstaw si� dla OBIEKTU klasy Koniec

	koni->Poczatek::przedstawSie(); //wywo�anie metody przedstaw si� z zasi�gu Poczatek dla OBIEKTU klasy Koniec

	koni->Srodek::przedstawSie(); //ZAD wywo�anie metody przedstaw si� z zasi�gu Srodek dla OBIEKTU klasy Koniec

	srod->Poczatek::przedstawSie(); //ZAD wywo�anie metody przedstaw si� z zasi�gu Poczatek dla OBIEKTU klasy Srodek

	//analogicznie do przyk�ad�w wy�ej

	pocz->wypiszX();

	pocz->wypiszTekst();

	srod->wypiszX();

	srod->wypiszTekst();

	koni->wypiszX();

	pocz->ustawX();

	pocz->ustawTekst("TRATATATATA");

	srod->ustawX();

	srod->ustawTekst("KUKU");

	koni->ustawX();

	pocz->wypiszX();

	pocz->wypiszTekst();

	srod->wypiszX();

	srod->wypiszTekst();

	koni->wypiszX();

	//ZAD Wywo�aj metody ustawX i wypiszX dla pozosta�ych zasi�g�w (dla klasy Koniec z Srodek i Pocz�tek, dla klasy Srodek z Poczatek)

	delete pocz; //zniszczenie obiektu. Wywo�a konstruktor

	delete srod; //zniszczenie obiektu. Wywo�a konstruktor

	delete koni; //zniszczenie obiektu. Wywo�a konstruktor

	//Wszystkie przyk�ady powy�ej b�d� te� dzia�a�y dla obiekt�w nie tworzonych dynamicznie:

	Srodek srodeczek(3232.222);

	srodeczek.przedstawSie();

	srodeczek.wypiszX();

	srodeczek.ustawX();

	srodeczek.wypiszX();

	return 0;

}
