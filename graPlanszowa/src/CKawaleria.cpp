/*
 * CKawaleria.cpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */
#include "CKawaleria.hpp"
#include <iostream>

CKawaleria::CKawaleria(char symbol, int zycie, int atak, int szybkosc)
: CJednostka (0, 0, symbol, zycie, atak, szybkosc)
{
}

bool CKawaleria::atak(int x, int y)
{
	std::cout << "Patataj!" << std::endl;

	return true;
}

