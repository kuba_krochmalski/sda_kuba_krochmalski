/*
 * CKawaleria.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CKAWALERIA_HPP_
#define CKAWALERIA_HPP_

#include "CJednostka.hpp"

class CKawaleria : public CJednostka
{
public:
	CKawaleria(char symbol = 'K', int zycie = 120,
				int atak = 30, int szybkosc = 5);

	bool atak(int x, int y);
};


#endif /* CKAWALERIA_HPP_ */
