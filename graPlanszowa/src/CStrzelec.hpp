/*
 * CStrzelec.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CSTRZELEC_HPP_
#define CSTRZELEC_HPP_

#include "CJednostka.hpp"

class CStrzelec : public CJednostka
{
public:
	CStrzelec(char symbol = 'S', int zycie = 100,
				int atak = 10, int szybkosc = 3);

	bool atak(int x, int y);
};


#endif /* CSTRZELEC_HPP_ */
