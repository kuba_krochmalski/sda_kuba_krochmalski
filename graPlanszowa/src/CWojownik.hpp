/*
 * CWojownik.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CWOJOWNIK_HPP_
#define CWOJOWNIK_HPP_

#include "CJednostka.hpp"

class CWojownik : public CJednostka
{
public:
	CWojownik(char symbol = 'W', int zycie = 180,
				int atak = 20, int szybkosc = 2);

	bool atak(int x, int y);
};


#endif /* CWOJOWNIK_HPP_ */
