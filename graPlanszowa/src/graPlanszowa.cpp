#include <iostream>
#include "CJednostka.hpp"
#include "CStrzelec.hpp"
#include "CWojownik.hpp"
#include "CKawaleria.hpp"

using namespace std;

int main()
{
	CStrzelec s1;
	CWojownik w1;
	CKawaleria k1;

	s1.atak(1, 1);
	s1.CJednostka::atak(1, 1);
	cout << s1.getZycie() << endl;

	w1.atak(1, 1);
	w1.CJednostka::atak(1, 1);
	cout << w1.getZycie() << endl;

	k1.atak(1, 1);
	k1.CJednostka::atak(1, 1);
	cout << k1.getZycie() << endl;

	return 0;
}
