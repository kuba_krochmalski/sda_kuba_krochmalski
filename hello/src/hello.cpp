//============================================================================
// Name        : hello.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

int main() {
	std::cout << "--------\nHello World!\n--------" << std::endl;
	std::cerr << "Hello world!";

	//warunek ? je�li-prawda : je�li-fa�sz ----> operator tr�jargumentowy

	int a;

	std::cin >> a;

	std::string komunikat = a > 3 ? "Yay!" : "Nay!";
	std::cout << komunikat << std::endl;

	int* x = NULL; //mozna tak robic, zeby pozakac, ze na nic na razie nie wskazuje
	int* y;

	x = new int;
	y = new int;

	*x = 42;
	*y = 13;
	y = x;

	delete x;
	delete y;
	x = NULL; //mozna tak robic, zeby wyzerowac wskaznik
	y = NULL;




	return 0;

}
