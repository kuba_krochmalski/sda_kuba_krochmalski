//============================================================================
// Name        : ideZadania.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <math.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

float srednia(float a, float b, float c);
float pierwiastek(float x);
int wartoscBezwzgledna(float n);
int najwieksza(int a, int b, int c);
void wielokrotnosci(int n, int m);
int sumaKwadratow(int n);
int NWD(int x, int y);

int main()
{
//	cout << "Napis zawierajacy rozne dziwne znaczki // \\ \\ $ # & %" << endl;
//	cout << "-------------------------" << endl;
//
//	cout << "Srednia" << endl;
//	cout << "Podaj 3 liczby." << endl;
//	float a, b, c;
//	cin >> a; cin >> b; cin >> c;
//	cout << srednia(a, b, c) << endl;
//	cout << "-------------------------" << endl;
//
//	cout << "Pierwiastek" << endl;
//	cout << "Podej liczbe." << endl;
//	float x;
//	cin >> x;
//	cout << pierwiastek(x) << endl;
//	cout << "-------------------------" << endl;
//
//	cout << "Wypisywanie liczby" << endl;
//	cout << "Podaj liczbe." << endl;
//	float z;
//	cin >> z;
//	cout << round(z*100)/100 << endl;
//	cout << round(z*1000)/1000 << endl;
//	cout << round(z*10000)/10000 << endl;
//
//	cout << scientific << z << endl;
//	cout << "-------------------------" << endl;
//
//	cout << "Wartosc bezwzgledna" << endl;
//	cout << "Podej liczbe calkowita." << endl;
//	int n;
//	cin >> n;
//	cout << wartoscBezwzgledna(n) << endl;
//	cout << "-------------------------" << endl;
//
//	cout << "Najwieksza z trzech" << endl;
//	cout << "Podej 3 liczby calkowite." << endl;
//	int liczba1, liczba2, liczba3;
//	cin >> liczba1; cin >> liczba2; cin >> liczba3;
//	cout << najwieksza(liczba1, liczba2, liczba3) << endl;
//	cout << "-------------------------" << endl;
//
//	cout << "Wielokrotnosci n mniejsze od m" << endl;
//	cout << "Podej 2 liczby calkowite." << endl;
//	int n, m;
//	cin >> n; cin >> m;
//	wielokrotnosci(n, m);
//	cout << "-------------------------" << endl;
//
//	cout << "Suma kwadratow" << endl;
//	cout << "Podej liczbe calkowita." << endl;
//	int n;
//	cin >> n;
//	cout << sumaKwadratow(n) << endl;
//	cout << "-------------------------" << endl;

	cout << "NWD" << endl;
	cout << "Podej 2 liczby calkowite." << endl;
	int x, y;
	cin >> x; cin >> y;
	cout << NWD(x, y) << endl;
	cout << "-------------------------" << endl;




	return 0;
}

float srednia(float a, float b, float c)
{
	return (a+b+c)/3;
}

float pierwiastek(float x)
{
	return sqrt(x);
}

int wartoscBezwzgledna(float n)
{
	return abs(n);
}

int najwieksza(int a, int b, int c)
{
	if(a>b)
	{
		if(a>c)
		{
			return a;
		}
		else
		{
			return c;
		}
	}
	else
	{
		if(b>c)
		{
			return b;
		}
		else
		{
			return c;
		}
	}
}

void wielokrotnosci(int n, int m)
{
	int w = n;
	while(w < m)
	{
		cout << w << endl;
		w = w + n;
	}
}

int sumaKwadratow(int n)
{
	int suma = 0;

	for(int i=0; i<=n; i++)
	{
		suma += i*i;
	}

	return suma;
}

int NWD(int x, int y)
{
	int NWD = 1;
	for(int i=x; i<=y; i++)
	{
		if((x%i==0)&&(y%i==0))
		{
			NWD = i;
		}
	}
	return NWD;
}
