#include <iostream>

using namespace std;

int main()
{
	int t = 0;
	cin >> t;

	long long* n = new long long[t];
	long long temp;

	for (int i = 0; i < t; ++i)
	{
		cin >> temp;
		n[i] = temp/5;
		cout << n[i] << endl;
	}

	delete [] n;
	return 0;
}
