#include <iostream>

using namespace std;

class Dupa
{
public:
	Dupa()
	{
		cout << "Dupa()" << endl;
	}
	~Dupa()
	{
		cout << "~Dupa()" << endl;
	}
};

class Majty: public Dupa
{
public:
	Majty()
	{
		cout << "Majty()" << endl;
	}
	~Majty()
	{
		cout << "~Majty()" << endl;
	}
};

class KlasaLicznik
{
private:
	int licznik;

public:
	KlasaLicznik()
	{
		cout << "Konstruktor domyslny" << endl;
		licznik = 1;
	}

	KlasaLicznik(const KlasaLicznik& other)
	{
		cout << "Konstruktor kopiujacy" << endl;
		licznik = other.licznik + 1;
	}

	~KlasaLicznik()
	{
		cout << "~Destrukcja! Licznik: " << licznik << endl;
	}
};

void funkcja(KlasaLicznik obiekt)
{
	cout << "Jestem funkcja, przyjmuje obiekt." << endl;

	KlasaLicznik nowyLicznik(obiekt);
}

class MamFloata
{
public:
	float* dane;

public:
	MamFloata(): dane(new float(0.0f))
	{
		cout << "Konstruktor rezerwujacy miejsce" << endl;
	}

	MamFloata(float liczba): dane(new float(liczba))
	{
		cout << "Konstruktor rezerwujacy miejsce" << endl;
	}

	MamFloata(const MamFloata& other): dane(new float(*other.dane))
	{
		cout << "Konstruktor kopiujacy" << endl;
	}

	MamFloata& operator=(const MamFloata& other)
	{
		cout << "Operator przypisania" << endl;
		*dane = *other.dane + 0.1;
		return *this;
	}

	~MamFloata()
	{
		cout << "Destruktor zwalniajacy miejsce" << endl;
		delete dane;
	}
};

class DynamLicznik
{
public:
	int* licznik;

public:
	DynamLicznik(): licznik(new int(1))
	{
		cout << "Konstruktor rezerwujacy miejsce" << endl;
	}

	DynamLicznik(const DynamLicznik& other): licznik(new int(*other.licznik + 1))
	{
		cout << "Konstruktor kopiujacy" << endl;
	}

	~DynamLicznik()
	{
		cout << "Destruktor" << endl;
		*licznik -= 1;
		cout << "Wartosc licznika: " << *licznik << endl;

		if (*licznik == 0)
		{
			delete licznik;
		}
	}
};

void funkcja2(MamFloata obiekt)
{
	cout << "Adres: " << obiekt.dane << endl;
	cout << "Wartosc: " << *obiekt.dane << endl;

	MamFloata nowyObiekt(obiekt);

	cout << "Adres: " << nowyObiekt.dane << endl;
	cout << "Wartosc: " << *nowyObiekt.dane << endl;
}

void funkcja3(DynamLicznik obiekt)
{
	cout << "Adres: " << obiekt.licznik << endl;
	cout << "Licznik: " << *obiekt.licznik << endl;

	DynamLicznik nowyObiekt(obiekt);

	cout << "Adres: " << nowyObiekt.licznik << endl;
	cout << "Licznik: " << *nowyObiekt.licznik << endl;
}


int main()
{
//	Dupa dupa1;
//	Dupa* dupa2 = new Dupa();
//	delete dupa2;
//	Majty majty1;

	{
	KlasaLicznik licznik1;

	funkcja(licznik1);

	KlasaLicznik licznik2 = licznik1;
	}

	cout << "------------------------------" << endl;

	{
	MamFloata float1(1.23);

	cout << "Adres: " << float1.dane << endl;
	cout << "Wartosc: " << *float1.dane << endl;

	funkcja2(float1);

	MamFloata float2 = float1;

	cout << "Adres: " << float2.dane << endl;
	cout << "Wartosc: " << *float2.dane << endl;

	MamFloata float3;

	float3 = float1;

	cout << "Adres: " << float3.dane << endl;
	cout << "Wartosc: " << *float3.dane << endl;
	}

	cout << "------------------------------" << endl;

	DynamLicznik dyn1;

	cout << "Adres: " << dyn1.licznik << endl;
	cout << "Licznik: " << *dyn1.licznik << endl;

	funkcja3(dyn1);

	DynamLicznik dyn2 = dyn1;

	cout << "Adres: " << dyn2.licznik << endl;
	cout << "Licznik: " << *dyn2.licznik << endl;

	return 0;
}
