#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

class Kostka
{
private:
	int mIloscScianek;
	int mWylosowanaWartosc;

public:
	Kostka(int x): mIloscScianek(x), mWylosowanaWartosc()
	{
		srand(time(NULL));
		this->losuj();
	}

	void losuj()
	{
		mWylosowanaWartosc = rand()%mIloscScianek+1;
	}

	int getWartosc()
	{
		return mWylosowanaWartosc;
	}
};

int main()
{
	cout << "TWORZENIE" << endl;
	Kostka kostka1(4);
	cout << "Wylosowalem: " << kostka1.getWartosc() << endl;
	Kostka kostka2(6);
	cout << "Wylosowalem: " << kostka2.getWartosc() << endl;
	Kostka kostka3(12);
	cout << "Wylosowalem: " << kostka3.getWartosc() << endl;
	Kostka kostka4(20);
	cout << "Wylosowalem: " << kostka4.getWartosc() << endl;

	cout << "---------------------" << endl;

	cout << "LOSOWANIE" << endl;
	kostka1.losuj();
	cout << "Wylosowalem: " << kostka1.getWartosc() << endl;
	kostka2.losuj();
	cout << "Wylosowalem: " << kostka2.getWartosc() << endl;
	kostka3.losuj();
	cout << "Wylosowalem: " << kostka3.getWartosc() << endl;
	kostka4.losuj();
	cout << "Wylosowalem: " << kostka4.getWartosc() << endl;

	return 0;
}
