#include <iostream>

const int MAKSYMALNY_ROZMIAR_KOLEJKI = 5;

class Kolejka
{
private:
	int dane[MAKSYMALNY_ROZMIAR_KOLEJKI];
	int koniec;

public:

	bool pelna()
	{
		return koniec >= MAKSYMALNY_ROZMIAR_KOLEJKI;
	}

	bool pusta()
	{
		return koniec <= 0;
	}

	void pokazKolejke()
	{
		std::cout << koniec << " [";

		for (int i = 0; i < koniec; ++i)
		{
			std::cout << dane[i] << ' ';
		}

		std::cout << "]" << std::endl;
	}

	int wstaw(int nowaWartosc)
	{
		// nowa osoba do kolejki

		if (pelna())
		{
			std::cout << "Kolejka jest pelna!" << std::endl;
			return -1;
		}
		else
		{
			dane[koniec] = nowaWartosc;
			koniec += 1;
			return nowaWartosc;
		}
	}

private:
	void przesunKolejkeWLewo()
	{
		for (int i = 1; i < koniec; ++i)
		{
			dane[i-1] = dane[i];
		}

	}

public:
	int usun()
	{
		// usu� osob� z kolejki
		if (!pusta())
		{
			int obsluzony = dane[0];

			przesunKolejkeWLewo();

			koniec -= 1;
			return obsluzony;
		}
		else
		{
			std::cout << "Kolejka jest pusta!" << std::endl;
			return -1;
		}
	}

	Kolejka(): dane{0,0,0,0,0}, koniec(0)
	{
		//myk myk myk
	}

};

int main()
{

	Kolejka naNarty;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(90) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(76) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(54) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Usuwam: " << naNarty.usun() << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(32) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Usuwam: " << naNarty.usun() << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Usuwam: " << naNarty.usun() << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Usuwam: " << naNarty.usun() << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Usuwam: " << naNarty.usun() << std::endl;

	std::cout << "Wstawiam: " << naNarty.wstaw(14) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(74) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(71) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(33) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(38) << std::endl;

	naNarty.pokazKolejke();

	std::cout << "Wstawiam: " << naNarty.wstaw(60) << std::endl;

	naNarty.pokazKolejke();

	return 0;
}
