//============================================================================
// Name        : kolejka.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

const int MAKSYMALNY_ROZMIAR_KOLEJKI = 5;

struct Kolejka
{
	int dane[MAKSYMALNY_ROZMIAR_KOLEJKI];
	int koniec;
};

void pokazKolejke(Kolejka kolejkaLiczb)
{
		if(kolejkaLiczb.koniec == 0)
		{
			std::cout << "Kolejka pusta!" << std::endl;
		}
		else
		{
			std::cout << kolejkaLiczb.koniec << " [";
			for(int i=0; i < MAKSYMALNY_ROZMIAR_KOLEJKI; i++)
			{
			std::cout << kolejkaLiczb.dane[i] << " ";
			}
			std::cout << "]";
		}
		std::cout << std::endl;
}

void wstaw(Kolejka& kolejkaLiczb, int nowaWartosc)
{
	//nowa osoba do kolejki
	kolejkaLiczb.dane[kolejkaLiczb.koniec] = nowaWartosc;
	kolejkaLiczb.koniec += 1;
}


int main()
{
	//stworzenie nowej pustej kolejki
	Kolejka naNarty = {
			{0, 0, 0, 0, 0},
			0
	};

	pokazKolejke(naNarty);

	wstaw(naNarty, 90);

	pokazKolejke(naNarty);

	wstaw(naNarty, 67);

	pokazKolejke(naNarty);

}
