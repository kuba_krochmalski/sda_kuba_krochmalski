#include <iostream>
#include <string>

class Ksiazka
{
private:
	std::string tytul;
	std::string autor;
	int iloscStron;
	double cena;

public:
	void wypiszSie()
	{
		std::cout << "Tytu�: " << tytul << std::endl;
		std::cout << "Autor: " << autor << std::endl;
		std::cout << "Ilo�� stron: " << iloscStron << std::endl;
		std::cout << "Cena: " << cena << std::endl;
	}

	Ksiazka(std::string tytul2, std::string autor2,
			int iloscStron2, double cena2)
	{
		tytul = tytul2;
		autor = autor2;
		iloscStron = iloscStron2;
		cena = cena2;
	}

	~Ksiazka()
	{
		std::cout << "DESTRUKCJA ksiazki: " << tytul << std::endl;
	}
};

int main()
{
	const int ILOSC_KSIAZEK = 5;

	Ksiazka ksiazka0("Hobbit, czyli tam i z powrotem",
			"J.R.R. Tolkien", 300, 25.0);
	Ksiazka ksiazka1("Dru�yna pier�cienia", "J.R.R. Tolkien", 500,
			40.0);
	Ksiazka ksiazka2("Kroniki Amberu", "Roger Zelazny", 550, 40.0);
	Ksiazka ksiazka3("Diuna", "F. Herbert", 600, 50.5);
	Ksiazka ksiazka4("Kroniki Jakuba W�drowycza", "Andrzej Pilipiuk",
			350, 36.25);

	ksiazka0.wypiszSie();
	ksiazka1.wypiszSie();
	ksiazka2.wypiszSie();
	ksiazka3.wypiszSie();
	ksiazka4.wypiszSie();

	//for (int i = 0; i < ILOSC_KSIAZEK; ++i)
	//{
	//	ksiazkai.wypiszSie();
	//}

	return 0;
}
