/*
 * Lista.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Lista.hpp"
#include <iostream>

Lista::Lista()
: mPierwszy(0)
, mOstatni(0)
, mRozmiar(0)
{
	// TODO Auto-generated constructor stub

}

Lista::~Lista()
{
	// TODO Auto-generated destructor stub
}

void Lista::dodajPoczatek(int x)
{
	if(mPierwszy != 0)
	{
		Wezel* tmp = new Wezel(x);
		tmp->mNastepny = mPierwszy;
		mPierwszy = tmp;
		++mRozmiar;
	}
	else //jak jest pusta
	{
		mPierwszy = new Wezel(x);
		mOstatni = mPierwszy;
		++mRozmiar;
	}
}

void Lista::dodajKoniec(int x)
{
	if (mPierwszy != 0)
	{
		Wezel* tmp = new Wezel(x);
		mOstatni->mNastepny = tmp;
		mOstatni = tmp;
		++mRozmiar;
	}
	else //jak jest pusta
	{
		mPierwszy = new Wezel(x);
		mOstatni = mPierwszy;
		++mRozmiar;
	}
}

int Lista::pobierz(int indeks)
{

	Wezel* tmp = mPierwszy;

	if (mRozmiar == 0)
	{
		std::cout << "Lista jest pusta! Return = ";
		return -1;
	}
	else if(indeks > mRozmiar)
	{
		std::cout << "Lista nie ma tylu elementow! Return = ";
		return -1;
	}
	else
	{
		for (int i = 1; i < indeks; ++i)
		{
			tmp = tmp->mNastepny;
		}

		return tmp->mWartosc;
	}
}


void Lista::wypisz()
{
	Wezel* tmp = mPierwszy;

	std::cout << "{ ";

	if(mRozmiar == 0)
	{
		std::cout << "Lista jest pusta!";
	}

	while(tmp != 0)
	{
		std::cout << "[" << tmp->mWartosc << "] ";
		tmp = tmp->mNastepny;
	}
	std::cout <<" }" << std::endl;
}

bool Lista::czyPusta()
{
	if(mRozmiar == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Lista::wyczysc()
{
	Wezel* tmp = mPierwszy;
	Wezel* niszcz = mPierwszy;

	while(tmp != 0)
	{
		niszcz = tmp;
		tmp = tmp->mNastepny;
		delete niszcz;
	}

	mPierwszy = 0;
	mOstatni = 0;
	mRozmiar = 0;
}

void Lista::usun(int indeks)
{
	Wezel* tmp = mPierwszy;
	Wezel* poprz  = mPierwszy;
	int licznik = 0;

	if (mRozmiar == 0)
	{
		std::cout << "Lista jest pusta!";
	}
	else if (indeks > mRozmiar)
	{
		std::cout << "Lista nie ma tylu elementow!";
	}
	else
	{
		while(tmp != 0)
		{
			++licznik;
			poprz = tmp;

			if(licznik == indeks)
			{
				poprz->mNastepny = tmp->mNastepny;
				delete tmp;
			}
			else
			{
				poprz = tmp;
				tmp = tmp->mNastepny;
			}
		}






	}
}





