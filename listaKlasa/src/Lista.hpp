/*
 * Lista.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include <iostream>

using namespace std;

class Lista
{
private:
	class Wezel
	{
	public:
		int mWartosc;
		Wezel* mNastepny;

	public:
		const Wezel* getNastepny()
		{
			return mNastepny;
		}

		void setNastepny(Wezel* nastepny)
		{
			mNastepny = nastepny;
		}

		int getX()
		{
			return mWartosc;
		}

		void setX(int x)
		{
			mWartosc = x;
		}

		Wezel(int x)
		: mWartosc(x)
		, mNastepny(0)
		{

		}
	};

private:
	Wezel* mPierwszy;
	Wezel* mOstatni;
	int mRozmiar;

public:
	Lista();
	virtual ~Lista();
	void dodajPoczatek(int x);
	void dodajKoniec(int x);
	int pobierz(int indeks);
	void wyczysc();
	void usun(int indeks);
	void znajdz();
	void pierwszyElement();
	void ostatniElement();
	bool czyPusta();
	void sortowanie();
	void wypisz();


};

#endif /* LISTA_HPP_ */
