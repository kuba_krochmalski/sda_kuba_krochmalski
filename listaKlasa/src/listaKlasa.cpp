//============================================================================
// Name        : listaKlasa.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Lista.hpp"

using namespace std;

int main()
{
	Lista lista;

	lista.dodajPoczatek(4);
	lista.wypisz();
	lista.dodajPoczatek(7);
	lista.wypisz();
	lista.dodajPoczatek(13);
	lista.wypisz();
	lista.dodajKoniec(17);
	lista.wypisz();
	lista.dodajPoczatek(11);
	lista.wypisz();
	lista.dodajKoniec(0);
	lista.wypisz();
	lista.dodajKoniec(3);
	lista.wypisz();

	cout << lista.pobierz(1) << endl;
	cout << lista.pobierz(2) << endl;
	cout << lista.pobierz(3) << endl;
	cout << lista.pobierz(8) << endl;

	lista.wyczysc();

	lista.wypisz();
	lista.dodajPoczatek(9);
	lista.dodajPoczatek(99);
	lista.dodajKoniec(41);
	lista.dodajKoniec(45);
	lista.wypisz();




	return 0;
}
