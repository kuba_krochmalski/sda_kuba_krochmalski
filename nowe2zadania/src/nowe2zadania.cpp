#include <iostream>

int main()
{
//	std::cout << "Podaj ilosc liczb: " << std::flush;
//	int iloscLiczb;
//	std::cin >> iloscLiczb;
//	std::cout << "-------------------------" << std::endl;
//
//	int* liczby = new int[iloscLiczb];
//	int iloczyn = 1;
//
//	for (int i = 0; i < iloscLiczb; ++i)
//	{
//		do
//		{
//		std::cout << "Podaj liczbe nr " << i+1 << ": " << std::flush;
//		std::cin >> liczby[i];
//		} while (liczby[i] == 0);
//
//		iloczyn *= liczby[i];
//	}
//
//	std::cout << "-------------------------" << std::endl;
//	std::cout << "Iloczyn liczb wynosi: " << iloczyn << std::endl;
//
//	delete[] liczby;
//	return 0;

	std::cout << "Podaj PESEL: " << std::flush;
	long long PESEL;
	std::cin >> PESEL;
	int cyfry[11];
	int wagi[11] = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};
	int pomnozone[11];
	long long tymczasowy = PESEL;
	int indeks = 10;

	while (tymczasowy > 0)
	{
		cyfry[indeks] = tymczasowy%10;
		pomnozone[indeks] = cyfry[indeks]*wagi[indeks];
		tymczasowy /= 10;
		--indeks;
	}

	int suma = 0;

	for (int i=0; i<10; ++i)
	{
		suma += pomnozone[i];
	}

	int wynik = 10 - (suma%10);

	std::cout << wynik << std::endl;

	if(wynik == cyfry[10])
	{
		std::cout << "POPRAWNY PESEL" << std::endl;
	}
	else
	{
		std::cout << "NIEPOPRAWNY PESEL" << std::endl;
	}

	return 0;
}
