#include <iostream>
#include <string>

const std::string RPN[] =

{ "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -",

"1 1 + 2 + 3 1 1 + 7 - 9 / * -" };

const int wyniki[] =

{ 1, 4, 14, 4 };

void push(int stos[], int& wierzcholek, int nowa)
{
	stos[wierzcholek] = nowa;
	++wierzcholek;
}

void pop(int stos[], int& wierzcholek)
{
	stos[wierzcholek-1] = 0;
	--wierzcholek;
}

void operacja(int stos[], int& wierzcholek, int wynik)
{
	pop(stos, wierzcholek);
	pop(stos, wierzcholek);
	push(stos, wierzcholek, wynik);
}

int wylicz(const std::string wyrazenie)
{
	int stos[20] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	int wierzcholek = 0;
	int wynik = 0;

	for (unsigned int i=0; i<wyrazenie.length(); ++i)
	{
		char znak = wyrazenie[i];
		if (znak == ' ')
		{
			continue;
		}
		else if (znak == '+')
		{
			wynik = stos[wierzcholek-1] + stos[wierzcholek-2];
			operacja(stos, wierzcholek, wynik);
		}
		else if (znak == '-')
		{
			wynik = stos[wierzcholek-2] - stos[wierzcholek-1];
			operacja(stos, wierzcholek, wynik);
		}
		else if (znak == '*')
		{
			wynik = stos[wierzcholek - 2] * stos[wierzcholek - 1];
			operacja(stos, wierzcholek, wynik);
		}
		else if (znak == '/')
		{
			wynik = stos[wierzcholek - 2] / stos[wierzcholek - 1];
			operacja(stos, wierzcholek, wynik);
		}
		else
		{
			push(stos, wierzcholek, znak - '0');
		}
	}

	return stos[wierzcholek-1];
}

void sprawdz(const std::string rpn, int oczekiwanyWynik)

{

	int wynik = wylicz(rpn);

	if (wynik == oczekiwanyWynik)

	{

		std::cout << "Cacy ";

	}

	else

	{

		std::cout << "Be   ";

	}

	std::cout << rpn << " => " << wynik << std::endl;

}

int main(int argc, char* argv[])

{

	for (int i = 0; i < 4; ++i)

	{

		sprawdz(RPN[i], wyniki[i]);

	}

	return 0;

}
