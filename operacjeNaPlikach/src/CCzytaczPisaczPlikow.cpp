/*
 * CCzytaczPisaczPlikow.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "CCzytaczPisaczPlikow.hpp"
#include <iostream>

CCzytaczPisaczPlikow::CCzytaczPisaczPlikow(std::string sciezka)
{
	mSciezka = sciezka;
	mStanPliku = zamkniety;
}

CCzytaczPisaczPlikow::~CCzytaczPisaczPlikow()
{
	// TODO Auto-generated destructor stub
}

void CCzytaczPisaczPlikow::otworzPlik()
{
	if (mStanPliku == zamkniety)
	{
		mPlik.open(mSciezka.c_str(), std::ios::in | std::ios::out);

		if (mPlik.good() == true)
		{
			std::cout << "Uzyskano dostep do pliku." << std::endl;
			mStanPliku = otwarty;
		}
		else
		{
			std::cout << "Nie uzyskano dostepu do pliku." << std::endl;
		}
	}
	else if (mStanPliku == otwarty)
	{
		std::cout << "Plik jest juz otwarty." << std::endl;
	}
	else
	{
		std::cout << "Wystapil blad!" << std::endl;
	}
}
void CCzytaczPisaczPlikow::zamknijPlik()
{
	if (mStanPliku == otwarty)
	{
		std::cout << "Zamykanie pliku." << std::endl;
		mPlik.close();
		mStanPliku = zamkniety;
	}
	else if (mStanPliku == zamkniety)
	{
		std::cout << "Plik jest juz zamkniety." << std::endl;
	}
	else
	{
		std::cout << "Wystapil blad!" << std::endl;
	}
}
void CCzytaczPisaczPlikow::czytajPlik()
{
	if (mStanPliku == otwarty)
	{
		std::string linia;

		while (!mPlik.eof())
		{
			getline(mPlik, linia);
			std::cout << linia << std::endl;
		}
	}
	else
	{
		std::cout << "Nie mozna odczytac zawartosci pliku!" << std::endl;
	}
}
void CCzytaczPisaczPlikow::napiszPlik()
{
	if (mStanPliku == otwarty)
	{
		std::string linia;

		do
		{
			getline(std::cin, linia);
			mPlik << linia << std::endl;

		} while (linia != "");

	}
	else
	{
		std::cout << "Nie mozna nadpisac pliku!" << std::endl;
	}
}

