/*
 * CCzytaczPisaczPlikow.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef CCZYTACZPISACZPLIKOW_HPP_
#define CCZYTACZPISACZPLIKOW_HPP_
#include "CCzytaczPisaczPlikow.hpp"
#include "CPisaczPlikow.hpp"
#include "CCzytaczPlikow.hpp"

class CCzytaczPisaczPlikow : public CCzytaczPlikow, public CPisaczPlikow
{
public:
	CCzytaczPisaczPlikow(std::string sciezka);
	virtual ~CCzytaczPisaczPlikow();
	void otworzPlik();
	void zamknijPlik();
	void czytajPlik();
	void napiszPlik();
};

#endif /* CCZYTACZPISACZPLIKOW_HPP_ */
