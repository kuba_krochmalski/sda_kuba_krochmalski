/*
 * CCzytaczPlikow.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "CCzytaczPlikow.hpp"
#include <iostream>
#include <fstream>

CCzytaczPlikow::CCzytaczPlikow()
{

}

CCzytaczPlikow::CCzytaczPlikow(std::string sciezka)
: COperacjaNaPliku(sciezka)
{
	// TODO Auto-generated constructor stub

}

CCzytaczPlikow::~CCzytaczPlikow()
{
	// TODO Auto-generated destructor stub
}

void CCzytaczPlikow::otworzPlik()
{
	if(mStanPliku == zamkniety)
	{
		mPlik.open(mSciezka.c_str(), std::ios::in);

		if (mPlik.good() == true)
		{
			std::cout << "Uzyskano dostep do pliku." << std::endl;
			mStanPliku = otwarty;
		}
		else
		{
			std::cout << "Nie uzyskano dostepu do pliku." << std::endl;
		}
	}
	else if(mStanPliku == otwarty)
	{
		std::cout << "Plik jest juz otwarty." << std::endl;
	}
	else
	{
		std::cout << "Wystapil blad!" << std::endl;
	}

}

void CCzytaczPlikow::zamknijPlik()
{
	if(mStanPliku == otwarty)
	{
		std::cout << "Zamykanie pliku." << std::endl;
		mPlik.close();
		mStanPliku = zamkniety;
	}
	else if(mStanPliku == zamkniety)
	{
		std::cout << "Plik jest juz zamkniety." << std::endl;
	}
	else
	{
		std::cout << "Wystapil blad!" << std::endl;
	}
}

void CCzytaczPlikow::czytajPlik()
{
	if(mStanPliku == otwarty)
	{
		std::string linia;

		while (!mPlik.eof())
		{
			getline(mPlik, linia);
			std::cout << linia << std::endl;
		}
	}
	else
	{
		std::cout << "Nie mozna odczytac zawartosci pliku!" << std::endl;
	}
}
