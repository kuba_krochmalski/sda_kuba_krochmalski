/*
 * CCzytaczPlikow.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef CCZYTACZPLIKOW_HPP_
#define CCZYTACZPLIKOW_HPP_
#include "COperacjaNaPliku.hpp"

class CCzytaczPlikow : virtual public COperacjaNaPliku
{
public:
	CCzytaczPlikow();
	CCzytaczPlikow(std::string sciezka);
	virtual ~CCzytaczPlikow();
	void virtual otworzPlik();
	void virtual zamknijPlik();
	void virtual czytajPlik();
};

#endif /* CCZYTACZPLIKOW_HPP_ */
