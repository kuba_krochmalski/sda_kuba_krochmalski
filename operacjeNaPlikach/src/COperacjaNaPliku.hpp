/*
 * COperacjaNaPliku.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef COPERACJANAPLIKU_HPP_
#define COPERACJANAPLIKU_HPP_
#include <string>
#include <fstream>

class COperacjaNaPliku
{
public:
	enum Stan
	{
		otwarty,
		zamkniety,
		blad
	};

protected:
	std::string mSciezka;
	Stan mStanPliku;
	std::fstream mPlik;

public:
	COperacjaNaPliku();
	COperacjaNaPliku(std::string sciazka);
	virtual ~COperacjaNaPliku();
	virtual void otworzPlik() = 0;
	virtual void zamknijPlik() = 0;
};

#endif /* COPERACJANAPLIKU_HPP_ */
