/*
 * CPisaczPlikow.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "CPisaczPlikow.hpp"
#include <iostream>

CPisaczPlikow::CPisaczPlikow()
{

}
CPisaczPlikow::CPisaczPlikow(std::string sciezka)
: COperacjaNaPliku(sciezka)
{
	// TODO Auto-generated constructor stub

}

CPisaczPlikow::~CPisaczPlikow()
{
	// TODO Auto-generated destructor stub
}

void CPisaczPlikow::otworzPlik()
{
	if (mStanPliku == zamkniety)
	{
		mPlik.open(mSciezka.c_str(), std::ios::out);

		if (mPlik.good() == true)
		{
			std::cout << "Uzyskano dostep do pliku." << std::endl;
			mStanPliku = otwarty;
		}
		else
		{
			std::cout << "Nie uzyskano dostepu do pliku." << std::endl;
		}
	}
	else if (mStanPliku == otwarty)
	{
		std::cout << "Plik jest juz otwarty." << std::endl;
	}
	else
	{
		std::cout << "Wystapil blad!" << std::endl;
	}
}
void CPisaczPlikow::zamknijPlik()
{
	if (mStanPliku == otwarty)
	{
		std::cout << "Zamykanie pliku." << std::endl;
		mPlik.close();
		mStanPliku = zamkniety;
	}
	else if (mStanPliku == zamkniety)
	{
		std::cout << "Plik jest juz zamkniety." << std::endl;
	}
	else
	{
		std::cout << "Wystapil blad!" << std::endl;
	}
}
void CPisaczPlikow::napiszPlik()
{
	if(mStanPliku == otwarty)
	{
		std::string linia;

		do
		{
			getline(std::cin, linia);
			mPlik << linia << std::endl;

		}while(linia != "");

	}
	else
	{
		std::cout << "Nie mozna nadpisac pliku!" << std::endl;
	}
}

