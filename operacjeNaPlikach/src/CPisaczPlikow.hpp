/*
 * CPisaczPlikow.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef CPISACZPLIKOW_HPP_
#define CPISACZPLIKOW_HPP_
#include "COperacjaNaPliku.hpp"

class CPisaczPlikow : virtual public COperacjaNaPliku
{
public:
	CPisaczPlikow();
	CPisaczPlikow(std::string sciezka);
	virtual ~CPisaczPlikow();
	void virtual otworzPlik();
	void virtual zamknijPlik();
	void virtual napiszPlik();
};

#endif /* CPISACZPLIKOW_HPP_ */
