#include <iostream>
using namespace std;
#include "COperacjaNaPliku.hpp"
#include "CCzytaczPlikow.hpp"
#include "CPisaczPlikow.hpp"
#include "CCzytaczPisaczPlikow.hpp"

int main()
{
	CCzytaczPlikow p("src/tekst.txt");
	p.otworzPlik();
	std::cout << "------------------------" << std::endl;
	p.czytajPlik();
	std::cout << "------------------------" << std::endl;
	p.zamknijPlik();
	p.czytajPlik();
	std::cout << "------------------------" << std::endl;

//	CPisaczPlikow p2("src/tekst2.txt");
//	p2.otworzPlik();
//	p2.napiszPlik();
//	p2.zamknijPlik();
//	std::cout << "------------------------" << std::endl;
//
//	CCzytaczPlikow p3("src/tekst2.txt");
//	p3.otworzPlik();
//	std::cout << "------------------------" << std::endl;
//	p3.czytajPlik();
//	std::cout << "------------------------" << std::endl;
//	p3.zamknijPlik();
//	std::cout << "------------------------" << std::endl;

	CCzytaczPisaczPlikow p4("src/tekst3.txt");
	p4.otworzPlik();
	p4.napiszPlik();
	p4.zamknijPlik();
	std::cout << "------------------------" << std::endl;
	p4.otworzPlik();
	p4.czytajPlik();
	p4.zamknijPlik();
	std::cout << "------------------------" << std::endl;



	return 0;
}
