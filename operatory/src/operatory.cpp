#include <iostream>
#include <cstdlib>
#include <cerrno>

using namespace std;

long potegowanie (long x, long y);
void wyswietlMenu();

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		clog << "Za malo argumentow!" << endl;
		return 1;
	}

	if (argc > 3)
	{
		clog << "Za duzo argumentow!" << endl;
		return 1;
	}

	char* arg1end;
	char* arg2end;

	long x = strtol(argv[1], &arg1end, 10);
	if (arg1end[0] != '\0')
	{
		clog << "Argument nr 1 nie jest liczba!" << endl;
		return 1;
	}
	else if (errno == ERANGE)
	{
		clog << "Argument nr 1 nie zmiescil sie w zmiennej!" << endl;
		return 1;
	}

	long y = strtol(argv[2], &arg2end, 10);
	if (arg2end[0] != '\0')
	{
		clog << "Argument nr 2 nie jest liczba!" << endl;
		return 1;
	}
	else if (errno == ERANGE)
	{
		clog << "Argument nr 2 nie zmiescil sie w zmiennej!" << endl;
		return 1;
	}

	cout << "Argumenty: x = " << x << "; y = " << y << endl;

	cout << "-----------------------------" << endl;
	wyswietlMenu();
	cout << "-----------------------------" << endl;

	char wybor;

	while (true)
	{
	cin >> wybor;

		switch (wybor)
		{
		case '+':
			cout << "A + B = " << x + y << endl;
			break;
		case '-':
			cout << "A - B = " << x - y << endl;
			break;
		case '*':
			cout << "A * B = " << x * y << endl;
			break;
		case '/':
			if (y==0)
			{
				clog << "Nie bede dzielic przez zero!" << endl;
				break;
			}
			cout << "A / B = " << x / y << endl;
			break;
		case '%':
			cout << "A % B = " << x % y << endl;
			break;
		case '&':
			cout << "A & B = " << (x & y) << endl;
			break;
		case '|':
			cout << "A | B = " << (x | y) << endl;
			break;
		case '^':
			cout << "A ^ B = " << (x ^ y) << endl;
			break;
		case '<':
			cout << "A << B = " << (x >> y) << endl;
			break;
		case '>':
			cout << "A >> B = " << (x << y) << endl;
			break;
		case 'P':
			cout << "A do potegi B = " << potegowanie(x, y) << endl;
			break;
		case 'Q':
			cout << "Zamykanie..." << endl;
			return 0;
			break;
		default:
			cout << "Nieprawidlowa komenda!" << endl;
		}
	}

	return 0;

}

void wyswietlMenu()
{
	cout << "     ---MENU---     " << endl;
	cout << "1. Dodawanie (+)" << endl;
	cout << "2. Odejmowanie (-)" << endl;
	cout << "3. Mnozenie (*)" << endl;
	cout << "4. Dzielenie (/)" << endl;
	cout << "5. Modulo (%)" << endl;
	cout << "6. Bitowy And (&)" << endl;
	cout << "7. Bitowy Or (|)" << endl;
	cout << "8. Bitowy Xor (^)" << endl;
	cout << "9. Bitowe << (<)" << endl;
	cout << "10. Bitowe >> (>)" << endl;
	cout << "11. Potegowanie (P)" << endl;
	cout << "12. Wyjscie (Q)" << endl;
}

long potegowanie (long x, long y)
{
	if (y==0)
	{
		return 1;
	}

	else if (y==1)
	{
		return x;
	}

	else
	{
		int z = x;

		for (int i=2; i<=y; i++)
		{
			x *= z;
		}

		return x;
	}
}
