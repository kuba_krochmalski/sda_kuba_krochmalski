#include <iostream>

#include "incl/kuku.hpp"

using namespace std;

int main(int argc, char* argv[])
{
	for (int i = 0; i < argc; i++)
	{
		cout << i << " argument to: " << argv[i] << endl;
	}

	kuku();

	return 0;
}
