/*
 * Actor.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include "Actor.hpp"
#include "CGame.hpp"

Actor::Actor()
: mX(0)
, mY(0)
, mSymbol('$')
, mFgColour(Game::FgColour::DARKMAGENTA)
, mBgColour(Game::BgColour::DARKGRAY)
, gamePtr(0)
{
	// TODO Auto-generated constructor stub
}

Actor::Actor(int x, int y)
: mX(x)
, mY(y)
, mSymbol('$')
, mFgColour(Game::FgColour::DARKMAGENTA)
, mBgColour(Game::BgColour::DARKGRAY)
, gamePtr(0)
{
}

Actor::Actor(int x, int y, char symbol, Game::FgColour::Colour fgc, Game::BgColour::Colour bgc, CGame* gPtr)
: mX(x)
, mY(y)
, mSymbol(symbol)
, mFgColour(fgc)
, mBgColour(bgc)
, gamePtr(gPtr)
{
}

Actor::~Actor()
{
	// TODO Auto-generated destructor stub
}

void Actor::render(Game::Console& console)
{
	console.drawChar(mSymbol, mX, mY, mFgColour, mBgColour);
}

int Actor::getX() const {
	return mX;
}

int Actor::getY() const {
	return mY;
}

void Actor::move(int x, int y)
{
	if(gamePtr->canMove(mX+x, mY+y))
	{
		mX += x;
		mY += y;
	}
}

bool Actor::checkColision(Actor& enemy)
{
	if((this->getX() - enemy.getX() < 2) && (this->getX() - enemy.getX() > -2) &&
		(this->getY() - enemy.getY() < 2) && (this->getY() - enemy.getY() < 2))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Actor::setFgColour(Game::FgColour::Colour fgColour)
{
	mFgColour = fgColour;
}
