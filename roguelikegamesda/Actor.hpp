/*
 * Actor.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef ACTOR_HPP_
#define ACTOR_HPP_
#include "Console.hpp"

class CGame;

class Actor
{
private:
	int mX;
	int mY;
	char mSymbol;
	Game::FgColour::Colour mFgColour;
	Game::BgColour::Colour mBgColour;
	CGame* gamePtr;

public:
	Actor();
	Actor(int x, int y);
	Actor(int x, int y, char symbol, Game::FgColour::Colour fgc, Game::BgColour::Colour bgc, CGame* gPtr);
	virtual ~Actor();
	void render(Game::Console& console);
	void move(int x, int y);
	int getX() const;
	int getY() const;
	bool checkColision(Actor& enemy);
	void setFgColour(Game::FgColour::Colour fgColour);
};

#endif /* ACTOR_HPP_ */
