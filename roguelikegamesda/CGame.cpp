/*
 * CGame.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#include "CGame.hpp"
#include <iostream>

CGame::CGame()
{
	// TODO Auto-generated constructor stub

}

CGame::~CGame()
{
	// TODO Auto-generated destructor stub
}

void CGame::init()
{
	mConsole = Game::Console();
	short int w = 100;
	short int h = 40;
	mConsole.setConsoleSize(w, h);

	short int mapW = 20;
	short int mapH = 10;
	mMap = Map(mapW, mapH);
	mMap.init();

	mEnemies = vector<Enemy>();
	mEnemies.push_back(Enemy(3, 3, 'E', Game::FgColour::RED, Game::BgColour::BLACK, this));
	mEnemies.push_back(Enemy(10, 5, 'E', Game::FgColour::RED, Game::BgColour::BLACK, this));
	mEnemies.push_back(Enemy(12, 8, 'E', Game::FgColour::RED, Game::BgColour::BLACK, this));

	mPlayer = Player(1, 1, 'P', Game::FgColour::YELLOW, Game::BgColour::BLACK, 100, this);

	for(int i = 0; i < mMap.getWidth(); ++i)
	{
		for(int k = 0; k < mMap.getHeight(); ++k)
		{
			if((i == 0) || (i == mMap.getWidth() - 1) || (k == 0) || (k == mMap.getHeight() - 1))
			{
				mMap.setTile(i, k, 1);
			}
		}
	}
}

void CGame::draw()
{
	mConsole.clear();
	mMap.render(mConsole);

	if(mPlayer.getHp() > 0)
	{
		cout << endl << endl << endl << "Punkty zycia: " << mPlayer.getHp() << "/100" << endl;
	}
	else
	{
		cout << endl << endl << endl << "GAME OVER!" << endl;
	}

	mPlayer.render(mConsole);

	for(unsigned int i = 0; i < mEnemies.size(); ++i)
	{
		mEnemies[i].render(mConsole);
	}
}

void CGame::attackEnemy()
{
	for(vector<Enemy>::iterator it = mEnemies.begin(); it != mEnemies.end(); ++it)
	{
		if(mPlayer.checkColision(*it))
		{
				mEnemies.erase(it);
				advance(it, -1);
		}
	}
}

void CGame::attackPlayer()
{
	for(vector<Enemy>::iterator it = mEnemies.begin(); it != mEnemies.end(); ++it)
	{
		if(mPlayer.checkColision(*it))
		{
			mPlayer.changeHP(35);

			if(mPlayer.getHp() <= 0)
			{
				mPlayer.setFgColour(Game::FgColour::BLACK);
			}
		}
	}
}

void CGame::updatePlayer(Game::Key key)
{
	int dx = 0;
	int dy = 0;

	switch (key)
	{
	case Game::Key::MOVE_UP:
		dy = -1;
		break;
	case Game::Key::MOVE_DOWN:
		dy = 1;
		break;
	case Game::Key::MOVE_LEFT:
		dx = -1;
		break;
	case Game::Key::MOVE_RIGHT:
		dx = 1;
		break;
	case Game::Key::QUIT:
	case Game::Key::INVALID:
	default:
		//Nothing
		break;
	}

	mPlayer.move(dx, dy);
	attackEnemy();
}

void CGame::updateEnemies(Game::Key key)
{
	int dx = 0;
	int dy = 0;

	for(unsigned int i = 0; i < mEnemies.size(); ++i)
	{
		switch(mEnemies[i].getRandMove())
		{
		case 1:
			dy = -1;
			break;
		case 2:
			dy = 1;
			break;
		case 3:
			dx = -1;
			break;
		case 4:
			dx = 1;
			break;
		}

	mEnemies[i].move(dx, dy);
	attackPlayer();
	}
}

void CGame::gameLoop(Game::Key key)
{
	while(key != Game::Key::QUIT)
	{
		draw();
		key = mConsole.getKey();
		updatePlayer(key);
		draw();
		key = mConsole.getKey();
		updateEnemies(key);
	}
}

bool CGame::canMove(int x, int y)
{
	if(mMap.isWall(x, y) == true)
	{
		return false;
	}

	else if((x == mPlayer.getX()) && (y == mPlayer.getY()))
	{
		return false;
	}

	else
	{
		for(vector<Enemy>::iterator it = mEnemies.begin(); it != mEnemies.end(); ++it)
		{
			if((x == it->getX()) && (y == it->getY()))
			{
				return false;
			}
		}
		return true;
	}
}

