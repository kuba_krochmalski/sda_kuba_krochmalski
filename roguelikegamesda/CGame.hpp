/*
 * CGame.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef CGAME_HPP_
#define CGAME_HPP_
#include "Map.hpp"
#include "Enemy.hpp"
#include "Player.hpp"
#include "Console.hpp"
#include <vector>

using namespace std;

class CGame
{
private:
	Map mMap;
	vector<Enemy> mEnemies;
	Player mPlayer;
	Game::Console mConsole;

public:
	CGame();
	virtual ~CGame();

	void init();
	void gameLoop(Game::Key key);
	bool canMove(int x, int y);
	void draw();
	void updatePlayer(Game::Key key);
	void updateEnemies(Game::Key key);
	void attackEnemy();
	void attackPlayer();
};

#endif /* CGAME_HPP_ */
