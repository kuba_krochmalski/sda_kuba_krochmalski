/*
 * Enemy.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#include "Enemy.hpp"

Enemy::Enemy()
: Actor()
, isAggressive(false)
{
	// TODO Auto-generated constructor stub

}
Enemy::Enemy(int x, int y)
: Actor(x, y)
, isAggressive(false)
{

}
Enemy::Enemy(int x, int y, char symbol, Game::FgColour::Colour fgc, Game::BgColour::Colour bgc, CGame* gPtr)
: Actor(x, y, symbol, fgc, bgc, gPtr)
, isAggressive(false)
{

}

Enemy::~Enemy()
{
	// TODO Auto-generated destructor stub
}

int Enemy::getRandMove()
{
	int randMove = rand()%4 + 1;

	return randMove;
}

