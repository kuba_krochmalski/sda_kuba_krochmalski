/*
 * Enemy.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_
#include "Actor.hpp"

class Enemy : public Actor
{
private:
	bool isAggressive;

public:
	Enemy();
	Enemy(int x, int y);
	Enemy(int x, int y, char symbol, Game::FgColour::Colour fgc, Game::BgColour::Colour bgc, CGame* gPtr);
	virtual ~Enemy();
	int getRandMove();
};

#endif /* ENEMY_HPP_ */
