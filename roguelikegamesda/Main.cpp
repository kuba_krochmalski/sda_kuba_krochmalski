//============================================================================
// Name        : Gra.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "Console.hpp"
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include "Actor.hpp"
#include "Enemy.hpp"
#include "Player.hpp"
#include "Tile.hpp"
#include "Map.hpp"
#include "CGame.hpp"
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
	srand(time(NULL));

	CGame game;
	game.init();

	Game::Key key = Game::Key::INVALID;

	game.gameLoop(key);

	return 0;
}
