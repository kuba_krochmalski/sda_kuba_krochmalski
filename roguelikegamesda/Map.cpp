/*
 * Map.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include "Map.hpp"
#include <algorithm>

Map::Map(int width, int height)
: mWidth(width)
, mHeight(height)
{
}

Map::Map()
: mWidth(10)
, mHeight(10)
{
}

void Map::init()
{
	for(int x = 0; x < mWidth; ++x)
	{
		for(int y = 0; y < mHeight; ++y)
		{
			mMapa.push_back(Tile(x, y, false));
		}
	}
}

Map::~Map()
{
	// TODO Auto-generated destructor stub
}

struct comparePos
{
	int posX;
	int posY;

	comparePos(int x, int y)
	{
		posX = x;
		posY = y;
	}

	bool operator()(const Tile& tile)
	{
		if(tile.x == posX && tile.y == posY) {return true;}
		else {return false;}
	}
};

bool Map::isWall(int x, int y)
{
	std::vector<Tile>::iterator it = std::find_if(mMapa.begin(), mMapa.end(), comparePos(x, y));

	return it->isWall;
}

void Map::setTile(int x, int y, bool isW)
{
	std::vector<Tile>::iterator it = std::find_if(mMapa.begin(), mMapa.end(), comparePos(x, y));

	it->isWall = isW;
}

void Map::setHeight(int height)
{
	mHeight = height;
}

void Map::setWidth(int width)
{
	mWidth = width;
}

void Map::render(Game::Console& console)
{
	for(std::vector<Tile>::iterator it = mMapa.begin(); it != mMapa.end(); ++it)
	{
		if(it->isWall == true)
		{
			console.drawChar('#', it -> x, it -> y, Game::FgColour::WHITE, Game::BgColour::BLACK);
		}

		else
		{
			console.drawChar(' ', it -> x, it -> y, Game::FgColour::WHITE, Game::BgColour::BLACK);
		}
	}
}

int Map::getHeight()
{
	return mHeight;
}

int Map::getWidth()
{
	return mWidth;
}
