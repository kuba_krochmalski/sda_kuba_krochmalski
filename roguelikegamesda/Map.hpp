/*
 * Map.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef MAP_HPP_
#define MAP_HPP_
#include <vector>
#include "Tile.hpp"
#include "Console.hpp"

class Map
{
private:
	std::vector<Tile> mMapa;
	int mWidth;
	int mHeight;

public:
	Map(int width, int height);
	Map();
	virtual ~Map();

	bool isWall(int x, int y);
	void setTile(int x, int y, bool isW);
	void render(Game::Console& console);
	void init();
	void setHeight(int height);
	void setWidth(int width);
	int getHeight();
	int getWidth();
};

#endif /* MAP_HPP_ */
