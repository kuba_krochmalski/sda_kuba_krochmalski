/*
 * Player.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#include "Player.hpp"

Player::Player()
: Actor()
, mHP(100)
{
	// TODO Auto-generated constructor stub

}

Player::Player(int x, int y)
: Actor(x, y)
, mHP(100)
{

}

Player::Player(int x, int y, char symbol, Game::FgColour::Colour fgc, Game::BgColour::Colour bgc, int pz, CGame* gPtr)
: Actor(x, y, symbol, fgc, bgc, gPtr)
, mHP(pz)
{

}

Player::~Player()
{
	// TODO Auto-generated destructor stub
}

void Player::changeHP(int damage)
{
	mHP -= damage;
}

