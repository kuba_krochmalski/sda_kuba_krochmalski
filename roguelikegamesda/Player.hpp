/*
 * Player.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_
#include "Actor.hpp"

class Player : public Actor
{
private:
	int mHP;

public:
	Player();
	Player(int x, int y);
	Player(int x, int y, char symbol, Game::FgColour::Colour fgc, Game::BgColour::Colour bgc, int pz, CGame* gPtr);
	virtual ~Player();
	void changeHP(int damage);

	int getHp() const
	{
		return mHP;
	}
};

#endif /* PLAYER_HPP_ */
