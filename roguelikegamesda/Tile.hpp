/*
 * Tile.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef TILE_HPP_
#define TILE_HPP_
#include "Console.hpp"

struct Tile
{
	int x;
	int y;
	bool isWall;

	Tile(int xx, int yy, bool isW)
	: x(xx)
	, y(yy)
	, isWall(isW)
	{
	}

	~Tile()
	{
	}
};

#endif /* TILE_HPP_ */
