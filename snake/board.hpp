#ifndef _BOARD_HPP_
#define _BOARD_HPP_

//Plansza do gry
struct board {
  int length;
  int width;
  int border_thickness;
  char symbol;
};

#endif /* _BOARD_HPP_ */
