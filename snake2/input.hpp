#ifndef _INPUT_HPP_
#define _INPUT_HPP_

/**
 * Returns a single character read from standard input or the null-terminating
 * character if the buffer is empty. If a call to enable_canonical_mode was
 * called prior to this function, then read_key will be non-blocking.
 */
char read_key();

#endif /* _INPUT_HPP_ */
