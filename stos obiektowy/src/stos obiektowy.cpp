#include <iostream>

const int MAKSYMALNY_ROZMIAR_STOSU = 5;

class Stos
{
private:
	int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisa� nowy element
	int dane[MAKSYMALNY_ROZMIAR_STOSU];

public:
	int pokazRozmiar()
	{
		// poka� rozmiar stosu
		return wierzcholek;
	}

	int pokazWierzcholek()
	{
		// sprawd�, co jest na wierzchu stosu
		// je�li stos jest pusty, to poinformuj o tym
		if (wierzcholek == 0)
		{
			return 0;
		}
		else
		{
			// w przeciwnym wypadku - poka� wierzcho�ek
			// ostatni wstawiony element znajduje si� o 1 ni�ej ni� miejsce na nowy element
			int wierzch = dane[wierzcholek - 1];
			return wierzch;
		}
	}

	int wstaw(int nowaWartosc)
	{
		if (wierzcholek >= MAKSYMALNY_ROZMIAR_STOSU)
		{
			std::cout << "Stos pe�ny!" << std::endl;
			return 0;
		}

		// wstawi� liczb� na szczyt stosu
		dane[wierzcholek] = nowaWartosc; // zapisz dane na wierzcho�ku stosu
		wierzcholek += 1;		// przesuwam wierzcho�ek
		return nowaWartosc;
	}

	int zdejmij()
	{
		if (wierzcholek == 0)
		{
			std::cout << "Pr�ba zdj�cia z pustego stosu!" << std::endl;
			return 0;
		}

		wierzcholek -= 1; // przesuwam wierzcho�ek na pierwszy istniej�cy element
		int zdjete = dane[wierzcholek]; // odczytaj dane z wierzchu stosu
		return zdjete;
	}

	Stos()
	{
		// zerowanie stosu
		wierzcholek = 0;
		for (int i = 0; i < MAKSYMALNY_ROZMIAR_STOSU; ++i)
		{
			dane[i] = 0;
		}
	}
};


int main()
{
	// nowy stos
	Stos liczby;

	std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

	std::cout << "Wstawiam: " << liczby.wstaw(4) << std::endl;

	std::cout << "Na wierzchu jest: " << liczby.pokazWierzcholek() << std::endl;

	std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

	std::cout << "Wstawiam: " << liczby.wstaw(7) << std::endl;

	std::cout << "Na wierzchu jest: " << liczby.pokazWierzcholek() << std::endl;

	std::cout << "Rozmiar: "<< liczby.pokazRozmiar() << std::endl;

	std::cout << "Zdj�te: " << liczby.zdejmij() << std::endl;

	std::cout << "Na wierzchu jest: " << liczby.pokazWierzcholek() << std::endl;

	std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

	std::cout << "Zdj�te: " << liczby.zdejmij() << std::endl;

	std::cout << "Na wierzchu jest: " << liczby.pokazWierzcholek() << std::endl;

	std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

	std::cout << "Zdj�te: " << liczby.zdejmij() << std::endl;

	std::cout << "Wstawiam: " << liczby.wstaw(42) << std::endl;
	std::cout << "Wstawiam: " << liczby.wstaw(128) << std::endl;
	std::cout << "Wstawiam: " << liczby.wstaw(-1) << std::endl;
	std::cout << "Wstawiam: " << liczby.wstaw(8) << std::endl;
	std::cout << "Wstawiam: " << liczby.wstaw(-4) << std::endl;
	std::cout << "Wstawiam: " << liczby.wstaw(10) << std::endl;
	std::cout << "Wstawiam: " << liczby.wstaw(100) << std::endl;

	std::cout << "Rozmiar: "<< liczby.pokazRozmiar() << std::endl;
	std::cout << "Na wierzchu jest: " << liczby.pokazWierzcholek() << std::endl;

	std::cout << "Wstawiam: " << liczby.wstaw(100) << std::endl;

	return 0;
}
