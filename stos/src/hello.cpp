//============================================================================
// Name        : hello.cpp
// Author      : kuba
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

const int MAKSYMALNY_ROZMIAR_STOSU = 5;

struct Stos
{
	int wierzcholek;
	int dane[MAKSYMALNY_ROZMIAR_STOSU];
};

void pokazRozmiar(Stos stosLiczb)
{
	//pokaz rozmiar stosu
	std::cout << "Rozmiar: " << stosLiczb.wierzcholek << std::endl;
}

void pokazWierzcholek(Stos stosLiczb)
{
	//sprawdz, co jest na wierzchu stosu
	if(stosLiczb.wierzcholek == 0)
	{
		std::cout << "Stos jest pusty" << std::endl;
	}
	else
	{
	std::cout << "Na gorze jest: " << stosLiczb.dane[stosLiczb.wierzcholek - 1] << std::endl;
	}
}

void wstaw(Stos& stosLiczb, int nowaWartosc)
{
	//wstaw liczbe na szczyt stosu
	if (stosLiczb.wierzcholek >= MAKSYMALNY_ROZMIAR_STOSU)
	{
		std::cout << "Stos pelny!" << std::endl;
	}
	else
	{
	stosLiczb.dane[stosLiczb.wierzcholek] = nowaWartosc;
	stosLiczb.wierzcholek += 1;
	std::cout << "Dodaje do stosu: " << nowaWartosc << std::endl;
	}
}

void zdejmij(Stos& stosLiczb)
{
	//zdejmij element ze stosu
	if (stosLiczb.wierzcholek ==0)
	{
		std::cout << "Proba zdjecia elementu z pustego stosu!" << std::endl;
	}
	else
	{
	stosLiczb.wierzcholek -= 1;
	int zdjete = stosLiczb.dane[stosLiczb.wierzcholek];
	std::cout << "Zdejmuje ze stosu: " << zdjete << std::endl;
	}
}
int main()
{

	Stos liczby; //nowy stos

	//zerowanie stosu
	liczby.wierzcholek = 0;
	for (int i=0; i < MAKSYMALNY_ROZMIAR_STOSU; i++)
	{
		liczby.dane[i] = 0;
	}

	pokazRozmiar(liczby);

	wstaw(liczby, 4);

	pokazRozmiar(liczby);

	pokazWierzcholek(liczby);

	wstaw(liczby, 7);

	pokazRozmiar(liczby);

	pokazWierzcholek(liczby);

	zdejmij(liczby);

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	zdejmij(liczby);

	pokazRozmiar(liczby);

	zdejmij(liczby);
	zdejmij(liczby);

	wstaw(liczby, 13);

	wstaw(liczby, 15);

	wstaw(liczby, 20);

	wstaw(liczby, 34);

	wstaw(liczby, 9);

	wstaw(liczby, 100);










	return 0;
}

