#include <iostream>

int main()
{
	std::cout << "Podaj ilo�� liczb: " << std::endl;
	int ilosc;
	std::cin >> ilosc;

	int* tablica;

	tablica = new int[ilosc];

	int suma = 0;

	for (int i=0; i<ilosc; ++i)
	{
		std::cout << "Podaj " << i+1 << ". element tablicy: " << std::flush;
		std::cin >> tablica[i];
		suma += tablica[i];
	}

	std::cout << "-------------------------" << std::endl;
	std::cout << "Suma: " << suma << std::endl;
	std::cout << "-------------------------" << std::endl;

	delete[] tablica;

	for (int i = 0; i < ilosc; ++i)
	{
		std::cout << tablica[i] << std::endl;
	}

	return 0;
}
