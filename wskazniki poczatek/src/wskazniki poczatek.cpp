#include <iostream>

int main()
{
	int a = 10;
	int* x = NULL; //zerowanie wskaznika !zawsze NULL!

	std::cout << "A= " << a << std::endl;
	std::cout << "X= " << x << std::endl << std::endl;

	std::cout << "&A= " << (&a) << std::endl; //adres zmiennej a
	x = &a; //przypisuje do x adres zmiennej a
	std::cout << "X= " << x << std::endl << std::endl;

	a += 1;

	std::cout << "A= " << a << std::endl;
	std::cout << "*X= " << (*x) << std::endl << std::endl;

	*x += 5;

	std::cout << "A= " << a << std::endl;
	std::cout << "*X= " << (*x) << std::endl << std::endl;

	std::cout << "&X= " << (&x) << std::endl << std::endl;

	int* y = NULL;

	std::cout << "Y= " << y << std::endl;
	std::cout << "&Y= " << (&y) << std::endl << std::endl;

	y = new int;

	std::cout << "Y= " << y << std::endl;
	std::cout << "&Y= " << (&y) << std::endl << std::endl;

	delete y;



	return 0;

}
