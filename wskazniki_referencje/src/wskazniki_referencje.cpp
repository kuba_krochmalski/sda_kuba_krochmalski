#include <iostream>

void kwadrat(int& x);
void zwiekszOPiec(int* w);

int main(int argc, char* argv[])

{

	std::cout << "Podaj liczb�: " << std::endl;

	int liczba;

	std::cin >> liczba;

	zwiekszOPiec(&liczba);

	kwadrat(liczba);

	std::cout << "(liczba+5)^2 = " << liczba << std::endl;

	return 0;

}

void kwadrat(int& x)
{
	x *= x;
}
void zwiekszOPiec(int* w)
{
	*w += 5;
}
