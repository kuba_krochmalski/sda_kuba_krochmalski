#include <iostream>

void przypisz(int liczby[], const int& granica)
{
	for (int i = 0; i <= granica; ++i)
	{
		if(i%2 != 0)
		{
			liczby[i/2] = i;
		}
	}
}

int main()
{
	int granica;

	std::cout << "Program wypisuje liczby naturalne." << std::endl;
	std::cout << "Podaj granice: " << std::flush;
	std::cin >> granica;

	int* naturalne;
	naturalne = new int[granica+1];

	przypisz(naturalne, granica);

	for (int i = 0; i <= granica/2-1; ++i)
	{
		std::cout << naturalne[i] << std::endl;
	}

	delete[] naturalne;

	return 0;
}
