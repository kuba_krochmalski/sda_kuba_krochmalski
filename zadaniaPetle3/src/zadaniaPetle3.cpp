#include <iostream>

void przypisz(int liczby[])
{
	for (int i = 0; i < 20; ++i)
	{
		liczby[i] = i+1;
	}
}

int main()
{
	std::cout << "Liczba podzielna przez 3 = 'Fizz'" << std::endl;
	std::cout << "Liczba podzielna przez 5 = 'Buzz'" << std::endl;
	std::cout << "Liczba podzielna przez 3 i przez 5 = 'FizzBuzz'" << std::endl << std::endl;

	int liczby[20];
	przypisz(liczby);

	for (int i = 0; i < 20; ++i)
	{
		if(liczby[i]%3 == 0)
		{
			if(liczby[i]%5 == 0)
			{
				std::cout << "FizzBuzz" << std::endl;
			}
			else
			{
				std::cout << "Fizz" << std::endl;
			}
		}
		else if(liczby[i]%5 == 0)
		{
			std::cout << "Buzz" << std::endl;
		}
		else
		{
			std::cout << liczby[i] << std::endl;
		}
	}
	return 0;
}
