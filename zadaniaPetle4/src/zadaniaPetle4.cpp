#include <iostream>

int main()
{
	std::cout << "Program oblicza srednia z podanych liczb." << std::endl;
	std::cout << "Podaj ilosc liczb: " << std::flush;
	int iloscLiczb;
	std::cin >> iloscLiczb;
	std::cout << "---------------------" << std::endl;

	double* tablica;
	tablica = new double[iloscLiczb];

	double suma = 0;

	for (int i = 0; i < iloscLiczb; i++)
	{
		std::cout << "Podaj liczbe nr " << i+1 << ": " << std::flush;
		std::cin >> tablica[i];
		suma += tablica[i];
	}
	std::cout << "---------------------" << std::endl;

	double srednia = suma/iloscLiczb;

	std::cout << "Srednia to: " << srednia << std::endl;

	delete[] tablica;

	return 0;
}
