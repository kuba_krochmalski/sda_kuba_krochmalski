#include <iostream>

int main()
{
	std::cout << "Program sumuje kolejne liczby calkowite (od 0) tak," << std::endl;
	std::cout << "aby nie przekroczyc podanego limitu." << std::endl << std::endl;
	std::cout << "Podaj limit: " << std::flush;
	int limit, suma = 0, liczba = 0;
	std::cin >> limit;

	while(suma < limit-liczba)
	{
		suma += liczba;

		++liczba;
	}

	std::cout << "Suma: " << suma << std::endl;
	std::cout << "ilosc dodanych do siebie liczb: " << liczba << std::endl;
	return 0;
}
