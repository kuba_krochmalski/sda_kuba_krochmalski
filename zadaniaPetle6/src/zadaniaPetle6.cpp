#include <iostream>
#include <cstdlib>
#include <ctime>

int main()
{
	std::cout << "Program losuje 100 liczb z zakresu od 0 do 20 i zlicza ich wystapienia." << std::endl;
	int liczby[100];
	int wystapienia[21];

	for (int i = 0; i < 21; ++i)
	{
		wystapienia[i] = 0;
	}

	srand(time(NULL));

	for (int i = 0; i < 100; ++i)
	{
		liczby[i] = rand()%21;

		for (int k = 0; k < 21; k++)
		{
			if(liczby[i] == k)
			{
				wystapienia[k] += 1;
			}
		}

		// std::cout << liczby[i] << std::endl; - wypisywanie wylosowanych liczb
	}
	std::cout << "---------------------" << std::endl;

	for (int i = 0; i < 21; ++i)
	{
		std::cout << "Wystapienia " << i << " :" << wystapienia[i] << std::endl;
	}
	return 0;
}
