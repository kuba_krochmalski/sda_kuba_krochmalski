#include <iostream>
#include <algorithm>
#include <cmath>

int indeksPercentylu(double percentyl, int maksIndeks)
{
	return static_cast<int>(std::floor(maksIndeks * percentyl));
}

void znajdzPercentyle(double* wartosci, int ilosc, int& percentyl50,
		int& percentyl95)

{
	const int indeks50 = indeksPercentylu(0.5, ilosc - 1);

	const int indeks95 = indeksPercentylu(0.95, ilosc - 1);

	std::sort(wartosci, wartosci + ilosc);

	percentyl50 = wartosci[indeks50];

	percentyl95 = wartosci[indeks95];
}


int main()
{
	std::cout << "Program oblicza 50. oraz 95. percentyl podanego zbioru liczb." << std::endl;
	std::cout << "Podaj ilosc liczb: " << std::flush;
	int iloscLiczb;
	std::cin >> iloscLiczb;
	std::cout << "------------------------" << std::endl;

	double* tablica;
	tablica = new double[iloscLiczb];

	for(int i=0; i<iloscLiczb; ++i)
	{
		std::cout << "Podaj liczbe nr " << i+1 << ": " << std::flush;
		std::cin >> tablica[i];
	}
	std::cout << "------------------------" << std::endl;

	std::sort(tablica, tablica+iloscLiczb); //sortowanie

	std::cout << "Posortowana" << std::endl;
	for (int i = 0; i < iloscLiczb; ++i)
	{
		std::cout << "Liczba nr " << i+1 << ": " << tablica[i] << std::endl;
	}
	std::cout << "------------------------" << std::endl;

	int percentyl50, percentyl95;

	znajdzPercentyle(tablica, iloscLiczb, percentyl50, percentyl95);

	std::cout << "50. percentyl wynosi: " << percentyl50 << ", a 95. percentyl wynosi: " << percentyl95 << std::endl;

	delete[] tablica;
	return 0;
}
