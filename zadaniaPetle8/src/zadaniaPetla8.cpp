#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>

int ileCyfr(int x)
{
	int cyfry = 1;

	while(x != 0)
	{
		x /= 10;
		++cyfry;
	}

	return cyfry-1;
}

int main()
{
	srand(time(NULL));
	int liczba;
	liczba = rand();

	std::cout << liczba << std::endl;

	std::cout << "Ilosc cyfr: " << ileCyfr(liczba) << std::endl;

	int* cyfry;
	cyfry = new int[ileCyfr(liczba)];

	int tymczasowa = liczba;

	for (int i=0; i<ileCyfr(liczba); ++i)
	{
		cyfry[i] = tymczasowa/(pow(10,ileCyfr(liczba)-(i+1)));
		tymczasowa = tymczasowa - (cyfry[i]*(pow(10,ileCyfr(liczba)-(i+1))));
	}

	int suma = 0;

	for (int i = 0; i < ileCyfr(liczba); ++i)
	{
		std::cout << cyfry[i] << std::endl;
		suma += cyfry[i];
	}

	std::cout << "SUMA: " << suma << std::endl;

	delete[] cyfry;
	return 0;
}
