#include <iostream>
#include <math.h>

void rozwiazRownanie(double a, double b, double c);

int main()
{
	rozwiazRownanie(1.0, 1.0, 0.0);
	std::cout << "-----------------------" << std::endl;
	rozwiazRownanie(3.0, -6.0, 3.0);
	std::cout << "-----------------------" << std::endl;
	rozwiazRownanie(0.5, 2.0, -4.0);
	std::cout << "-----------------------" << std::endl;
	rozwiazRownanie(0.0, 1.0, 1.0);
	std::cout << "-----------------------" << std::endl;
	rozwiazRownanie(1.0, 1.0, 1.0);
	std::cout << "-----------------------" << std::endl;

	return 0;
}

void rozwiazRownanie(double a, double b, double c)
{
	double delta = b * b - 4 * a * c;

	if (a == 0)
	{
		std::cout << "B��dne dane." << std::endl;
	}
	else if (delta > 0)
	{
		std::cout << "X1 = " << (-b - sqrt(delta)) / (2 * a) << std::endl;
		std::cout << "X2 = " << (-b + sqrt(delta)) / (2 * a) << std::endl;
	}
	else if (delta == 0)
	{
		std::cout << "X0 = " << -b / (2 * a) << std::endl;
	}
	else
	{
		std::cout << "Brak rozwiazan." << std::endl;
	}
}
