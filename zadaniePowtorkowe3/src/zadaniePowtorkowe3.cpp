#include <iostream>
#include <math.h>

int NWD(int m, int n);

int main()
{
	std::cout << NWD(16, 12) << std::endl;
	std::cout << NWD(6, 0) << std::endl;
	std::cout << NWD(0, 9) << std::endl;

	return 0;
}

int NWD(int m, int n)
{
	int NWD = 0;

	if(m == 0)
	{
		return n;
	}

	if (n == 0)
	{
		return m;
	}

	else
	{
		int r = n%m;

		for (int i=1; i<=r; i++)
		{
			if((r%i == 0) && (m%i == 0))
			{
				NWD = i;
			}
		}
		return NWD;
	}
}
