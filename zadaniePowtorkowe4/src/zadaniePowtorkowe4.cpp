#include <iostream>

int minimum(const int tablica[], int rozmiar);

int main()
{
	const int dane[] = {4, 7, 2, -2, 3, 0, 2, -1};
	const int rozmiar = 8;

	std::cout << minimum(dane, rozmiar) << std::endl;

	return 0;
}

int minimum(const int tablica[], int rozmiar)
{
	int minimum = tablica[0];

	for (int i=0; i<rozmiar; ++i)
	{
		if(tablica[i]<minimum)
		{
			minimum = tablica[i];
		}
	}

	return minimum;
}
