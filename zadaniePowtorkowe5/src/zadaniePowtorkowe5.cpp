#include <iostream>

int iteracyjnaSilnia(int n);
int rekurencyjnaSilnia(int n);

int main()
{
	int liczba = 5;
	std::cout << "Silnia z " << liczba << " iteracyjnie: " << iteracyjnaSilnia(liczba) << std::endl;
	std::cout << "Silnia z " << liczba << " rekurencyjnie: " << rekurencyjnaSilnia(liczba) << std::endl;

	return 0;
}

int iteracyjnaSilnia(int n)
{
	if (n < 0)
	{
		std::cerr << "B��dne dane!" << std::endl;
		return 0;
	}
	else if (n == 0 || n == 1)
	{
		return 1;
	}

	else
	{
		int silnia = 1;
		for (int i = 1; i <= n; ++i)
		{
			silnia *= i;
		}
		return silnia;
	}
}
int rekurencyjnaSilnia(int n)
{
	if (n < 0)
	{
		std::cerr << "B��dne dane!" << std::endl;
		return 0;
	}
	else if (n == 0 || n == 1)
	{
		return 1;
	}

	else
	{
		return n*rekurencyjnaSilnia(n-1);
	}
}
