#include <iostream>

void wypisz(int ilosc, int tablica[]);

int main()
{
	int iloscLiczb = 18;

	if(iloscLiczb <= 0)
	{
		std::cout << "Nic do wypisania!" << std::endl;
		return 0;
	}

	else
	{
		int liczby[iloscLiczb];

		liczby[0] = 1;
		liczby[1] = 1;

		for (int i = 2; i < iloscLiczb; i++)
		{
			liczby[i] = liczby[i - 1] + liczby[i - 2];
		}

		wypisz(iloscLiczb, liczby);

		return 0;
	}
}

void wypisz(int ilosc, int tablica[])
{
	for (int i = 0; i < ilosc; i++)
	{
		std::cout << i+1 << ". " << tablica[i] << std::endl;
	}
}
