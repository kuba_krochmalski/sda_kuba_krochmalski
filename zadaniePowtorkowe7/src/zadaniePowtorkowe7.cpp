#include <iostream>



void pokazMacierz(const int tablica[5][5]);

void transponuj(int tablica[5][5]);



int main()

{

	int macierz[5][5] = {{1, 2, 3, 4, 5},

						{6, 7, 8, 9, 10},

						{11, 12, 13, 14, 15},

						{16, 17, 18, 19, 20},

						{21, 22, 23, 24, 25}};



	pokazMacierz(macierz);

	std::cout << "------------------" << std::endl;

	transponuj(macierz);

	pokazMacierz(macierz);



	return 0;

}



void pokazMacierz(const int tablica[5][5])

{

	for (int y = 0; y < 5; ++y)

	{

		for (int x = 0; x < 5; ++x)

		{

			if (tablica[y][x] < 10)

			{

				std::cout << ' ';

			}

			std::cout << tablica[y][x] << ' ';

		}

		std::cout << '\n';

	}

	std::cout << std::flush;

}



void transponuj(int tablica[5][5])

{

	int temp;



	for (int y = 0; y < 5; ++y)

	{

		for (int x = y; x < 5; ++x)

		{

			temp = tablica[y][x];

			tablica[y][x] = tablica[x][y];

			tablica[x][y] = temp;

		}

	}

}
