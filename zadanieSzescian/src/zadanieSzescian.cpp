#include <iostream>

void obliczSzescian(double x, double& wynik)
{
	wynik = x*x*x;
}

int main()
{
	double liczba;
	double wynik;

	std::cout << "Podaj liczbe rzeczywista: " << std::flush;
	std::cin >> liczba;

	obliczSzescian(liczba, wynik);

	std::cout << "Szescian z " << liczba << " wynosi " << wynik << std::endl;

	return 0;
}
